var polygon, city, forecast, popup;
var resultForeCast = document.getElementById('resultForeCast');
var mymap = L.map('mapid').locate({ setView: true });
L.tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
    {
        attribution: "OpenStreetMap",
        "minZoom": 0,
        "maxZoom": 20
    }).addTo(mymap);

var input = document.getElementById('search');
var options = {
    types: ['(cities)'],
    componentRestrictions: { country: 'br' }
};
autocomplete = new google.maps.places.Autocomplete(input, options);
google.maps.event.addListener(autocomplete, 'place_changed', function () {
    setBoundsAndLocationId();
});

function setBoundsAndLocationId() {
    var location = autocomplete.getPlace().geometry.location.toJSON();
    var bounds = L.bounds(location.lat, location.lng);
    mymap.setView([location.lat, location.lng], 13);

    $.ajax({
        url: './api/getLocaleId/' + autocomplete.getPlace().name,
        type: 'GET',
        cache: false,
        crossDomain: true,
        success: function (data) {
            city = data;
        },
        error: function (e) {
            alert(e.responseText);
            city = undefined;
        },
        async: false
    });
    if (!city) {
        resultForeCast.innerHTML = "";
        return;
    }


    var value = $('input[name=forecast]:checked', '.searchBox').val();
    getForeCast(value, city.id)

}

function changeForeCast() {
    if (!city) {
        alert('Selecione uma cidade!');
        return;
    }
    var value = $('input[name=forecast]:checked', '.searchBox').val();
    getForeCast(value, city.id)
}

function getForeCast(days, cityId) {
    resultForeCast.innerHTML = "";
    var url;
    if (days == '4') {
        url = './api/forecast/' + cityId;
    } else {
        url = './api/forecast7/' + cityId;
    }

    $.ajax({
        url: url,
        type: 'GET',
        cache: false,
        crossDomain: true,
        success: function (data) {
            forecast = data;
        },
        error: function (e) {
            alert(e.responseText);
            forecast = undefined;
        },
        async: false
    });
    resultForeCast.innerHTML = "";

    if (!forecast) {
        return;
    }
    var lbCity = document.createElement('label');
    lbCity.innerHTML = forecast.city + ' - ' + forecast.uf;
    var lbUpdate = document.createElement('label');
    lbUpdate.innerHTML = 'Atualizado: ' + forecast.time;
    resultForeCast.appendChild(lbCity);
    resultForeCast.appendChild(lbUpdate);
    resultForeCast.appendChild(document.createElement('hr'));

    var select;
    forecast.forecast.forEach(element => {
        var div = document.createElement('div');
        var radio = document.createElement('input');
        radio.type = 'radio';
        radio.setAttribute('name', 'forecastResult');
        radio.setAttribute('value', element.day);
        radio.onchange = function () {
            createPopUp(element);
        };
        if (!select) {
            radio.setAttribute('checked', '');
            select = true;
            createPopUp(element);
        }

        var label = document.createElement('label');
        label.innerHTML = element.day;
        div.appendChild(radio);
        div.appendChild(label);
        resultForeCast.appendChild(div);
    });
}

function createPopUp(element) {
    console.log(element);
    //condition: "Pancadas de Chuva"
    //conditionCode:"pc"
    //day:"2017-12-05"
    //iuv:"13.0"
    //max:"26"
    //min:"21"



    var div = document.createElement('div');
    div.className = "forecastPopUp"
    var lb = document.createElement('label');
    lb.innerHTML = "Dia: " + element.day;
    div.appendChild(lb);
    lb = document.createElement('label');
    lb.innerHTML = "Mínima: " + element.min + "°C";
    div.appendChild(lb);
    lb = document.createElement('label');
    lb.innerHTML = "Máxima: " + element.max + "°C";
    div.appendChild(lb);
    lb = document.createElement('label');
    lb.innerHTML = "IUV : " + element.iuv;
    div.appendChild(lb);
    lb = document.createElement('label');
    lb.innerHTML = "Previsão : " + element.condition;
    div.appendChild(lb);

    var popup = L.popup()
        .setLatLng([autocomplete.getPlace().geometry.location.lat(), autocomplete.getPlace().geometry.location.lng()])
        .setContent(div)
        .openOn(mymap);
}