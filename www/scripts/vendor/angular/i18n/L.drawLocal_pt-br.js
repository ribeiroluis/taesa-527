﻿L.drawLocal = {
    "draw": {
        "toolbar": {
            "actions": { "title": "Cancelar desenho", "text": "Cancelar" },
            "finish": { "title": "Terminar desenho", "text": "Terminar" },
            "undo": { "title": "Apagar ultimo ponto desenhado", "text": "Apagar ultimo ponto" },
            "buttons": {
                "polyline": "Desenhar uma polilinha",
                "polygon": "Desenhar um poligono",
                "rectangle": "Desenhar um retangulo",
                "circle": "Desenhar um circulo",
                "marker": "Desenhar um marcador"
            }
        },
        "handlers": {
            "circle": { "tooltip": { "start": "Clique e arraste para desenhar um círculo." }, "radius": "Raio" },
            "marker": { "tooltip": { "start": "Clique no mapa para inserir um marcador." } },
            "polygon": { "tooltip": { "start": "Clique para começar a desenhar a forma.", "cont": "Clique para continuar a desenhar a forma.", "end": "Clique no primeiro ponto para fechar esta forma." } },
            "polyline": { "error": "<strong>Erro:</strong> As bordas da forma não podem se fechar!", "tooltip": { "start": "Clique para começar a desenhar linha.", "cont": "Clique para continuar desenhando a linha.", "end": "Clique no último ponto para concluir a linha." } },
            "rectangle": { "tooltip": { "start": "Clique e arraste para desenhar um retângulo." } },
            "simpleshape": { "tooltip": { "end": "Solte o mouse para terminar de desenhar." } }
        }
    },
    "edit": {
        "toolbar": {
            "actions": {
                "save": { "title": "Salvar modificações.", "text": "Salvar" },
                "cancel": { "title": "Cancelar edição, descarta todas as alterações.", "text": "Cancelar" }
            },
            "buttons": { "edit": "Editar camadas.", "editDisabled": "Não há camadas para editar.", "remove": "Apagar camdas.", "removeDisabled": "Sem camadas para apagar." }
        },
        "handlers": { "edit": { "tooltip": { "text": "Arrastar as bordas ou marcador para editar a camada.", "subtext": "Clique em cancelar para desfazer alterações." } }, "remove": { "tooltip": { "text": "Clique em um recurso para remover" } } }
    }
};