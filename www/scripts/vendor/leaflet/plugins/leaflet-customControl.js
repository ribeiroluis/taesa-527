﻿//This plugin depends of JQuery UI https://code.jquery.com/ui/1.12.1/jquery-ui.js, https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css

//_layer = L.geoJson(geoJsonObject, {
//    active: true | false,
//    type: GeoJsonType,
//    name: string,
//    group: string,
//    subGroup: undefined 
//             | {exclusive: true, type: ["default", "cableKind", "cableGauge", "networkStructure", "singlePhaseKind"], callback: function}
//             | { exclusive: false, type: "treeKindName", callback: function } };                
//});
//{ type: "overlayer", overLayers: [] | undefined, restoreLayers: true, container: "", hiddden: true  }
//{ type: "baselayer", baseLayers: [{ tileLayer: L.tileLayer(), name: "Name", default: true, container: "", hiddden: true  }] }

//buscar um alimentador, abrir camada buscar outro alimentador

var GLOBAL_CUSTOMCONTROL;

L.Control.customControl = L.Control.extend({
    _currentBaseLayer: undefined,
    _overlayers: {},
    _baselayers: {},
    _currentBounds: null,

    onAdd: function (map) {
        var currentBaseLayer;
        this._map = map;
        if (GLOBAL_CUSTOMCONTROL && GLOBAL_CUSTOMCONTROL._currentBaseLayer) {
            currentBaseLayer = GLOBAL_CUSTOMCONTROL._currentBaseLayer;
        }
        GLOBAL_CUSTOMCONTROL = this;
        GLOBAL_CUSTOMCONTROL._currentBaseLayer = currentBaseLayer;
        
        if (this.options.type === "baselayer" && this.options.baseLayers) {
            this._baselayers = this.options.baseLayers;
        }
        if (this.options.type === "overlayer") {
            if (!GLOBAL_CUSTOMCONTROL.events) {
                map.on("layeradd ", this._layeradd);
                map.on("layerremove ", this._layerremove);
                this._restoreLayers(map);
                GLOBAL_CUSTOMCONTROL.events = true;
            }
        }
        this._createContainer();
        return L.DomUtil.create("div");
    },

    onRemove: function (map) {
        $(this._customcontainer).remove();
    },

    _layeradd: function (e) {
        var self;

        if (e.layer._layers && e.layer.options && e.layer.options.addOnControl) {
            var group = e.layer.options.group;
            var name = e.layer.options.name;
            self = GLOBAL_CUSTOMCONTROL;
            if (!e.layer.options.isChanged) {
                if (!self._overlayers[group]) {
                    self._overlayers[group] = [];
                    self._overlayers[group].push({ name: name, layer: e.layer });
                    if ($(".customControl-acordionDiv").length > 0) {
                        self._createOverLayerGroup(document.getElementById("customControlOverlay"), group, self._overlayers[group]);
                    }
                } else {
                    self._overlayers[e.layer.options.group].push({ name: name, layer: e.layer });
                    var item = self._createOverlayerGroupItems(e.layer);
                    $("#content" + group.replace(/\s/g, '')).append(item);
                }
                self._updateAcordion();
                e.layer.options.isChanged = true;
            }
            if (!e.layer.options.active) {
                e.layer.options.isChanged = true;
                this.removeLayer(e.layer);
            }
        }
        if (e.layer._tiles) {
            return;
        }
    },

    _layerremove: function (e) {
        if (e.layer._layers) {
            var layer = e.layer;
            var self = GLOBAL_CUSTOMCONTROL;
            if (!layer.options || !layer.options.addOnControl || layer.options.isChanged || layer.options.isRemoved) {
                return;
            }
            $("#layer" + layer.options.group.replace(/\s/g, '') + "_" + layer.options.name.replace(/\s/g, '')).remove();
            var layerIndex = self._overlayers[layer.options.group].findIndex(function (e) { return e.name === layer.options.name });
            self._overlayers[layer.options.group].splice(layerIndex, 1);
        }
        if (e.layer._tiles) {
        }
    },

    _createContainer: function () {
        var dragDiv = L.DomUtil.create("div", "customControl-dragDiv");
        var divider = L.DomUtil.create("div", "divider");
        dragDiv.appendChild(divider);

        switch (this.options.type) {
            case "overlayer":
                dragDiv.innerText = "Camadas";
                this._createOverlayerContainer(dragDiv);
                break;
            case "baselayer":
                dragDiv.innerText = "Mapas";
                this._createBaseLayerContainer(dragDiv);
                break;
        }

        $(this.options.container).append(dragDiv);

        $(dragDiv).draggable({
            containment: this.options.container
        });
        this._customcontainer = dragDiv;
    },

    _createBaseLayerContainer: function (parentElement) {
        var self = this;
        var div = L.DomUtil.create("div", "");
        var select = L.DomUtil.create("select", "block");
        self._baselayers.forEach(function (e) {
            select.appendChild(self._createBaseLayerItem(e));
        });
        $(select).change(function () {

            var value = this.value;
            var baseLayer = self._baselayers.find(function (e) { return e.name === value });

            if (!self._map) {
                baseLayer.tileLayer._map.removeLayer(GLOBAL_CUSTOMCONTROL._currentBaseLayer.tileLayer);
                baseLayer.tileLayer._map.addLayer(baseLayer.tileLayer);

                baseLayer.tileLayer._map.options.minZoom = baseLayer.tileLayer.options.minZoom;
                baseLayer.tileLayer._map.options.maxZoom = baseLayer.tileLayer.options.maxZoom;
                if (baseLayer.tileLayer._map.options.maxZoom < baseLayer.tileLayer._map.getZoom()) {
                    baseLayer.tileLayer._map.setZoom(baseLayer.tileLayer._map.options.maxZoom);
                }
            } else {
                self._map.removeLayer(GLOBAL_CUSTOMCONTROL._currentBaseLayer.tileLayer);
                self._map.addLayer(baseLayer.tileLayer);

                self._map.options.minZoom = baseLayer.tileLayer.options.minZoom;
                self._map.options.maxZoom = baseLayer.tileLayer.options.maxZoom;
                if (self._map.options.maxZoom < self._map.getZoom()) {
                    self._map.setZoom(self._map.options.maxZoom);
                }
            }
            GLOBAL_CUSTOMCONTROL._currentBaseLayer = baseLayer;

        });
        div.appendChild(select);
        parentElement.appendChild(div);
    },

    _createBaseLayerItem: function (baseLayer) {
        var option = L.DomUtil.create("option", "");
        option.value = baseLayer.name;
        option.innerHTML = baseLayer.name;

        if (GLOBAL_CUSTOMCONTROL._currentBaseLayer) {
            if (GLOBAL_CUSTOMCONTROL._currentBaseLayer.name === baseLayer.name) {
                option.setAttribute("selected", "selected");
                if (!this._map.hasLayer(baseLayer.tileLayer)) {
                    this._map.addLayer(baseLayer.tileLayer);
                    this._currentBaseLayer = baseLayer;
                }
                
            }
        } else if (baseLayer.default) {
            option.setAttribute("selected", "selected");
            if (!this._map.hasLayer(baseLayer.tileLayer)) {
                this._map.addLayer(baseLayer.tileLayer);
                this._currentBaseLayer = baseLayer;
            }
        }
        return option;
    },

    _createOverlayerContainer: function (parentElement) {
        var self = this;
        var acordion = L.DomUtil.create("div", "customControl-acordionDiv");
        acordion.id = "customControlOverlay";

        Object.keys(self._overlayers).forEach(function (e) {
            var itemsGroup = self._overlayers[e];
            self._createOverLayerGroup(acordion, e, itemsGroup);
        });
        parentElement.appendChild(acordion);
        $(acordion).accordion({
            heightStyle: "content"
        });
    },

    _createOverLayerGroup: function (parentElement, groupName, itemsGroup) {
        var self = this;
        var span = L.DomUtil.create("span", "group");
        span.innerHTML = groupName;
        var navigate = L.DomUtil.create("i", "fa fa-arrows");
        $(navigate).click(function (e) {
            $(e.target).parent().toggleClass("active");
            self._navigate(groupName);
        });
        var remove = L.DomUtil.create("i", "fa fa-trash");
        $(remove).click(function (e) {
            $(e.target).parent().toggleClass("active");
            self._removeGroup(groupName);
        });
        span.appendChild(navigate);
        span.appendChild(remove);
        var divContent = L.DomUtil.create("div", "");
        itemsGroup.forEach(function (e) {
            var item = self._createOverlayerGroupItems(e.layer);
            divContent.appendChild(item);
        });
        span.id = "group" + groupName.replace(/\s/g, '');
        divContent.id = "content" + groupName.replace(/\s/g, '');
        parentElement.appendChild(span);
        parentElement.appendChild(divContent);
    },

    _createOverlayerGroupItems: function (layer) {
        var id = layer.options.group.replace(/\s/g, '') + "_" + layer.options.name.replace(/\s/g, '') + "_" + new Date().getTime();
        var divItem = L.DomUtil.create("div", "group-item");
        divItem.id = "layer_" + id.replace(/\s/g, '');
        var label = L.DomUtil.create("label", "");
        label.innerText = layer.options.name;
        label.setAttribute("for", id);

        var checkBox = L.DomUtil.create("input", "");
        checkBox.setAttribute("type", "checkbox");
        checkBox.setAttribute("group", layer.options.group);
        checkBox.setAttribute("layerName", layer.options.name);
        checkBox.setAttribute("leafletid", layer._leaflet_id);
        checkBox.id = id;
        if (layer.options.active) {
            checkBox.setAttribute("checked", "");
        }

        divItem.appendChild(checkBox);
        divItem.appendChild(label);

        if (layer.options.subGroup) {
            this._createOverlayerSubGroupItems(layer, layer.options.subGroup, divItem);
        }

        $(checkBox).click(function () {
            var l = null;
            var group = this.getAttribute("group");
            var name = this.getAttribute("layerName");
            var layers = GLOBAL_CUSTOMCONTROL._overlayers[group].filter(function (e) { return e.name == name });
            if (layers.length > 1) {
                var leafLetId = this.getAttribute("leafletid");
                var l = layers.find(function (e) { return e.layer._leaflet_id == leafLetId });
            } else {
                l = layers[0];
            }

            if (!l)
                console.error("Layer not found!");

            if (this.checked && l.layer.options.active) {
                GLOBAL_CUSTOMCONTROL._map.removeLayer(l.layer);
                GLOBAL_CUSTOMCONTROL._map.addLayer(l.layer);
                l.layer.options.active = true;
                $(this).siblings('ul').find('input').prop("checked", "checked");
                $(this).siblings('ul').find('input').attr("active", "true");
                return;
            }
            if (l.layer.options.active) {
                l.layer.options.active = false;
                GLOBAL_CUSTOMCONTROL._map.removeLayer(l.layer);
                $('li.subgroup input').prop("checked", "")
                $('li.subgroup input').attr("active", "false")
            } else {
                l.layer.options.active = true;
                GLOBAL_CUSTOMCONTROL._map.addLayer(l.layer);
                $('li.subgroup input').prop("checked", "checked");
                $('li.subgroup input').attr("active", "true");
            }
        });
        return divItem;
    },

    _createOverlayerSubGroupItems: function (layer, subGroup, parentElement) {
        if (subGroup.exclusive) {
            var select = L.DomUtil.create("select", "subgroup");
            subGroup.type.forEach(function (e) {
                var option = L.DomUtil.create("option", "");
                option.setAttribute("group", layer.options.group);
                option.setAttribute("layerName", layer.options.name);
                option.value = e.name;
                option.innerHTML = e.alias;
                select.appendChild(option);
            });
            $(select).change(function () {
                var value = this.value;
                if (subGroup.callback) {
                    subGroup.callback({
                        layer: layer,
                        type: this.value
                    });
                }
            });
            parentElement.appendChild(select);

        } else {
            var subGroupsFound = [];
            Object.keys(layer._layers).forEach(function (e) {
                var type = layer._layers[e].feature.properties[subGroup.type];
                if (subGroupsFound.indexOf(type) < 0) {
                    subGroupsFound.push(type);
                }
            });
            var ul = L.DomUtil.create("ul", "subgroup hidden");
            subGroupsFound.forEach(function (e) {
                var li = L.DomUtil.create("li", "subgroup");

                var id = layer.options.group.replace(/\s/g, '') + "_" + layer.options.name.replace(/\s/g, '') + "_" + e.replace(/\s/g, '');
                var label = L.DomUtil.create("label", "");
                label.innerText = e;
                label.setAttribute("for", id);

                var checkBox = L.DomUtil.create("input", "");
                checkBox.setAttribute("type", "checkbox");
                checkBox.setAttribute("group", layer.options.group);
                checkBox.setAttribute("layerName", layer.options.name);
                checkBox.setAttribute("subGroupItem", e);
                checkBox.setAttribute("active", layer.options.active)
                checkBox.id = id;
                if (layer.options.active) {
                    checkBox.setAttribute("checked", "");
                }

                $(checkBox).click(function () {
                    var group = this.getAttribute("group");
                    var name = this.getAttribute("layerName");
                    var item = this.getAttribute("subGroupItem");
                    var active = this.getAttribute("active");
                    var l = GLOBAL_CUSTOMCONTROL._overlayers[group].find(function (e) { return e.name == name });
                    if (!l)
                        console.error("Layer not found!");

                    Object.keys(l.layer._layers).forEach(function (e) {
                        var layer;
                        if (l.layer._layers[e].feature.properties[l.layer.options.subGroup.type] == item) {
                            if (active == "true") {
                                GLOBAL_CUSTOMCONTROL._map.removeLayer(l.layer._layers[e])
                            } else {
                                GLOBAL_CUSTOMCONTROL._map.addLayer(l.layer._layers[e])
                            }
                        }
                    });

                    if (active == "true") {
                        this.setAttribute("active", "false");
                        if ($(this).closest('ul').find('input[checked]:checked').length == 0) {
                            $("#" + group.replace(/\s/g, '') + "_" + name.replace(/\s/g, '')).prop("checked", "");
                        }
                    } else {
                        this.setAttribute("active", "true");
                        $("#" + group.replace(/\s/g, '') + "_" + name.replace(/\s/g, '')).prop("checked", "checked");
                    }
                    if (subGroup.callback) {
                        subGroup.callback(this);
                    }
                });
                li.appendChild(checkBox);
                li.appendChild(label);
                ul.appendChild(li);
            });
            var a = L.DomUtil.create("a", "subgroup fa fa-caret-down");
            parentElement.appendChild(a);
            parentElement.appendChild(ul);
            $(a).click(function () {
                $(ul).toggleClass("hidden");
            });
        }
    },

    _navigate: function (groupName) {
        var self = this;
        var bounds;
        Object.keys(self._overlayers[groupName]).forEach(function (e) {
            if (bounds)
                return;
            bounds = self._overlayers[groupName][e].layer.getBounds();
        });

        if (!bounds) return;

        this._map.fitBounds(bounds);
    },

    _removeGroup: function (groupName) {
        var self = this;

        if (!self._overlayers[groupName]) return;

        Object.keys(self._overlayers[groupName]).forEach(function (e) {
            var layer = self._overlayers[groupName][e].layer;
            layer.isGroupRemoved = true;
            layer.options.isRemoved = true;
            if (self._map) {
                self._map.removeLayer(layer);
            } else if (layer._map) {
                layer._map.removeLayer(layer);
            }
        });
        delete self._overlayers[groupName];
        $("#group" + groupName.replace(/\s/g, '')).remove();
        $("#content" + groupName.replace(/\s/g, '')).remove();
        self._updateAcordion();
    },

    _updateAcordion: function () {
        $(".customControl-acordionDiv").accordion("refresh");
    },

    _restoreLayers: function () {
        var map = this._map;
        if (GLOBAL_CUSTOMCONTROL.options.restoreLayers !== false) {
            var _overlayers = GLOBAL_CUSTOMCONTROL._overlayers;
            var bounds;
            var overLayerToRemove = [];
            Object.keys(_overlayers).forEach(function (_e) {
                var group = _overlayers[_e];
                group.forEach(function (e) {
                    if (e.layer.options.keepOnControl == false) {
                        overLayerToRemove.push({ layer: e, group: _e });
                        return;
                    }
                    e.layer.options.pointToLayer = map.context.getMarker;
                    var _layer = L.geoJson(e.layer.toGeoJSON(), e.layer.options);
                    if (_layer.getBounds) {
                        bounds = _layer.getBounds();
                    }
                    e.layer = _layer;
                    map.addLayer(_layer);
                });
            });
            if (overLayerToRemove.length > 0) {
                overLayerToRemove.forEach(function (l) {
                    var index = GLOBAL_CUSTOMCONTROL._overlayers[l.group].findIndex(function (e) {
                        return e == l.layer;
                    });
                    if (index > -1) {
                        GLOBAL_CUSTOMCONTROL._overlayers[l.group].splice(index, 1);
                    }
                    if (GLOBAL_CUSTOMCONTROL._overlayers[l.group].length == 0) {
                        delete GLOBAL_CUSTOMCONTROL._overlayers[l.group];
                    }
                });
            }
            if (bounds) {
                GLOBAL_CUSTOMCONTROL._currentBounds = bounds;
            }
        } else {
            var _overlayers = GLOBAL_CUSTOMCONTROL._overlayers;
            Object.keys(_overlayers).forEach(function (e) {
                GLOBAL_CUSTOMCONTROL._removeGroup(e);
            });
        }
    },

    _getCurrentBounds: function () {
        return this._currentBounds;
    },

    _getLoadedOverlayers: function () {
        return this._overlayers;
    },
});

L.customControl = function (opts) {
    let l = new L.Control.customControl(opts);
    if (opts.hidden != false) {
        //force initialize to register events;
        opts.map.addControl(l);
        //remve control for option
        opts.map.removeControl(l);
    }
    return l;
}