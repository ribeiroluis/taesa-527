﻿/// <reference path="websocket.client.js" />
/// <reference path="../vendor/jquery/jquery.min.js" />
/// <reference path="dom.js"/>
function _ConnectionUtilities() {
    var wsClient;
    var _onOpen;
    var _onMessage;
    var _onClose;
    var _onError;
    var _ajaxUrl;

    this.ws = undefined;

    this.successConnect = false;

    this.setOnMessage = function (onMessage) {
        if (onMessage) {
            _onMessage = onMessage;
            if (wsClient)
                wsClient.onMessage = _onMessage;
        } else
            return false;
    }
    this.getOnMessage = function () {
        return _onMessage;
    }

    this.setOnOpen = function (onOpen) {
        if (onOpen) {
            _onOpen = onOpen;
            if (wsClient)
                wsClient.onOpen = _onOpen;
        } else
            return false;
    }
    this.getOnOpen = function () {
        return _onOpen;
    }

    this.setOnClose = function (onClose) {
        if (onClose) {
            _onClose = onClose;
            if (wsClient)
                wsClient.onClose = _onClose;
        } else
            return false;
    }
    this.getOnClose = function () {
        return _onClose;
    }

    this.setOnError = function (onError) {
        if (onError) {
            _onError = onError;
            if (wsClient)
                wsClient.onError = _onError;
        } else
            return false;

    }
    this.getOnError = function () {
        return _onError;
    }

    this.setAjaxUrl = function (url) {
        //var ajaxUrl = document.location.origin + "/amqp/post";
        //var ajaxUrl = "http://localhost:3003/amqp/post";

        if (url)
            _ajaxUrl = url;
        else
            return false;
    }
    this.getAjaxUrl = function () {
        return _ajaxUrl;
    }

    this.GetWSKey = function () {
        var ajaxDataKey = undefined;
        if (wsClient !== undefined && wsClient.customKey !== undefined)
            ajaxDataKey = wsClient.customKey;

        return ajaxDataKey;
    }

    this.getWSUrl = function () {
        if (wsClient)
            return wsClient.getURL();
        else
            return undefined;
    }

    this.ensureWebSocketConnection = function (wsUrl, wsKey) {

        if (wsClient !== undefined && wsClient.getURL().indexOf(wsUrl) !== 0) {
            wsClient.disconnect();
            wsClient = undefined;
        }

        if (wsClient == undefined) {

            if (wsUrl.indexOf("/", wsUrl.length - 1) === -1)
                wsUrl += "/";

            wsClient = new WebSocketClient(wsUrl, 60000);
            wsClient.customKey = wsKey;
            wsClient.onOpen = _onOpen;
            wsClient.onMessage = _onMessage;
            wsClient.onClose = _onClose;
            wsClient.onError = _onError;

            this.successConnect = true;
        }

        if (wsClient.socketState !== wsClient.CONNECTED)
            wsClient.connect();

        this.ws = wsClient;
    }

    this.closeWebSocketConnection = function () {
        if (wsClient !== undefined)
            wsClient.disconnect();
    }

    this.sendMapTileProviderRequest = function (ajaxUrl, error, success) {
        try {
            $.ajax({
                type: "GET",
                url: ajaxUrl,
                error: error,
                success: success,
                async: false
            });
        } catch (err) { }

        return true;

    };

    this.sendAjaxRequest = function (ajaxUrl, ajaxType, ajaxData, successFunction, errorFunction, synchronizedRequest, contentType, requestTimeoutInMilliseconds, completeFunction) {
        if (!ajaxUrl)
            return false;

        if (!ajaxType)
            ajaxType = "GET";

        if (!requestTimeoutInMilliseconds || requestTimeoutInMilliseconds <= 0)
            requestTimeoutInMilliseconds = 30000;

        if (!contentType) {
            contentType = "application/json; charset=utf-8";
        }

        /* $.ajaxSetup({
             beforeSend: function (xhr) {
                 xhr.setRequestHeader('accepted-language', '"pt-BR');
             }
         });*/

        $.ajax({
            url: ajaxUrl,
            type: ajaxType,
            data: ajaxData,
            cache: false,
            crossDomain: true,
            success: successFunction ? successFunction : ajaxSuccesFunction,
            error: errorFunction ? errorFunction : ajaxErrorFunction,
            async: !synchronizedRequest,
            timeout: requestTimeoutInMilliseconds,
            contentType: contentType,
            complete: completeFunction,
            headers: {
                "Content-Language": "en-US",
                "Authorization": sessionStorage.getItem("authToken")
            }
        });

        return true;
    };

    this.sendFormData = function (ajaxUrl, formData, successFunction, errorFunction, progressCallback, fileObject, synchronizedRequest, requestTimeoutInMilliseconds, completeFunction) {

        if (!ajaxUrl)
            return false;

        if (!requestTimeoutInMilliseconds || requestTimeoutInMilliseconds <= 0)
            requestTimeoutInMilliseconds = 30000;

        $.ajax({
            url: ajaxUrl,
            type: "POST",
            data: formData,
            success: successFunction,
            error: errorFunction,
            complete: completeFunction,
            cache: false,
            async: !synchronizedRequest,
            timeout: requestTimeoutInMilliseconds,
            contentType: false,
            processData: false,
            headers: {
                "Authorization": sessionStorage.getItem("authToken")
            },
            xhr: function () {
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    myXhr.upload.addEventListener("progress", progressCallback, false);
                    myXhr.upload.fileObject = fileObject;
                }
                return myXhr;
            }
        });

        return true;
    };

    function ajaxErrorFunction(jqXHR, textStatus, errorThrown) {
        //console.error(jqXHR);
        if (jqXHR.responseJSON != null && jqXHR.responseJSON.Message != null) {
            domUtilities.showMessage("error", "Erro", jqXHR.responseJSON.Message);
        } else {
            domUtilities.showMessage("error", "Erro", jqXHR.statusText);
        }
    };

    function ajaxSuccesFunction(data) {
        console.info(data);
        domUtilities.showMessage("success", "Transação efetivada com sucesso!");
    };

    this.sendWebSocketRequest = function (message) {
        if (!wsClient) return;

        wsClient.sendMessage(message);
    };
}

var connectionUtilities = new _ConnectionUtilities();
