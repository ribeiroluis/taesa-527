﻿function WebSocketClient(WsUrl, KeepAliveTimeout) {

    if (!KeepAliveTimeout)
        KeepAliveTimeout = 120000;

    var connStateEnum = {
        CONNECTED: 1,
        CLOSED: 2
    };
    var reconnection = false;
    var ws;
    var wsUrl = WsUrl;
    
    var _onOpen = function (evt) {
        reconnection = false;
        setTimeout(function () { _ping(); }, publicSession.keepAliveTimeout);

        publicSession.socketState = connStateEnum.CONNECTED;
        if (publicSession.onOpen != undefined)
            publicSession.onOpen(evt);
    }
    var _onMessage = function (evt) {

        ///teste
        if (evt.data == "CONNECTED")
            console.log(evt.data);


        if (evt.data === undefined || !evt.data)
            return;

        if (publicSession.onMessage != undefined)
            publicSession.onMessage(evt.data);
    }
    var _onClose = function (evt) {
        if (publicSession.socketState == connStateEnum.CONNECTED) {
            reconnection = true;
            setTimeout(function () { _reconnect(); }, 1000);
        }

        publicSession.disconnect();
        if (publicSession.onClose != undefined)
            publicSession.onClose(evt);
    }
    var _onError = function (evt) {
        if (reconnection) {
            setTimeout(function () { _reconnect(); }, 5000);
            return;
        }

        if (publicSession.onError != undefined)
            publicSession.onError(evt);
    }
    var _ping = function () {
        if (publicSession.socketState == connStateEnum.CONNECTED) {
            if (ws == undefined || ws.readyState != ws.OPEN) {
                publicSession.connect();
                return;
            }

            setTimeout(_ping, publicSession.keepAliveTimeout);
            ws.send('');
        }
    }
    var _reconnect = function () {
        if (reconnection) {
            publicSession.connect();
        }
    }
    
    var publicSession = {
        keepAliveTimeout: KeepAliveTimeout,
        CONNECTED: connStateEnum.CONNECTED,
        CLOSED: connStateEnum.CLOSED,
        socketState: connStateEnum.CLOSED,
        customKey: undefined,

        onOpen: function onOpen(evt) { },
        onMessage: function onMessage(message) { },
        onClose: function onClose(evt) { },
        onError: function onError(evt) { },

        connect: function () {
            if (ws == undefined || ws.readyState != ws.OPEN) {
                ws = new window.WebSocket(wsUrl);
                ws.onopen = function (evt) { _onOpen(evt); };
                ws.onclose = function (evt) { _onClose(evt); };
                ws.onmessage = function (evt) { _onMessage(evt); };
                ws.onerror = function (evt) { _onError(evt); };
            }
        },
        disconnect: function () {
            publicSession.socketState = connStateEnum.CLOSED;

            if (ws != undefined && ws.readyState == ws.OPEN) {
                ws.close();
                ws = undefined;
            }

            reconnection = false;
        },
        sendMessage: function (message) {
            if (ws == undefined || ws.readyState != ws.OPEN)
                throw new Error("You have to open the connection before sending a message!");

            ws.send(message);
        },
        checkSupported: function () {
            return window.WebSocket;
        },
        getURL: function () {
            return wsUrl;
        }
    };

    return publicSession;
}