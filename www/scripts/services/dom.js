function _DomUtilities() {
    this._configToastr = function () {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "8000",
            "hideDuration": "1000",
            "timeOut": "8000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "showNotification": true
        };
    }();

    this.setStatusDataInfo = function (message) {
        if (!message)
            return;

        const flagStatus = this.findElement("#flagStatus");
        this.removeCssClasses(flagStatus,
            ["glyphicon", "glyphicon-remove-sign", "glyphicon-warning-sign", " glyphicon-ok-sign", "error", "warning", "ok"]);
        this.addCssClasses(flagStatus, ["glyphicon", "glyphicon-info-sign", "info"]);
        const messageStatus = this.findElement("#messageStatus");
        messageStatus.html(message);
    };

    this.setStatusDataSuccess = function (message) {
        if (!message)
            return;

        const flagStatus = this.findElement("#flagStatus");
        this.removeCssClasses(flagStatus,
            ["glyphicon", "glyphicon-remove-sign", "glyphicon-warning-sign", " glyphicon-info-sign", "error", "warning", "info"]);
        this.addCssClasses(flagStatus, ["glyphicon", "glyphicon-ok-sign", "ok"]);
        const messageStatus = this.findElement("#messageStatus");
        messageStatus.html(message);
    };

    this.setStatusDataWarning = function (message) {
        if (!message)
            return;

        const flagStatus = this.findElement("#flagStatus");
        this.removeCssClasses(flagStatus,
            ["glyphicon", "glyphicon-remove-sign", "glyphicon-ok-sign", " glyphicon-info-sign", "error", "ok", "info"]);
        this.addCssClasses(flagStatus, ["glyphicon", "glyphicon-warning-sign", "warning"]);
        const messageStatus = this.findElement("#messageStatus");
        messageStatus.html(message);
    };

    this.setStatusDataError = function (message) {
        if (!message)
            return;

        const flagStatus = this.findElement("#flagStatus");
        this.removeCssClasses(flagStatus,
            ["glyphicon", "glyphicon-warning-sign", "glyphicon-ok-sign", " glyphicon-info-sign", "warning", "ok", "info"]);
        this.addCssClasses(flagStatus, ["glyphicon", "glyphicon-remove-sign", "error"]);
        const messageStatus = this.findElement("#messageStatus");
        messageStatus.html(message);
    };

    this.showMessage = function (type, title, text, options) {

        if (toastr.options.showNotification) {
            switch (type.toUpperCase()) {
                case "ERROR":
                    toastr.error(text, title, options);
                    break;
                case "SUCCESS":
                    toastr.success(text, title, options);
                    break;
                case "INFO":
                    toastr.info(text, title, options);
                    break;
                case "WARNING":
                    toastr.warning(text, title, options);
                    break;
                default:
            }
        }
    };

    this.showLoading = function (elementContainerOrSelector, loadingText) {
        if (!loadingText)
            loadingText = "Loading...";

        // $(elementContainerOrSelector).isLoading({
        //     'position': "overlay",        // right | inside | overlay
        //     'text': loadingText,                 // Text to display next to the loader
        //     'class': "icon-refresh",    // loader CSS class
        //     'tpl': '<span class="isloading-wrapper %wrapper%">%text%<i class="%class% icon-spin"></i></span>',
        //     'disableSource': true,      // true | false
        //     'disableOthers': []
        // });
        // var layer = document.createElement("div");
        // layer.setAttribute("style", "backgroud-color:rgb(0,0,0); width: 100%; height: 100%");
        // $(elementContainerOrSelector).prepend(layer);

        //$(elementContainerOrSelector).isLoading({
        //    text: loadingText,
        //    //disableOthers: [$("#widgetSprint input[id='btnSearchSprint']")],
        //    position: "overlay"
        //});
    };

    this.hideLoading = function (elementContainerOrSelector) {
        $(elementContainerOrSelector).isLoading("hide");
    };

    this.changeClassViewMode = function (element, style, currentContent) {
        const ul = this.findAncestor(element, "ul");
        ul.find(".active").toggleClass("active");
        $(element).toggleClass("active");
        if (style) {
            element = $("#tabContent_" + currentContent).find(".MainTabContent > .tab-content")
            const activeClass = element.attr("class").split(" ");
            this.removeCssClasses(element, [activeClass[1], activeClass[2]]);
            this.addCssClasses(element, [style, "ng-scope"]);
        }
    };

    this.toggleToolbarHeight = function (element) {
        $(element).parent().css("overflow", "hidden");
        $(element).parent().toggleClass("reduced");

        const widgetContent = $(element).closest(".MainTabPainel").find("div[ng-include='']");
        widgetContent.toggleClass("WidgetContentReduced");
        widgetContent.toggleClass("WidgetContent");
        setTimeout(function () {
            $(element).parent().css("overflow", "no-display");
        }, 250);

    };

    this.toggleFullScreenWidget = function (element) {
        if (!element)
            return;

        const parentElement = element.parentElement.parentElement.parentElement.parentElement;

        if (!document.webkitIsFullScreen) {
            if (parentElement.webkitRequestFullScreen) {
                parentElement.webkitRequestFullScreen(parentElement.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            document.webkitCancelFullScreen();
        }

        var currentElement;
        document.addEventListener("webkitfullscreenchange", function () {
            if (document.webkitIsFullScreen) {
                currentElement = document.webkitCurrentFullScreenElement;
                $(element).find("i").removeClass("icon-full-screen");
                $(element).find("i").addClass("icon-exit-full-screen");
                $(element).find("b").text("Visualização normal");
                $(currentElement).addClass("full-screen-element");
            } else {
                $(currentElement).removeClass("full-screen-element");
                $(element).find("i").removeClass("icon-exit-full-screen");
                $(element).find("i").addClass("icon-full-screen");
                $(element).find("b").text("Tela cheia");

            }

        }, false);
    };

    this.createElement = function (elementType, classesArray, attributesObject) {
        const element = document.createElement(elementType);
        this.addCssClasses(element, classesArray);

        if (attributesObject && attributesObject instanceof Object) {
            const keys = Object.getOwnPropertyNames(attributesObject);

            for (let keysIndex = 0; keysIndex < keys.length; keysIndex++) {
                const key = keys[keysIndex];
                const value = attributesObject[key];
                element.setAttribute(key, value);
            }
        }

        return element;
    };

    this.includeContentFromFile = function (containerSelector, fileContentPath) {
        var fileContent;
        $.ajax({
            method: "GET",
            url: fileContentPath,
            async: false,
            success: function (data) {
                if (data)
                    fileContent = data;
            }
        });

        const currentContent = $(containerSelector).html();
        $(containerSelector).html(currentContent + fileContent);
        return $(containerSelector);
    };

    this.showElement = function (elementSelector, options) {
        $(elementSelector).show(options);
    };

    this.hideElement = function (elementSelector, options) {
        $(elementSelector).hide(options);
    };

    this.createElementA = function (href, text, classesArray, attributesObject) {
        const a = this.createElement("a", classesArray, attributesObject);

        if (href)
            a.href = href;

        if (text)
            a.innerHTML = text;

        return a;
    };

    this.createElementLabel = function (content) {
        const label = this.createElement("label");

        if (content)
            label.innerHTML = content;

        return label;
    };

    this.createElementSpan = function (content) {
        const span = this.createElement("span");

        if (content)
            span.innerHTML = content;

        return span;
    };

    this.removeElement = function (element) {
        if (!element) {
            return;
        }
        $(element).remove();
    };

    this.getElementChildren = function (element) {
        return $(element).children();
    };

    this.getFirstElementOfGroup = function (element) {
        return $(element).first();
    };

    this.setInnerHTMLElement = function (element, innerHTML) {
        if (!element)
            return;
        innerHTML = (innerHTML == undefined ? "" : innerHTML);
        $(element).html(innerHTML);
    };

    this.setInnerText = function (element, text) {
        if (!element)
            return;
        text = (text == undefined ? "" : text);
        $(element).text(text);
    };

    this.getInnerHTMLElement = function (element, html) {
        if (!element || !html)
            return;
        return $(element).html();
    };

    this.addChildAsLast = function (element, lastChild) {
        if (!element || !lastChild)
            return;

        $(element).append(lastChild);
    };

    this.addSiblingAfter = function (element, siblingAfter) {
        if (!element || !siblingAfter)
            return;

        $(element).after(siblingAfter);
    };

    this.addChildAsFirst = function (element, firstChild) {
        if (!element || !firstChild)
            return;

        $(element).prepend(firstChild);
    };

    this.addCssClasses = function (element, classesArray) {
        if (!element || !classesArray || !(classesArray instanceof Array))
            return;

        for (let classIndex = 0; classIndex < classesArray.length; classIndex++) {
            $(element).addClass(classesArray[classIndex]);
        }
    };

    this.removeCssClasses = function (element, classesArray) {
        if (!element || !classesArray || !(classesArray instanceof Array))
            return element;

        for (let classIndex = 0; classIndex < classesArray.length; classIndex++) {
            $(element).removeClass(classesArray[classIndex]);
        }

        return element;
    };

    this.addCssClassesToElements = function (elementArray, classesArray) {
        for (let elementIndex = 0; elementIndex < elementArray.length; elementIndex++) {
            this.addCssClasses(elementArray[elementIndex], classesArray);
        }
        return true;
    };

    this.removeCssClassesFromElements = function (elementArray, classesArray) {
        for (let elementIndex = 0; elementIndex < elementArray.length; elementIndex++) {
            this.removeCssClasses(elementArray[elementIndex], classesArray);
        }
        return true;
    };

    this.hasClass = function (element, classToFind) {
        if (!element || !classToFind)
            return;

        return $(element).hasClass(classToFind);
    };

    this.createElementInput = function (type, inputId, value) {
        const input = this.createElement("input");

        if (type)
            input.type = type;

        if (inputId)
            input.id = inputId;

        if (value)
            input.value = value;

        return input;
    };

    this.findElement = function (elementSelector) {
        return $(elementSelector);
    };

    this.findAncestor = function (element, ancestorSelector) {
        if (!element || !ancestorSelector)
            return [];

        return $(element).closest(ancestorSelector);
    };

    this.findDescendant = function (element, descendantSelector) {
        if (!element || !descendantSelector)
            return [];
        return $(element).find(descendantSelector);
    };

    this.findSiblings = function (element, filter) {
        if (!element) {
            return;
        }
        return $(element).siblings(filter);
    };

    this.triggerEvent = function (elementSelector, eventToTrigger) {
        $(elementSelector).trigger(eventToTrigger);
    };

    this.setAttribute = function (element, attributeName, attributeValue) {
        if (!element || !attributeName)
            return;

        $(element).attr(attributeName, attributeValue);
    };

    this.getAttribute = function (element, attributeName) {
        if (!element || !attributeName)
            return "";

        return $(element).attr(attributeName);
    };

    this.getHeight = function (element) {
        return $(element).height();
    };

    this.scrollElement = function (element, type, pixels) {
        switch (type) {
            case "top": element.scrollTop = pixels; break;
            case "left": element.scrollLeft = pixels; break;
        }
    };

    this.getCssStyleAttribute = function (element, attributeKey) {
        if (!element || !attributeKey)
            return "";

        return $(element).css(attributeKey);
    };

    this.setCssStyleAttribute = function (element, attributeKey, attributeValue) {
        if (!element || !attributeKey)
            return;

        $(element).css(attributeKey, attributeValue);
    };

    this.removeAttribute = function (element, attributeName) {
        if (!element || !attributeName)
            return "";

        $(element).removeAttr(attributeName);
    };

    this.bindEvent = function (element, event, callBack) {
        if (!element || !event)
            return;

        $(element).bind(event, callBack);
    };

    this.unbindEvent = function (element, event) {
        if (!element || !event)
            return "";

        $(element).unbind(event);
    };

    this.focusElement = function (element) {
        if (!element)
            return "";

        $(element).focus();

    };

    this.sortableElement = function (element, options) {
        if (!element || !options)
            return;

        element.sortable(options);
    };

    this.animateElement = function (element, properties, duration, easing, complete) {
        if (!element)
            return;

        duration = (duration == undefined ? 400 : duration);
        easing = (easing == undefined ? "swing" : easing);
        element.animate(properties, duration, easing, complete);
        return true;
    };

    this.onDocumentReady = function (callBack) {
        $(document).ready(function () {
            if (callBack) {
                callBack();
            }
        });
    };

    this.downloadFileOnBrowser = function (element, contentType, content, fileName) {
        const blobFile = new Blob([content], { type: contentType });
        const url = URL.createObjectURL(blobFile);
        const a = this.createElementA(url, undefined, undefined, {
            style: "display: none",
            download: fileName
        });
        a.dataset.downloadurl = [contentType, a.download, a.href].join(":");
        this.addChildAsLast(element, a);
        a.click();
        URL.revokeObjectURL(url);
        a.remove();
    };

    this.throwAlertNotification = function () {
        $(".badge").addClass("newNotification");
        setTimeout(function () {
            $(".badge").removeClass("newNotification");
        }, 1000);
    };

    this.checkNotificationClass = function (type) {
        switch (type.toLowerCase()) {
            case "info": return "alert-info";
            case "success": return "alert-success";
            case "error": return "alert-danger";
            case "warning": return "alert-warning";

        }
    };

    this.showModal = function (elementSelector) {
        if (!elementSelector) return;

        $(elementSelector).modal("show");
    };

    this.hideModal = function (elementSelector) {
        if (!elementSelector) return;

        $(elementSelector).modal("hide");
    };

    this.enableAutoCompleteOnElement = function (elementSelector, data, key, callback, scope) {
        if (!elementSelector || !data || !key) return;
        $(elementSelector).keydown(function (event) {
            if (event.keyCode === 39 || event.keyCode === 37 || event.keyCode === 13) {
                if (callback)
                    callback(event.currentTarget.value);
            }
        });
        const options = {
            data: data,
            getValue: key,
            list: {
                onClickEvent: function () {
                    if (callback)
                        callback($(elementSelector).getSelectedItemData()[key]);
                }.bind(this),
                maxNumberOfElements: 10,
                sort: {
                    enabled: true
                },
                match: {
                    enabled: true
                }
            }
        };
        $(elementSelector).easyAutocomplete(options);
    }

    this.toggleClass = function (elementSelector, classesArray) {
        if (!elementSelector || !classesArray || !(classesArray instanceof Array))
            return;
        $(elementSelector).toggleClass(classesArray.toString().replace(/,/g, " "));
    }

    /**
     * Show confirmation modal
     * @param {title} Title of dialog
     * @param {message} Message of dialog
     * @parm {options} options for dialog
     * @parm {buttons} buttons for dialog {label: "", cssClass: "", action: callBackFunciton}
    */
    this.showConfirmDialog = function(title, message, options, buttons, yesCallBack, noCallBack) {
        try {
            if (!BootstrapDialog) {
                const r = confirm(message);
                if (r) {
                    yesCallBack();
                }
                noCallBack();
                return;
            }
            let type = BootstrapDialog.TYPE_DEFAULT;
            let draggable = true;
            if (options) {
                if (options.type.toUpperCase())
                    switch (options.type.toUpperCase()) {
                    case "INFO":
                        type = BootstrapDialog.TYPE_INFO;
                        break;
                    case "PRIMARY":
                        type = BootstrapDialog.TYPE_PRIMARY;
                        break;
                    case "SUCCESS":
                        type = BootstrapDialog.TYPE_SUCCESS;
                        break;
                    case "WARNING":
                        type = BootstrapDialog.TYPE_WARNING;
                        break;
                    case "DANGER":
                        type = BootstrapDialog.TYPE_DANGER;
                        break;
                    default:
                        type = BootstrapDialog.TYPE_DEFAULT;
                        break;
                    }
                draggable = options.draggable !== undefined ? options.closable : true;
            }
            if (!buttons) {
                buttons = [
                    {
                        icon: "glyphicon glyphicon-ban-circle",
                        label: "defatult button",
                        cssClass: "btn-primary",
                        title: "button default title",
                        autospin: true,
                        hotkey: 13, // Enter.
                        action: function(dialogRef) {
                            dialogRef.close();
                            console.info(dialogRef);
                        }
                    }
                ];
            }
            BootstrapDialog.show({
                title: title,
                message: message,
                type: type,
                closable: false,
                draggable: draggable,
                buttons: buttons
            });
        } catch (e) {
            throw new Error("Error on try create BootstrapDialog");
        }
    };
}

var domUtilities = new _DomUtilities();
