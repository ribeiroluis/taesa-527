function MapService() {
    this.name = "MapService for LeafLet";
    this.version = "1.0.2";
    this.map = null;
    this.options = {
        zoomControl: false,
        minZoom: 0,
        maxZoom: 20,
        setView: true
    };
    this.properties = {
        addOn: {},
        customLayerCallback: undefined,
        defaultBaseLayer: null,
    };

    this.loadMap = function (mapContainer, addOnsOptions, afterInitializeMap) {
        this.map = new L.Map(mapContainer, {
            zoomControl: this.options.zoomControl,
            minZoom: this.options.minZoom,
            maxZoom: this.options.maxZoom,
            zoom: this.options.maxZoom / 2
        }).locate({ setView: this.options.setView });

        //Guarda o contexto para utilizações futuras
        this.map.context = this;

        this.baseLayers = loadTiles(this);

        //Adiciona o controle de camadas no mapa
        this.loadControlLayers(this.map);
        //Adiciona/carga o plugins no mapa
        loadAddOns(this, addOnsOptions);

        if (afterInitializeMap)
            this.map.on("load", afterInitializeMap);

        return this.map;
    };

    this.addLayerWithoutControl = function (layer) {
        if (!layer)
            throw "No layer found to be added";

        this.map.addLayer(layer);
    };

    this.addLayerOnMap = function (geoJsonObject, type, name, group, subGroup, active, groupLayers, fitBounds, keepOnControl, addOnControl) {
        if (!geoJsonObject || !type || !name || !group) {
            throw "The layer can't be added! geoJsonObject: " + geoJsonObject + ", name: " + name + ", group: " + group;
        }

        var layer;

        var _layer = L.geoJson(geoJsonObject, {
            pointToLayer: this.getMarker,
            style: this.getStyle,
            onEachFeature: this.getOnEachFeature,
            active: active,
            type: type,
            name: name,
            group: group,
            subGroup: subGroup,
            addOnControl: addOnControl != undefined ? addOnControl : true,
            keepOnControl: keepOnControl
        });

        if (groupLayers) {
            layer = L.markerClusterGroup();
            layer.addLayer(_layer);
        } else {
            layer = _layer;
        }


        if (groupLayers == undefined) {
            groupLayers = false;
        }

        this.map.addLayer(layer);
        if (fitBounds !== false) {
            this.fitBounds(layer);
        }
        return layer;
    };

    this.getLayersByFeaturetype = function (type) {
        var layers = [];
        var layersOnMap = this.map._layers;
        Object.keys(layersOnMap).forEach(function (e) {
            if (layersOnMap[e].feature && layersOnMap[e].feature.properties.equipmentType == type)
                layers.push(layersOnMap[e]);
        });
        return layers;
    }

    this.getStyle = function (feature) {
        if (feature.geometry.type === "Point") {
            //return;
        }
        var object;
        if (this.properties.customLayerCallback) {
            object = this.properties.customLayerCallback(feature);
        }
        if (!object) {
            return;
        }
        return object.style;
    }.bind(this);

    this.registerEventOnMap = function (event, callbackFunction) {
        this.map.on(event, callbackFunction);
    };

    this.addCircleMarkerAsGeoJSON = function (geojson, options) {
        var layer = L.geoJson(geojson, {
            pointToLayer: function (feature, latlng) {
                return L.circleMarker(latlng, options(feature));
            }
        })
        layer.addTo(this.map);
        return layer;
    };

    this.addCircleMarker = function (latlng, _options) {
        if (!_options) {
            _options = {};
        }

        var options = {
            fillColor: _options.fillColor == undefined ? "yellow" : _options.fillColor,
            weight: _options.weight == undefined ? 1 : _options.weight,
            fillOpacity: _options.fillOpacity == undefined ? 1 : _options.fillOpacity,
            className: _options.className == undefined ? "" : _options.className,
            color: _options.fillColor == undefined ? "yellow" : _options.fillColor
        };
        var latlngObject = L.latLng(latlng);
        var circleMarker = L.circleMarker(latlngObject, options);
        circleMarker.isFakeLayer = true;
        circleMarker.addTo(this.map);
        this.map.setView(latlngObject);
        return circleMarker;
    };

    this.addRectangleAndFitBounds = function (bounds, options) {
        if (!bounds[0] instanceof Array || !bounds[1] instanceof Array) {
            throw "Failed to read bounds: " + bounds;
        }
        // create an red rectangle
        var layer = L.rectangle(bounds, options).addTo(this.map);
        // zoom the map to the rectangle bounds
        this.fitBounds(layer);
    };

    this.removeLayerWithoutControl = function (layer) {
        this.map.removeLayer(layer);
    };

    this.removeLayer = function (layer) {
        this.map.removeLayer(layer);
    };

    this.fitBoundsInExistLayersOnMap = function () {
        var self = this.map._layers;
        var latlng = [];
        Object.keys(this.map._layers).forEach(function (e) {
            if (self[e]._latlngs) {
                latlng.push([self[e]._latlngs[0].lat, self[e]._latlngs[0].lng]);
            } else if (self[e]._latlng) {
                latlng.push([self[e]._latlng.lat, self[e]._latlng.lng]);
            }
        });

        if (latlng.length > 0) {
            this.fitBounds(undefined, latlng);
        }
    }

    this.fitBounds = function (layer, bounds) {
        if (!layer && !bounds) {
            throw "The layer can't be found! name: " + name + ", layer: " + layer + ", bounds: " + bounds;
        }
        if (layer) {
            if (layer.getBounds) {
                this.map.fitBounds(layer.getBounds());
            } else {
                this.map.setView(layer.getLatLng(), 18);
            }
            return true;
        } else if (bounds) {
            if (!bounds[0] instanceof Array || !bounds[1] instanceof Array) {
                throw "Failed to read bounds: " + bounds;
            }
            this.map.fitBounds(L.latLngBounds(bounds));
        }
    }.bind(this);

    this.createBounds = function (listPoints) {
        var bounds = L.bounds(listPoints);
        return [[bounds.max.x, bounds.max.y], [bounds.min.x, bounds.min.y]];
    };

    this.panTo = function (lat, lng) {
        if (lat == undefined || lng == undefined) return;

        this.map.panTo([lat, lng]);
    };

    this.centerMapAfterResize = function () {
        this.map.invalidateSize();
    }

    this.createDivIcon = function (className, html) {
        return new L.divIcon({
            className: className,
            html: html
        });
    };

    this.getMarker = function (feature, latlng) {
        if (feature.geometry.type === "LineString") {
            return;
        }
        var object;
        if (this.properties.customLayerCallback) {
            object = this.properties.customLayerCallback(feature);
        }
        if (!object) {
            return L.marker(latlng);
        }

        var marker;
        if (object.circleMarker) {
            var options = {
                fillColor: object.circleMarker.fillColor == undefined ? "yellow" : object.circleMarker.fillColor,
                weight: object.circleMarker.weight == undefined ? 1 : object.circleMarker.weight,
                fillOpacity: object.circleMarker.fillOpacity == undefined ? 1 : object.circleMarker.fillOpacity,
                className: object.circleMarker.className == undefined ? "" : object.circleMarker.className,
                color: object.circleMarker.fillColor == undefined ? "yellow" : object.circleMarker.fillColor,
                radius: object.circleMarker.radius == undefined ? 10 : object.circleMarker.radius,
            };
            //marker = L.circleMarker(L.latLng(latlng), options);
            return L.circle([latlng.lat, latlng.lng], 110, options);
        }
        if (object.markerHTML) {
            marker = L.marker(latlng,
                {
                    icon: new L.divIcon({
                        className: object.className,
                        html: object.markerHTML
                    }),
                    draggable: object.draggable
                });
        }
        if (!marker) {
            marker = L.marker(latlng);
        }


        if (marker.options.draggable) {
            marker.on("dragend", object.draggableCallBack);
        }
        if (object.bindPopUp) {
            if (object.popUpHtml) {
                marker.bindPopup(object.popUpHtml);
            } else {
                marker.bindPopup("I'm a poupup");
            }
        }
        if (object.clickCallback) {
            marker.on("click", object.clickCallback);
        } else {
            marker.on("click", function (e) { console.info(e) });
        }
        return marker;
    }.bind(this);

    this.addAddOn = function (addOn, options, callback) {
        if (!addOn) {
            throw "The AddOn can't be added! addOn: " + addOn;
        }
        switch (this.properties.addOn[addOn].type) {
            case "control": this.map.addControl(this.properties.addOn[addOn].value); break;
            case "layer": this.map.addLayer(this.properties.addOn[addOn].value); break;
        }
        if (callback)
            this.properties.addOn[addOn].callback = callback;
    };

    this.removeAddOn = function (addOn) {
        if (!addOn) {
            throw "The AddOn can't be removed! addOn: " + addOn;
        }
        switch (this.properties.addOn[addOn].type) {
            case "control": this.map.removeControl(this.properties.addOn[addOn].value); break;
            case "layer": this.map.removeLayer(this.properties.addOn[addOn].value); break;
        }
    };

    function loadTiles(object) {
        var OpenStreetMap = L.tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            {
                attribution: "OpenStreetMap",
                "minZoom": 0,
                "maxZoom": 20
            });

        // var Localtiles = L.tileLayer(appConfig.localTileAddress,
        //     {
        //         "attribution": "Visiona®2017",
        //         "tms": true,
        //         "minZoom": 0,
        //         "maxZoom": 19
        //     });



        var MapBox = L.tileLayer(
            "https://api.mapbox.com/v4/mapbox.streets-satellite/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoicmliZWlyb2x1aXMiLCJhIjoiY2lraWpxcHcyMDQ1YXYwa20wOGY1eXRibiJ9.q6wTsFeM8jiHqBBSeRKfaA",
            {
                "attribution": "MapBox",
                "minZoom": 0,
                "maxZoom": 17
            });

        object.properties.defaultBaseLayer = OpenStreetMap;

        return [
            { tileLayer: OpenStreetMap, name: "OpenStreetMap", "default": false },
            //{ tileLayer: Localtiles, name: "Visiona®2017", "default": false },
            { tileLayer: MapBox, name: "MapBox", "default": true },
        ];
    };

    function loadAddOns(object, options) {

        //Prepara plugin Radar - não adiciona no mapa automaticamente
        if (L.Control.MiniMap) {
            var radarLayer = new L.tileLayer(object.properties.defaultBaseLayer._url, object.properties.defaultBaseLayer.options);
            object.properties.addOn.radar = {
                value: new L.Control.MiniMap(radarLayer, { minZoom: 0, maxZoom: 13 }),
                type: "control"
            };
        }

        ///Prepara o plugin de zoom  - adiciona no mapa automaticamente
        if (L.control.zoom) {
            object.properties.addOn.controlZoom = {
                value: L.control.zoom({ position: "bottomright" }),
                type: "control"
            };
            object.map.addControl(object.properties.addOn.controlZoom.value);
        }

        //Prepara o plugin de delimitadores - não adiciona no mapa automaticamente
        if (L.Control.Draw) {
            if (!options) {
                options = {
                    draw: { polygon: false, marker: false, circle: false, polyline: false },
                    allowMultipleLayers: false
                };
            }
            var drawnItems = new L.FeatureGroup();
            var drawControl = new L.Control.Draw({
                draw: options.draw,
                position: "topright",
                edit: { featureGroup: drawnItems }
            });
            object.properties.addOn.draw = {
                value: drawControl,
                type: "control",
                drawItems: drawnItems,
                allowMultipleLayers: options.allowMultipleLayers,
                callback: object.properties.onDrawCallBack
            };
            object.map.on(L.Draw.Event.CREATED, function (e) {
                var draw = e.target.context.properties.addOn.draw;
                if (!draw.drawItems.isOnMap) {
                    e.target.addLayer(draw.drawItems);
                    draw.drawItems.isOnMap = true;
                }
                var layer = e.layer;
                if (!draw.allowMultipleLayers) {
                    Object.keys(draw.drawItems._layers).map(function (e) {
                        draw.drawItems.removeLayer(draw.drawItems._layers[e]);
                        if (draw.callback) {
                            draw.callback({ event: "removed", layer: layer });
                        }
                    });
                }
                draw.drawItems.addLayer(layer);
                if (draw.callback) {
                    draw.callback(layer);
                }
            });
            object.map.on(L.Draw.Event.DELETED, function (e) {
                var draw = e.target.context.properties.addOn.draw;
                if (draw.callback) {
                    draw.callback({ event: "deleted", layer: e });
                }
            });
            object.map.on(L.Draw.Event.EDITED, function (e) {
                var draw = e.target.context.properties.addOn.draw;
                var layers = e.layers;
                layers.eachLayer(function (layer) {
                    if (draw.callback) {
                        draw.callback(layer);
                    }
                });
            });
            //drawControl.addTo(object.map);
        }

        //Prepara o plugin de coordenadas - não adiciona no mapa automaticamente
        if (L.control.coordinates) {

            object.properties.addOn.coordinates = {
                value: L.control.coordinates({
                    position: "bottomleft",
                    decimals: 3,
                    decimalSeperator: ".",
                    labelTemplateLat: "Latitude: {y}",
                    labelTemplateLng: "Longitude: {x}",
                    enableUserInput: true,
                    useDMS: false,
                    useLatLngOrder: true
                }),
                type: "control"
            };
            //object.map.addControl(object.properties.addOn.coordinates.value);
        }

        //Prepara o plugin lupa - não adiciona no mapa automaticamente
        if (L.magnifyingGlass) {
            var magnifyingGlassLayer = new L.tileLayer(object.properties.defaultBaseLayer._url, object.properties.defaultBaseLayer.options);
            object.properties.addOn.magnifyingGlass = {
                value: L.magnifyingGlass({ layers: [magnifyingGlassLayer] }),
                type: "layer"
            };
        }

        //Prepara o plugin de escala de mapa - não adiciona no mapa automaticamente
        if (L.control.scale) {
            object.properties.addOn.scale = {
                value: L.control.scale({ position: "bottomright", imperial: false }),
                type: "control"
            };
        }

    };

    this.createLayer = function (layerToCreate, options) {
        return L.geoJson(layerToCreate, options);
    }

    this.removeLayerFromGroup = function (layer, group, layerType) {
        if (!layer || !group || !layerType) {
            throw "Não foi possível inserir a layer ao grupo";
        }

        var groupLayer = this.getOverlayersLoadedFromControl()[group];

        var groupType = groupLayer.find(function (element, index, array) {
            return element.name == layerType;
        });

        if (!groupType) {
            return;
        }
        groupType.layer.removeLayer(layer);

        if (groupType.layer.getLayers().length == 0) {
            var index = groupLayer.findIndex(function (element, index, array) {
                return element == groupType;
            });
            groupLayer.splice(index, 1);
        }

        return layer;
    };

    this.addLayerOnGroup = function (layer, group, layerType) {

        if (!layer || !group || !layerType) {
            throw "Não foi possível inserir a layer ao grupo";
        }

        var groupLayer = this.getOverlayersLoadedFromControl()[group];

        var groupType = groupLayer.find(function (element, index, array) {
            return element.name == layerType;
        });

        if (!groupType) {
            groupType = groupLayer.push({ "name": layerType, "layer": L.layerGroup() });
        }

        groupType.layer.addLayer(layer);

        return layer;
    };

    this.removeGroupFromControlAndMap = function (group) {
        this.properties.addOn.controlOverLayers.value._removeGroup(group);
    };

    this.getOverlayersLoadedFromControl = function () {
        return this.properties.addOn.controlOverLayers.value._getLoadedOverlayers();
    };

    this.getCurrentBoundsFromControl = function () {
        return this.properties.addOn.controlOverLayers.value._getCurrentBounds();
    };

    this.loadControlLayers = function (map) {
        if (L.customControl) {
            this.properties.addOn.controlOverLayers = {
                value: L.customControl({
                    type: "overlayer",
                    overLayers: this.overLayers,
                    map: map,
                    container: "#page-wrapper",
                    hiddden: true,
                    restoreLayers: false
                }),
                type: "control"
            };
            this.properties.addOn.controlBaseLayers = {
                value: L.customControl({
                    type: "baselayer",
                    baseLayers: this.baseLayers,
                    map: map,
                    container: "#page-wrapper",
                    hiddden: true
                }),
                type: "control"
            };

            map.options.maxZoom = this.properties.addOn.controlBaseLayers.value.options.map._layersMaxZoom;

        } else {
            //object.control = new L.control();
        }
    };
};
