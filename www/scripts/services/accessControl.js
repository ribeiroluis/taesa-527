﻿function _AcessControl() {

    this.getAuthorizeToken = function () {
        $.ajax({
            "async": false,
            "crossDomain": true,
            "url": window.appConfig.serverURL + "Token",
            "method": "POST",
            "headers": {
                "content-type": "application/x-www-form-urlencoded"
            },
            "xhrFields": {
                "withCredentials": true
            },
            "data": {
                "grant_type": "password"
            }
        }).done(function (data, textStatus, jqXHR) {
            sessionStorage.setItem("authToken", data.token_type + " " + data.access_token);
        }).fail(function (qXHR, textStatus, errorThrown) {            
            window.location.replace("./error.html");            
        });
    }

    this.getAllUserAccess = function () {
        connectionUtilities.sendAjaxRequest(window.appConfig.serverURL + "User/GetAllAccessByCurrentUser", "GET", undefined,
                function (data) {
                    if (data != null) {
                        accessControl.removeAllAccessesFromSessionStorage();
                        data.forEach(function (access, index) {
                            if (sessionStorage.getItem("userAccess." + access.accessCode) === null || access.allowChange) {
                                sessionStorage.setItem("userAccess." + access.accessCode, access.allowChange);
                                sessionStorage.setItem("userAccess." + access.module, false);
                            }
                        });
                    }
                    
                }, undefined, true);
    }

    this.removeAllAccessesFromSessionStorage = function () {
        for (var key in sessionStorage) {
            if (sessionStorage.hasOwnProperty(key)) {
                if (key.indexOf("userAccess.") != -1) {
                    sessionStorage.removeItem(key);
                }
            }
        }
    };

    this.checkAccess = function (module, allowChangeRequired) {
        var hasAccess = sessionStorage.getItem('userAccess.' + module) !== null;
        if (allowChangeRequired) {
            var allowChange = sessionStorage.getItem('userAccess.' + module) === true || sessionStorage.getItem('userAccess.' + module) === "true";
            hasAccess = hasAccess && allowChange === true;
        }
        return hasAccess;
    }
}

var accessControl = new _AcessControl();