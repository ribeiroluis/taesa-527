(function () {
    "use strict";

    var app = angular.module("mockTaesa", ["ngRoute"]);
    app.config(function ($routeProvider, $locationProvider) {
        $routeProvider
            .when("/", {
                redirectTo: "/map"
            })
            .when("/map", {
                templateUrl: "ng-views/map.html",
                controller: "mapController",
                controllerAs: "map"
            })
            .when("/alarm", {
                templateUrl: "ng-views/alarm.html",
                controller: "alarmController",
                controllerAs: "alarm"
            })
            .when("/data", {
                templateUrl: "ng-views/dataManagement.html",
                controller: "dataManagementController",
                controllerAs: "data"
            })
            .when("/feeder", {
                templateUrl: "./ng-views/feeder.html",
                controller: "feederController",
                controllerAs: "feeder"
            })
            .when("/line", {
                templateUrl: "./ng-views/lineManagement.html",
                controller: "lineManagementController",
                controllerAs: "line"
            })
            .when("/icons", {
                templateUrl: "./ng-views/iconManagement.html",
                controller: "iconManagementController",
                controllerAs: "icon"
            })
            .when("/base", {
                templateUrl: "./ng-views/operationalBase.html",
                controller: "operationalBaseController",
                controllerAs: "base"
            })
            .when("/profile", {
                templateUrl: "./ng-views/profile.html",
                controller: "profileController",
                controllerAs: "profile"
            })
            .when("/user", {
                templateUrl: "./ng-views/user.html",
                controller: "userController",
                controllerAs: "user"
            })
            .when("/allocationteam", {
                templateUrl: "./ng-views/allocationTeam.html",
                controller: "allocationTeamController",
                controllerAs: "team"
            })
            .when("/generateallocation", {
                templateUrl: "./ng-views/generateAllocation.html",
                controller: "allocationTeamController",
                controllerAs: "team"
            })
            .when("/addDamage", {
                templateUrl: "./ng-views/addDamage.html",
                controller: "damageController",
                controllerAs: "damage"
            })
            .when("/damage", {
                templateUrl: "./ng-views/damage.html",
                controller: "damageController",
                controllerAs: "damage"
            })
            .when("/alarmsConfig", {
                templateUrl: "./ng-views/alarmConfig.html",
                controller: "alarmConfigController",
                controllerAs: "alarm"
            })
            .when("/report", {
                templateUrl: "./ng-views/report.html",
                controller: "reportController",
                controllerAs: "report"
            });
    });

    app.run(function ($rootScope) {
        //ng-include terminou
        $rootScope.$on("$includeContentLoaded", function (event, templateName) {
            if (templateName.indexOf("sidebar.html") >= 0) {
                initializeSideBarMenu();                
            }
            if (templateName.indexOf("maptoolbar.html") >= 0) {
                if (actionOnActionBar)
                    actionOnActionBar();
            }

            if (templateName.indexOf("map.html") >= 0) {
                event.targetScope.$$childTail.initialize();
                event.targetScope.$watch('event.targetScope.map', function (newValue, oldValue) {
                    if (event.targetScope.registerMapOnController) {
                        event.targetScope.registerMapOnController();
                    }
                });
            }



        });
        $rootScope.$on("$viewContentLoaded", function (scope) {
            if (scope.targetScope.initialize)
                scope.targetScope.initialize();
        });
    });

    app.directive("focusiconsearch", function () {
        return {
            link: function (scope, element, attributes) {
                element.on("focus", function () {
                    $(".search-icon").css("color", "#f37021");
                });
                element.on("blur", function () {
                    $(".search-icon").css("color", "#9d9fa2");
                });
            }
        }
    });

    app.directive('onElementFinishRender', ($timeout) => {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                if(element.length > 0){
                    $timeout(function () {
                        scope.$emit(attr.onElementFinishRender, attr);                        
                    });                    
                }                
            }
        }
    });

    window.app = app;
})();