///<reference path="../scripts/vendor/underscore/underscore-min.js"/>


(function () {
    "use strict";

    //Register controller
    window.app.controller("profileController", profileController);

    function profileController($scope) {
        var accesses;
        this.alarmLevel = [            
            "Alto",
            "Médio",
            "Baixo"            
        ];



        this.addProfile = function () {
            this.isActive = true;
            this.isEditing = false;
            this.selectedProfile = this.getAccesses();
        };

        this.cancelAction = function () {
            this.isActive = false;
            this.isEditing = false;
            this.selectedProfile = null
        };

        this.saveProfile = function () {
            var profileName = this.selectedProfile.name;
            if (!this.profiles.find(p => { return p.name == profileName })) {
                this.profiles.push(this.selectedProfile);
            }
            this.isActive = false;
            this.isEditing = false;
            this.selectedProfile = null
        }

        this.getAccesses = function () {
            var _data;
            connectionUtilities.sendAjaxRequest('/api/getAccesses', "GET", undefined,
                function (data) {
                    if (!data)
                        return;
                    _data = data;
                }, undefined, true);
            return _data;
        };


        this.getProfiles = function () {
            var self = this;
            connectionUtilities.sendAjaxRequest('/api/getProfiles', "GET", undefined,
                function (data) {
                    if (!data)
                        return;
                    self.profiles = data;
                }, undefined, true);

            if (!$scope.$$phase)
                $scope.$apply();
        };
        this.getProfiles();
    }
})();