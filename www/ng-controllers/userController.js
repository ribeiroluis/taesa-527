(function () {
    "use strict";

    //Register controller
    window.app.controller("userController", userController);

    function userController($scope) {

        this.alarmLevel

        this.getUsers = function () {
            var self = this;
            connectionUtilities.sendAjaxRequest('/api/getUsers', "GET", undefined,
                function (data) {
                    if (!data)
                        return;

                    self.users = data;
                }, undefined, true);

            if (!$scope.$$phase)
                $scope.$apply();
        };

        this.getProfiles = function () {
            var self = this;
            connectionUtilities.sendAjaxRequest('/api/getProfiles', "GET", undefined,
                function (data) {
                    if (!data)
                        return;

                    self.profiles = data;
                }, undefined, true);

            if (!$scope.$$phase)
                $scope.$apply();
        };

        this.addProfileToUser = function (prof) {
            if (!this.selectedUser) {
                this.selectedUser = {
                    profiles: []
                }
            } else if (!this.selectedUser.profiles) {
                this.selectedUser.profiles = [];
            }


            if (this.selectedUser.profiles.indexOf(prof.name) < 0) {
                this.selectedUser.profiles.push(prof.name);
            }
        };

        this.saveUser = function () {
            if (!this.isEditing) {
                this.users.push(this.selectedUser);
            }
            this.isEditing = false;
            this.isActive = false;
        };


        this.getUsers();
        this.getProfiles();
    }
})();