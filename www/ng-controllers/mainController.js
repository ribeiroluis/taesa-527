///<reference path="../scripts/services/dom.js"/>
///<reference path="../scripts/services/connections.js"/>

(function () {
    "use strict";

    //Register controller
    window.app.controller("mainController", mainController);

    function mainController($scope, $route, $routeParams, $location, $http) {
        this.notifications = [];
        this.newNotifications = 0;
        this.alarms = 0;
        this.alarmsList = [];        

        this.connectOnWebSocketServer = function () {

            var self = this;

            connectionUtilities.setOnOpen(function (msg) {
                var message = {
                    "Type": "REGISTER LISTENER",
                    "UserId": self.user
                };
                connectionUtilities.sendWebSocketRequest(JSON.stringify(message));
            });

            connectionUtilities.setOnMessage(function (msg) {
                self.throwNotification(JSON.parse(msg));
            });

            connectionUtilities.setOnError(function (msg) {
                var errorNotification = {
                    Message: "A aplicação parou de responder. Verifique com o adminstrador do sistema. Erro: " + msg,
                    Type: "Error",
                    Date: new Date(),
                    MessageType: "Notification",
                    rason: msg
                };
                console.error(msg);
                self.throwNotification(errorNotification);
            });

            connectionUtilities.setOnClose(function (msg) {
                var errorNotification = {
                    Message: "A aplicação parou de responder. Verifique com o adminstrador do sistema. Erro: " + msg,
                    Type: "Error",
                    Date: new Date(),
                    MessageType: "Notification",
                    rason: msg
                };
                console.error(msg);
                self.throwNotification(errorNotification);
            });

            connectionUtilities.ensureWebSocketConnection(`ws://${window.location.hostname}:8081`);

        };

        this.throwNotification = function (notification) {
            try {
                switch (notification.Type) {
                    case "Error": notification.icon = "fa-exclamation-triangle"; notification.title = "Erro"; break;
                    case "Success": notification.icon = "fa-check-circle"; notification.title = "Sucesso"; break;
                    case "Warning": notification.icon = "fa-exclamation-triangle"; notification.title = "Alerta"; break;
                    case "Info": notification.icon = "fa-info-circle"; notification.title = "Informação"; break;
                }
                if (!notification.MessageType) return;

                if (notification.MessageType == "alarm") {
                    this.alarms++;
                    this.alarmsList.unshift(notification);
                }

                if (!domUtilities.hasClass("#notificationBell", "open")) {
                    this.newNotifications++;
                }

                this.notifications.push(notification);

                //console.log(new Date(notification.Date).getTime() + "->" + notification.Message);

                this.notifications.sort(function (a, b) {
                    return new Date(b.Date) - new Date(a.Date);
                });


                if (!$scope.$phase)
                    $scope.$apply();

                domUtilities.throwAlertNotification();
            } catch (error) {
                console.error(error);
            }
        };


        //debugger;
        //$location.path("/");
        if ($location.$$url != "/map" || $location.$$url != "") {
            //window.location.replace('#!/');
        }

        $scope.$on('viewLoaded', (scope, attr) => {
            setTimeout(() => {
                if ($location.$$url == "/map") {
                    $('.sidebar').addClass('close');
                    $('.concert-logo').addClass('close');

                } else {
                    $('.sidebar').removeClass('close');
                    $('.concert-logo').removeClass('close');
                }
            }, 50);
        });

        $scope.main.connectOnWebSocketServer();
        $scope.main.user = "DEV";
    }
})();

Highcharts.setOptions({
    lang: {
        months: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        shortMonths: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        weekdays: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        loading: ['Atualizando o gráfico...aguarde'],
        contextButtonTitle: 'Exportar gráfico',
        decimalPoint: ',',
        thousandsSep: '.',
        downloadJPEG: 'Baixar imagem JPEG',
        downloadPDF: 'Baixar arquivo PDF',
        downloadPNG: 'Baixar imagem PNG',
        downloadSVG: 'Baixar vetor SVG',
        printChart: 'Imprimir gráfico',
        rangeSelectorFrom: 'De',
        rangeSelectorTo: 'Para',
        rangeSelectorZoom: 'Zoom',
        resetZoom: 'Limpar Zoom',
        resetZoomTitle: 'Voltar Zoom para nível 1:1',
    }
});