(function () {
    "use strict";

    //Register controller
    window.app.controller("feederController", feederController);

    function feederController($scope) {
        this.getFeederColors = function () {
            var self = this;
            connectionUtilities.sendAjaxRequest('/api/getFeederColors', "GET", undefined,
                function (data) {
                    if (!data)
                        return;

                    self.feederColors = data;
                }, undefined, true);
            if (!$scope.$$phase)
                $scope.$apply();

        };
        this.getFeederColors();
    }
})();