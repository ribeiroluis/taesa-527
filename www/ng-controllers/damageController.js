///<reference path="../scripts/services/connections.js"/>
(function () {
    "use strict";

    //Register controller
    window.app.controller("damageController", damageController);

    function damageController($scope) {
        this.damageDetail;
        this.concessions = [];
        this.damageParameters = [];
        var date = new Date();
        this.selectedDamage = {
            date: date,
            dateValue: parseDatetoInput(date),
            defaultParams: {},
            damages: {}
        };

        function parseDatetoInput(date) {
            var time = date.toLocaleTimeString().split(':');
            return `${date.toISOString().split('T')[0]}T${time[0]}:${time[1]}`;
        }

        this.getDamages = function () {
            if (location.hash.indexOf('addDamage') >= 0) return;

            var self = this;
            connectionUtilities.sendAjaxRequest('/api/getDamages', "GET", undefined,
                function (data) {
                    if (!data)
                        return;

                    self.damages = data;

                    data.forEach(e => {
                        e._generateDate = `${new Date(e.generateDate).toLocaleString()}`;
                        e._eventDate = `${new Date(e.eventDate).toLocaleString()}`;
                    });
                    var columnDefs = [
                        { title: "Data da geração", "targets": 0 },
                        { title: "Data do evento", "targets": 1 },
                        { title: "Linha", "targets": 2 },
                        { title: "Torre", "targets": 3 },
                        { title: "Tipo", "targets": 4 },
                        { title: "Nível de contigência", "targets": 5 },
                        { title: "Quantidade de pessoas alocadas", "targets": 6 },
                        { title: "Prioridade da linha", "targets": 7 },
                        { title: "Regionais", "targets": 8 }
                    ];
                    var columns = [
                        { data: "_generateDate" },
                        { data: "_eventDate" },
                        { data: "concession" },
                        { data: "tower" },
                        { data: "type" },
                        { data: "level" },
                        { data: "peopleQuantity" },
                        { data: "linePriority" },
                        { data: "regional" }
                    ];
                    var table = $('#damageTable').DataTable({
                        scrollY: "300px",
                        scrollX: true,
                        scrollCollapse: true,
                        paging: false,
                        buttons: [
                            'csv', 'excel', 'pdf', 'print'
                        ],
                        data: data,
                        language: {
                            "url": "scripts/vendor/datatable/pt-br.json"
                        },
                        columnDefs: columnDefs,
                        columns: columns
                    });
                    new $.fn.dataTable.Buttons(table, {
                        buttons: [
                            {
                                extend: 'print',
                                text: 'Imprimir'
                            },
                            'csv', 'excel', 'pdf',
                        ]
                    });

                    if (!$scope.$$phase) $scope.$apply();

                    table.buttons().container()
                        .appendTo($('.button-group', table.table().container()));

                    $('#damageTable tbody').on('click', 'tr', function () {
                        var data = table.row(this).data();
                        $(this).toggleClass('row-selected');
                        var self = this;
                        $('.row-selected').each((i, e, arr) => {
                            if (e != self)
                                $(e).removeClass('row-selected');
                        });
                        var selecteds = $('.row-selected');
                        if (selecteds.length > 0) {
                            $('#page-wrapper').addClass('w-actionbar');
                        } else {
                            $('#page-wrapper').removeClass('w-actionbar');
                        }
                        var _data = [];
                        _data.push({ key: 'Data do evento', value: data._eventDate });
                        _data.push({ key: 'Data da geração', value: data._generateDate });
                        _data.push({ key: 'Linha', value: data.concession });
                        _data.push({ key: 'Torre', value: data.tower });
                        _data.push({ key: 'Nível de contingência', value: data.level });
                        _data.push({ key: 'Regionais', value: data.regional });
                        _data.push({ key: 'Prioridade da linha', value: data.linePriority });
                        _data = _data.concat(data.params);
                        $scope.damage.damageDetail = {
                            type: data.type,
                            peopleQuantity: data.peopleQuantity,
                            params: _data
                        };
                        if (!$scope.$$phase) $scope.$apply();
                    });
                }, undefined, true);
        };

        this.getDamageParameters = function () {
            var self = this;
            connectionUtilities.sendAjaxRequest('/api/getDamagesParameters', "GET", undefined,
                function (data) {
                    if (!data)
                        return;
                    self.damageParameters = data;

                    if (!$scope.$$phase) $scope.$apply();

                }, undefined, true);
        };

        this.getContingencyLevels = function () {
            var self = this;
            connectionUtilities.sendAjaxRequest('/api/getContingencyLevels', "GET", undefined,
                function (data) {
                    if (!data)
                        return;
                    self.contingencyLevels = data;

                    if (!$scope.$$phase) $scope.$apply();

                }, undefined, true);
        };

        this.addDamage = function () {
            window.location.replace('#!/addDamage');
        };

        this.cancelAddDamage = function () {
            window.location.replace('#!/damage');
        };

        this.checkIsValid = function () {
            if (!this.damageParameters.damages) return false;

            var self = this.selectedDamage;
            var selectedDamage = this.damageParameters.damages.find(e => { return e == self.damage });
            if (selectedDamage == undefined) return false;

            return Object.keys(this.selectedDamage.defaultParams).length == this.damageParameters.defaultParams.length &&
                selectedDamage.params.length == Object.keys(this.selectedDamage.damages).length && $('#datetimepicker1').val() != "" &&
                this.selectedDamage.concession != undefined && this.selectedDamage.tower != undefined;
        };

        this.generateDamage = function () {
            var totalPoints = 0;
            var damageDate = this.selectedDamage.value;
            _.each(this.selectedDamage.defaultParams, e => {
                totalPoints += e.points;
            });
            _.each(this.selectedDamage.damages, e => {
                totalPoints += e.points;
            });
            var self = this;
            var contingency = this.contingencyLevels.find(e => {
                return e.id == self.selectedDamage.damage.id;
            });
            this.contingency = contingency.levels.find(e => { return totalPoints >= e.minimum && totalPoints <= e.maximum });
            this.contingency.id = contingency.id;
        };

        this.saveDamage = function () {
            var data = {
                contingency: this.contingency,
                damage: this.selectedDamage
            };
            var self = this;
            connectionUtilities.sendAjaxRequest('/api/saveDamage', "POST", JSON.stringify(data),
                (data) => {
                    self.selectedDamage = {
                        date: date,
                        dateValue: parseDatetoInput(date),
                        defaultParams: {},
                        damages: {}
                    };
                    self.contingency = null;
                    if (!$scope.$$phase) $scope.$apply();
                    self.getContingencyLevels();
                },
                (err) => {
                    domUtilities.showMessage("error", "Erro ao tentar salvar a contingência");
                }, true);
        };

        this.closeActionBar = function () {
            this.damageDetail = {};
            $('#page-wrapper').removeClass('w-actionbar');
            $('.row-selected').each((i, e, arr) => {
                if (e != self)
                    $(e).removeClass('row-selected');
            });
        };

        this.getConcessions = function () {
            var self = this;
            connectionUtilities.sendAjaxRequest('./api/getConcessionsList', "GET", undefined,
                function (_data) {
                    self.concessions = _data;
                }, function () {
                    domUtilities.showMessage("Error", undefined, "Erro ao tentar buscar concessões");
                }, true);
        };

        this.searchTowers = function (value) {
            var self = $scope.damage;
            self.selectedDamage.concession = value;
            self.selectedDamage.tower = "";
            if (!$scope.$$phase) $scope.$apply();
            var data;
            domUtilities.showElement(".loader");
            setTimeout(() => {
                connectionUtilities.sendAjaxRequest('./api/getTowersIdsFromConcession/' + value, "GET", undefined,
                    function (_data) {
                        data = _data;

                        var towers = [];
                        data.forEach(element => {
                            towers.push({ tower: element });
                        });

                        domUtilities.enableAutoCompleteOnElement("#input2",
                            towers, "tower", function (val) {
                                self.selectedDamage.tower = val;
                                if (!$scope.$$phase) $scope.$apply();
                            });
                    }, function () {
                        domUtilities.showMessage("Error", undefined, "Erro ao tentar buscar torres");
                    }, true);
                domUtilities.hideElement(".loader");
            }, 50);
        };

        $scope.$on('elementFinishRender', function (elementFinishRender) {
            $('#datetimepicker1')[0].step = 60;
            // var d = new Date();
            // $('#datetimepicker1').val(d.toLocaleDateString());
            // $scope.damage.selectedDamage.date = new Date(d.getFullYear(), d.getMonth(), d.getDay());
            // $('#datetimepicker1').datepicker({
            //     todayBtn: "linked",
            //     language: "pt-BR",
            //     forceParse: false,
            //     autoclose: true,
            // }).on('changeDate', e => {
            //     $scope.damage.selectedDamage.date = e.date;
            // });
        });

        $scope.$on('inputfinish', (scope, attr) => {
            var self = scope.targetScope.$parent.damage;
            if (attr.name == "concession") {
                var concessions = [];
                self.concessions.forEach(c => {
                    concessions.push({ concession: c });
                });
                domUtilities.enableAutoCompleteOnElement("#input1",
                    concessions, "concession", self.searchTowers);
            }
        });

        $scope.initialize = function () {
            domUtilities.showElement(".loader");
            var self = $scope.damage;
            self.getConcessions();
            setTimeout(() => {
                self.getDamages();
                self.getDamageParameters();
                self.getContingencyLevels();

                domUtilities.hideElement(".loader");
            }, 0);
            $scope.$watch('damage.selectedDamage.damage', function (newValue, oldValue, scope) {
                if (oldValue != null) {
                    scope.damage.selectedDamage.damages = {}
                }
            });
        };
    }
})();

function changeDate(e) {
    angular.element(e).scope().damage.selectedDamage.date = new Date(e.value);
    if (!$scope.$$phase) $scope.$apply();
}