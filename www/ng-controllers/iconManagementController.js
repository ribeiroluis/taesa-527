(function () {
    "use strict";

    //Register controller
    window.app.controller("iconManagementController", iconManagementController);

    function iconManagementController($scope, $timeout) {
        this.getIcons = function () {
            var self = this;
            try {
                connectionUtilities.sendAjaxRequest('/api/getIcons/', "GET", undefined,
                    function (data) {
                        if (!data) {
                            throw new Error();
                        }
                        self.icons = data;
                    }, undefined, true);
                if (!$scope.$$phase) $scope.$apply();
            } catch (e) {
                console.error(e)
                domUtilities.showMessage("error", undefined, "Erro ao tentar buscar os icones cadastradaso, verifique");
            }
        };

        this.activeSelection = function (m) {
            if (m.i.active) {
                m.i.active = false;
                return;
            }

            this.icons.forEach(function (e) {
                e.active = false;
            });
            m.i.active = true;
        };

        this.triggerClick = function (index, m) {
            document.getElementById("inputIconFile" + index).value = "";
            m.active = true;            
            $timeout(function () {
                domUtilities.triggerEvent(document.getElementById("inputIconFile" + index), "click");
            }, 0);
        };

        this.loadPicture = function (e) {            
            var selected = this.icons.find(i => { return i.active; });
            domUtilities.showElement(".loader");
            var formData = new FormData(); 
            formData.append('name', selected.element);
            formData.append('icon', e.files[0], e.files[0].name);
            var self = this;
            connectionUtilities.sendFormData('./api/uploadIconFile', formData,
                (data) => {
                    self.getIcons();
                    domUtilities.hideElement(".loader");
                }, (err) => {
                    console.error(err);
                    domUtilities.hideElement(".loader");
                });            
        };

        // this.saveModifications = function () {
        //     try {
        //         let configEndPoints = window.appEndPoints.find(function (e) { return e.Controller === "Configuration" });
        //         let action = configEndPoints.Actions.find(function (e) { return e.Action === "UpdateMapIcons" });
        //         var self = this;
        //         var payload = [];
        //         this.icons.forEach(function (e) {
        //             if (e.isChange) {
        //                 var url = window.appConfig.serverURL + configEndPoints.Controller + "/" + action.Action + "?iconId=" + e.mRID;
        //                 connectionUtilities.sendAjaxRequest(url, action.Method, "[" + e.ByteArrayImage + "]",
        //                function (data) {
        //                    if (!data) {
        //                        throw new Error();
        //                    }
        //                }, undefined, true);                        
        //             }
        //         });
        //         domUtilities.showMessage("success", undefined, "Icones salvos com sucesso!");

        //     } catch (e) {
        //         console.error(e)
        //         domUtilities.showMessage("error", undefined, "Erro ao tentar buscar os icones cadastradaso, verifique");
        //     }
        // };

        // this.removeIcon = function () {
        //     let configEndPoints = window.appEndPoints.find(function (e) { return e.Controller === "Configuration" });
        //     let action = configEndPoints.Actions.find(function (e) { return e.Action === "UpdateMapIcons" });
        //     var icon = this.icons.find(function (e) { return e.active });
        //     if (icon) {
        //         if (icon.ByteArrayImage) {
        //             var url = window.appConfig.serverURL + configEndPoints.Controller + "/" + action.Action + "?iconId=" + icon.mRID;
        //             connectionUtilities.sendAjaxRequest(url, action.Method, undefined,
        //            function (data) {
        //                if (!data) {
        //                    throw new Error();
        //                }
        //                domUtilities.showMessage("success", undefined, "Ícone removido com sucesso!");
        //            }, undefined, true);
        //         }
        //         icon.isChange = false;                
        //         icon.ByteArrayImage = null;
        //         icon.img = null;
        //         icon.active = false;
        //     }
        // };

        this.enableSaveChanges = function () {
            if (!this.icons) return "inactive";
            return this.icons.filter(function (e) { return e.isChange }).length > 0 ? "" : "inactive";
        }

        this.enableRemoveIcon = function () {
            if (!this.icons) return "inactive";
            return this.icons.filter(function (e) { return e.active }).length > 0 ? "" : "inactive";
        };

        this.getIcons();
    }
})();