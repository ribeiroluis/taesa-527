(function () {
    "use strict";

    //Register controller
    window.app.controller("dataManagementController", dataManagementController);

    function dataManagementController($scope) {

        this.name = "teste";
        this.uploadSizeLimit = 0;
        this.filesToUpload = [];
        this.filesOnServer = {};
        this.elementsTypes = ["LINHA", "TORRE"];
        this.integrationFiles = [
            {
                "Name": "linha_2017-10-20T14:36:02.8247293-02:00.csv",
                "Length": 2736,
                "LastWriteTime": "2017-10-20T14:36:02.8247293-02:00"
            },
            {
                "Name": "torre_2017-10-20T14:36:02.8247293-02:00.csv",
                "Length": 2736,
                "LastWriteTime": "2017-10-11T15:33:43.5167701-03:00"
            }];

        this.readerFileSelected = function (e) {
            try {
                if (e.files.length < 1) {
                    return;
                }
                for (var i = 0; i < e.files.length; i++) {
                    var file = e.files[i]
                    var type = this.elementsTypes.find(function (e, i, arr) {
                        return file.name.toUpperCase().indexOf(e) >= 0;
                    });

                    if (!type) {
                        domUtilities.showMessage("error", undefined, "O nome do arquivo não foi reconhecido como elemento para upload: " + file.name);
                    } else {

                        this.filesToUpload.push({
                            name: file.name,
                            humanFileSize: humanFileSize(file.size),
                            size: file.size,
                            file: file,
                            type: type,
                            content: ""
                        });
                        this.loadFile(file);
                    }
                }
                e.value = "";
                if (!$scope.$$phase)
                    $scope.$apply();

            } catch (e) {
                domUtilities.showMessage("error", undefined, e.message);
            }
        };

        this.loadFile = function (file) {
            var name = file.name;
            var reader = new FileReader();
            reader.addEventListener("load", function (e) {
                var _file = this.filesToUpload.find(function (e) { return e.name == name });
                _file.content = e.target.result.split("\n").slice(0, 500).join("\n");
                if (!$scope.$$phase) {
                    $scope.$apply();
                }
            }.bind(this), true);
            reader.readAsText(file);
        };

        this.clearFilesToUpload = function () {
            try {
                this.filesToUpload = [];
            } catch (e) {
                domUtilities.showMessage("error", undefined, e.message);
            }
        };

        function humanFileSize(size) {
            var i = Math.floor(Math.log(size) / Math.log(1024));
            return Math.round(100 * (size / Math.pow(1024, i))) / 100 + " " + ["B", "kB", "MB", "GB"][i];
        };

        this.bus = [
            {
                name: "INPE - Monitoramento de queimadas",
                user: "admin",
                password: "admin",
                url: "http://10.10.9.10:8080"
            },
            {
                name: "INPE - Monitoramento de descargas atmosféricas",
                user: "admin",
                password: "admin",
                url: "http://10.10.9.11:8080"
            },
            {
                name: "INPE - Previsão de descargas atmosféricas",
                user: "admin",
                password: "admin",
                url: "http://10.10.9.16:8080"
            },
            {
                name: "INPE - Previsão de ventos severos",
                user: "admin",
                password: "admin",
                url: "http://10.10.9.12:8080"
            },
            {
                name: "SCADA TAESA",
                user: "admin",
                password: "admin",
                url: "http://10.10.10.12:8080"
            }            
        ];


    }
})();