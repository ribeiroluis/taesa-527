///<reference path="../scripts/services/connections.js"/>
var _scope;
(function () {
    "use strict";

    //Register controller
    window.app.controller("alarmConfigController", alarmConfigController);

    function alarmConfigController($scope) {
        _scope = $scope;

        this.getAlarmParameters = function () {
            var self = this;
            connectionUtilities.sendAjaxRequest('/api/getAlarmParameters', "GET", undefined,
                (data) => {
                    self.alarmParameters = data;

                    if (!$scope.$$phase) $scope.$apply();

                }, (err) => {
                    domUtilities.showMessage("error", "", "Erro ao tentar recupearar as configurações");
                }, true);
        };

        this.selectTab = function (e, tab) {
            $('.tab-pane').removeClass('active');
            $('#' + tab).addClass('active');
        };

        this.openFileDialog = function (name) {
            $(`#file_${name}`).click();
        };

        this.updateAlarmValues = function (name) {
            console.log(name)
            switch (name.toUpperCase()) {
                case 'BAIXO':
                    this.alarmParameters[1].minValue = this.alarmParameters[0].maxValue;
                    $("#slide_Médio").slider("values", 0, this.alarmParameters[1].minValue);
                    $(`#handle-Médio-0`).text(this.alarmParameters[1].minValue);
                    break;
                case 'MÉDIO':
                    this.alarmParameters[0].maxValue = this.alarmParameters[1].minValue;
                    $("#slide_Baixo").slider("values", 1, this.alarmParameters[0].maxValue);
                    $(`#handle-Baixo-1`).text(this.alarmParameters[0].maxValue);

                    this.alarmParameters[2].minValue = this.alarmParameters[1].maxValue;
                    $("#slide_Alto").slider("values", 0, this.alarmParameters[2].minValue);
                    $(`#handle-Alto-0`).text(this.alarmParameters[2].minValue);
                    break;
                case 'ALTO':
                    this.alarmParameters[1].maxValue = this.alarmParameters[2].minValue;
                    $("#slide_Médio").slider("values", 1, this.alarmParameters[1].maxValue);
                    $(`#handle-Médio-1`).text(this.alarmParameters[1].maxValue);
                    break;
            }

        };

        this.testSound = function (obj) {
            if (!obj.volumeOn) {
                var alarmPath = obj.alarmPath == "" ? '/scripts/dist/sounds/alert.wav' : obj.alarmPath;
                obj.audio = new Audio(alarmPath);
                obj.audio.play();
                obj.audio.addEventListener('ended', function () {
                    obj.volumeOn = false;
                    if (!$scope.$$phase) $scope.$apply();
                }, false);
            } else {
                obj.audio.pause();
            }
        };

        this.saveParameters = function () {
            var self = this;
            connectionUtilities.sendAjaxRequest('/api/saveAlarmConfiguration', "POST", JSON.stringify(this.alarmParameters),
                (data) => {
                    self.isModified = false;

                    if (!$scope.$$phase) $scope.$apply();
                    domUtilities.showMessage("success", "Configurações salvas com sucesso!");

                }, (err) => {
                    domUtilities.showMessage("error", "", "Erro ao tentar salvar as configurações");
                }, true);
        }

        $scope.initialize = function () {
            domUtilities.showElement(".loader");
            var self = $scope.alarm;
            setTimeout(() => {
                self.getAlarmParameters();
                domUtilities.hideElement(".loader");
            }, 0);
        };

        $scope.$on('elementFinishRender', function (scope, attr) {
            var alarmScope = scope.targetScope.$parent.alarm;
            var objectScope = scope.targetScope.p;
            var name = attr.name;
            $(attr.$$element).slider({
                range: true,
                min: 0,
                max: 100,
                values: [objectScope.minValue, objectScope.maxValue],
                create: function () {
                    $(`#handle-${name}-0`).text($(this).slider("values", 0))
                    $(`#handle-${name}-1`).text($(this).slider("values", 1))
                },
                slide: function (event, ui) {
                    $(`#handle-${name}-0`).text(ui.values[0]);
                    $(`#handle-${name}-1`).text(ui.values[1]);
                    objectScope.minValue = ui.values[0];
                    objectScope.maxValue = ui.values[1];
                    alarmScope.isModified = true;
                    alarmScope.updateAlarmValues(name);
                    if (!$scope.$$phase) $scope.$apply();
                }
            });
        });
    }
})();

function sendFile(element) {
    debugger;
    domUtilities.showElement(".loader");
    var alarmScope = angular.element(element).scope().$parent.alarm;
    var scope = angular.element(element).scope().p;
    var formData = new FormData();
    formData.append('name', element.name);
    formData.append('sound', element.files[0], element.files[0].name);
    connectionUtilities.sendFormData('/api/uploadAlarmSound', formData,
        (data) => {
            scope.alarmPath = data.alarmPath;
            alarmScope.isModified = true;
            if (!_scope.$$phase) _scope.$apply();
            domUtilities.hideElement(".loader");
        }, (err) => {
            console.error(err);
            domUtilities.hideElement(".loader");
        });
};