(function () {
    "use strict";

    //Register controller
    window.app.controller("allocationTeamController", allocationTeamController);

    function allocationTeamController($scope, $routeParams) {
        this.name = "allocationTeamController";
        var mapScope, mapController, geocoder;

        var equipmentTypeAlias = {
            FIRE: "Foco de incêndio",
            TOWER: "Torre",
            FORECASTFIRE: "Previsão de incêndio",
            THUNDER: "Raio",
            FORECASTTHUNDER: "Previsão de raio"
        };

        $('#side-menu a').removeClass('active');
        $('.fa.fa-users').parent().addClass('active');

        this.generateAlocation = function () {
            window.location.replace('#!/generateallocation/?isEditing=false');
            this.isEditing = false;
        };

        this.getBases = function () {
            var self = this;
            connectionUtilities.sendAjaxRequest('/api/getTeams', "GET", undefined,
                function (data) {
                    if (!data)
                        return;

                    self.bases = data;
                    self._bases = data.map(base => { return base.Name });
                }, undefined, true);

            if (!$scope.$$phase)
                $scope.$apply();

            var geojson = {
                "type": "FeatureCollection",
                "features": []
            };
            self.bases.forEach(base => {
                base.type = "base";
                geojson.features.push({
                    "type": "Feature",
                    "properties": base,
                    "geometry": {
                        "type": "Point",
                        "coordinates": [
                            base.Longitude,
                            base.Latitude,
                        ]
                    }
                });
            });
            mapScope.addLayerOnMap(geojson,
                "Point",
                "Bases",
                "Bases Operacionais",
                undefined,
                true,
                false,
                true,
                false);

        };

        this.loadAlarms = function () {
            this.priorities = Object.keys(_.groupBy($scope.main.alarmsList, (value) => {
                return value.Data.point.priority;
            }));
            $scope.main.alarmsList.forEach(alarm => {
                var data = alarm.Data;
                console.log(data);
            });
            this.generateParams = {
                fire: true,
                priorities: ['Muito Alto'],
                area: 'line',
                workTime: 8,
                velocity: 47.5
            };
            this.changeArea('line');
        };

        this.changeArea = function () {
            var self = this;
            var filterAreas = [];
            if (!self.generateParams.thunder && !self.generateParams.fire) {
                this.priorities = [];
                this.areas = [];
                return;
            }


            if (self.priorities.length == 0) {
                self.priorities = Object.keys(_.groupBy($scope.main.alarmsList, (value) => {
                    return value.Data.point.priority;
                }));
            }
            if (self.generateParams.thunder) {
                filterAreas = _.filter($scope.main.alarmsList, (value) => {
                    return self.generateParams.priorities.indexOf(value.Data.point.priority) >= 0 && (value.Data.point.type == "thunder" || value.Data.point.type == "thunderForecast");
                });
            }
            if (self.generateParams.fire) {
                var filter = _.filter($scope.main.alarmsList, (value) => {
                    return self.generateParams.priorities.indexOf(value.Data.point.priority) >= 0 && value.Data.point.type == "fire";
                });
                filterAreas = filterAreas.concat(filter);
            }
            if (self.generateParams.wind) {
                var filter = _.filter($scope.main.alarmsList, (value) => {
                    return self.generateParams.priorities.indexOf(value.Data.point.priority) >= 0 && (value.Data.point.type == "windForecast" || value.Data.point.type == "wind");
                });
                filterAreas = filterAreas.concat(filter);
            }
            if (this.generateParams.area == "line") {
                this.areas = Object.keys(_.groupBy(filterAreas, (value) => {
                    return value.Data.point.concession;
                }));
            } else {
                this.areas = ["Cidade A", "Cidade B", "Cidade C"];
            }
        };

        this.customMarker = function (feature) {
            var layerProperties = {};
            switch (feature.properties.type.toUpperCase()) {
                case "BASE":
                    layerProperties = {
                        markerHTML: "<img class='mapIconEnergisa' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAACMUlEQVRoQ+2YT0jCUBzH3xIypBIjSo8dI+c8RFB2MDobdLWC6FxUdwMh7xV1DqLyHHmOPGRBdHBqXTuqEYkVgoGubTDR/XFue2/j1d5x+PY+n/2+v/c2CYD5IDDnB7aA1RW0K/BnK1AIBkMMwyQ4QYIgYv5sNoNCFnqEnilqvgnAPgsbFgGn+wDYm6LpO5gi0AS6gIt5oYoYFtAAjkREt4AauGOSBPXldcCw2ANXZ6DxklNKjqGKaBZQBfcHQX1pDdQmyA5g12seOK/PQaOQhSrSs4BW8D62Y91D/Txs9esHNNnO5gZsEVUBveAetxMQrAQ3GBa+Uq0jEVEUgAEuzgoKEYkACnCUIi0BM8BRiLQE8hTF7XiS4RDtKkJztmdcPMm3dSm70xSPV2Sv64kWSdM8u6KAHnCBTquAME+LiKKAEXCjAlpEpAKBQJrdrOOfpze33I16iYrSiaS3Ar30yPDG4gILFydzuXBHhITJD09lhjuAumVcCRzV9fZozU6Pd+yckm20WKoxwgGECkjvfTkRn9fVXaD0VpPdjfQuCnued0yHwGaC/7AyfZzEYpI1/6fAYTLJPwnv6IgpVSi9f/Dr7ESjcCqAvYDQA4NujykV+K5W+HWg9wC2AthHCHsBuwc0tj70JsY+QtgLYNUDmcey5G304OKIT7HZ58Du6rake0IzKt8DcgKp+5Ql70KRuYgtwD8B7COElYDGM8fyn6v+O205oQqALWB1hewKWF2BX26m4kDa4to4AAAAAElFTkSuQmCC'/>",
                        className: "markericon-garage",
                        bindPopUp: true
                    };
                    var div = domUtilities.createElement("div", ["custom-popup"]);
                    var company = domUtilities.createElementSpan(`${feature.properties.Name}`);
                    domUtilities.addChildAsLast(div, company);
                    layerProperties.popUpHtml = div;
                    break;
                case "RANGE":
                    layerProperties = {
                        circleMarker: {
                            weight: 0.5,
                            fillOpacity: 0.5,
                            fillColor: window.criticalColors[feature.properties.priority],
                            radius: 80 //features.properties.radius
                        }
                    }; break;
                case "FIRE":
                    layerProperties = {
                        "markerHTML": `<img class='mapIconEnergisa' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAACQUlEQVRYR6WWDVEkQQyFHw4OB+CAUwA4AAeHA1AAKAAH4AAcwDnAATgAB1x9y2QqO5t0Z/pStTW7O+nk5eWv9zQmN5KOJV1I+hgz8XNqb/AwAK4lfUm6kvQ4aOe/AZhfAMDGahll4EHSn4W3IRCjAN4lHQThrgYxAgDHAMiEVADk11QjzbSMALiTdNmwSmEeSjqbdJoFuhYAURE9z5bg9HbSNUZC/bUArP161W4sfE6K55Keo0MtANB87w5Vo7cjOH2afgDodzS0MgDmbN8BwJjltccA74nY679KOl0ezADQ4/S6UYchi6bivNch8/sMgEVLIZGGSuFVgLE3SAUp2UgEwPc5zo8knTSsv006FQDobHVFBCAasy3jsERknKsIusyJkIHelIscAID2tLqpgCANMLeTgrXRY8MA8L03JQ3cfMangNazwVGJwnRoLVrMJFtU3ubckh5AlUKo44M+1exnBU4qdjhPGrZSUKWfGxCO0c/Wb4WFTfCegZdOuxmFRExrok81R3dCxjj10JIdAN+FxDMXYIC5wIfqj6TXTWEN9ADYhuNZuWy00jCnbk0K0pWasNBK6TwNPYBWEYabrJOy7O6w1TkeADkFdSTNW01yJgPgB9fOJMxoo/LnDVYoVlQiAHP/m43lMspuPWuvbthfppQAmJqbHZAB4H/rcX/xHGHAsxk6Xw4iDwwQRMATWc77Xhb8XkmdtwDwDiNMM2a7DaCeY3tv+4DuoX3T+qnklu4ACCxUC5Er3d/FrToE/w/yUoAhphkBWwAAAABJRU5ErkJggg==' style='background-color:${feature.properties.criticalColor}'/>`,
                        "className": "markericon-fire"
                    }; break;
                case "THUNDER":
                    layerProperties = {
                        "markerHTML": `<img class='mapIconEnergisa' src='data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTguMS4xLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDEyIDEyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAxMiAxMjsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSIxNnB4IiBoZWlnaHQ9IjE2cHgiPgo8Zz4KCTxwb2x5Z29uIHBvaW50cz0iMTAsNSA2LjU1MSw1IDksMCA0LjcxNCwwIDIsNyA0LjQyOSw3IDMsMTIgICIgZmlsbD0iIzAwMDAwMCIvPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=' style='background-color:${feature.properties.criticalColor}'/>`,
                        "className": "markericon-thunder"
                    }; break;
            }
            layerProperties.clickCallback = this.onclickMarkerCallBack;
            return layerProperties;
        }.bind(this);

        this.onclickMarkerCallBack = function (obj) {
            console.log(obj);
            let url, action, routeData;
            var self = this;
            domUtilities.showElement(".loader");
            setTimeout(function () {
                try {
                    self.object = {};
                    self.object = {
                        date: obj.target.feature.properties.date || obj.target.feature.properties.collectionDate,
                        base: obj.target.feature.properties.base,
                        Type: {
                            "value": equipmentTypeAlias[obj.target.feature.properties.type.toUpperCase()],
                            "alias": "Tipo de Objeto",
                            "type": "string"
                        },
                        Id: {
                            "value": obj.target.feature.properties.id,
                            "alias": "Id",
                            "type": "string"
                        },
                        Feeder: {
                            "value": obj.target.feature.properties.feederId,
                            "alias": "Alimentador",
                            "type": "string"
                        },
                        Latitude: {
                            "alias": "Latitude",
                            "type": "number",
                            "value": obj.latlng.lat
                        },
                        Longitude: {
                            "alias": "Longitude",
                            "type": "number",
                            "value": obj.latlng.lng
                        },
                        Properties: [
                            {
                                "alias": "Tipo",
                                "type": "string",
                                "value": equipmentTypeAlias[obj.target.feature.properties.type.toUpperCase()]
                            },
                            {
                                "alias": "Concessão",
                                "type": "string",
                                "value": obj.target.feature.properties.concession
                            },
                            {
                                "alias": "Prioridade da linha",
                                "type": "number",
                                "value": obj.target.feature.properties.linePriority
                            },
                            {
                                "alias": "Satélite",
                                "type": "string",
                                "value": obj.target.feature.properties.satellite
                            },
                            {
                                "alias": "Data da coleta",
                                "type": "date",
                                "value": obj.target.feature.properties.collectionDate
                            },
                            {
                                "alias": "Altura (m)",
                                "type": "number",
                                "value": obj.target.feature.properties.heigth
                            },
                            {
                                "alias": "Tensão",
                                "type": "string",
                                "value": obj.target.feature.properties.voltage
                            },
                            {
                                "alias": "Distância da rede (m)",
                                "type": "number",
                                "value": obj.target.feature.properties.distance
                            },
                            {
                                "alias": "Previsão",
                                "type": "date",
                                "value": obj.target.feature.properties.forecastDate
                            },
                            {
                                "alias": "Data da atualização",
                                "type": "date",
                                "value": obj.target.feature.properties.lastUpdate
                            },
                            {
                                "alias": "Descargas previstas",
                                "type": "number",
                                "value": obj.target.feature.properties.projectedElectricalDischarges
                            },
                            {
                                "alias": "Probabilidade",
                                "type": "percent",
                                "value": obj.target.feature.properties.probability
                            },
                            {
                                "alias": "Criticidade",
                                "type": "number",
                                "value": obj.target.feature.properties.criticality
                            },
                            {
                                "alias": "Prioridade",
                                "type": "string",
                                "value": obj.target.feature.properties.priority
                            }
                        ]
                    };

                    self.object.type = obj.target.feature.properties.equipmentType;
                    self.streetViewImageUrl = "http://maps.googleapis.com/maps/api/streetview?size=800x600&location=" + obj.latlng.lat + "," + obj.latlng.lng + "&fov=120&heading=235&pitch=10&sensor=false&key=" + "AIzaSyCrfAGaiYykAJ09B9O6BRodqpiYK10D_wc";
                    self.streetViewLink = "https://www.google.com/maps?layer=c&cbll=" + obj.latlng.lat + "," + obj.latlng.lng;
                    domUtilities.addCssClasses('#page-wrapper', ["w-actionbar"]);
                    var result;
                    var latlng = {
                        lat: obj.latlng.lat,
                        lng: obj.latlng.lng
                    }

                    if (!geocoder) {
                        domUtilities.hideElement(".loader");
                        //domUtilities.showMessage("warning", "", "O plugin do Google® não está disponível. Aperte ctrl+F5");
                    } else {
                        geocoder.geocode({ 'location': latlng }, function (results, status) {
                            try {
                                if (status === 'OK') {
                                    var address = {
                                        street: "",
                                        city: "",
                                        state: "",
                                        country: "",
                                    };
                                    results.forEach(result => {
                                        var type = result.types[0];
                                        var name = result.address_components[0].long_name;
                                        switch (type) {
                                            case "route": address.street = (name == "Unnamed Road" ? "Desconhecido" : name); break;
                                            case "administrative_area_level_2": address.city = name; break;
                                            case "administrative_area_level_1": address.state = name; break;
                                            case "country": address.country = name; break;
                                        }
                                    });
                                    self.object.address = address;
                                } else {
                                    throw "";
                                }
                                if (!$scope.$phase) {
                                    $scope.$apply();
                                }
                            } catch (error) {
                                domUtilities.showMessage("error", "", "Erro ao tentar recupearar as informações da localidade");
                            }
                            domUtilities.hideElement(".loader");
                        }.bind(this));
                    }
                    if (!$scope.$phase) {
                        $scope.$apply();
                    }
                } catch (error) {
                    console.error(error);
                    domUtilities.showMessage("error", undefined, error);
                }
            }, 50);

        }.bind(this);

        this.loadAllocations = function () {
            var self = this;
            connectionUtilities.sendAjaxRequest('/api/getAllocations', "GET", undefined,
                function (data) {
                    if (!data)
                        return;

                    self.allocations = data;
                }, undefined, true);
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        };

        this.showAllocationsInMap = function (id) {
            var allocation = _.filter(this.allocations, value => {
                return value.id == id;
            });
            var geojson = {
                "type": "FeatureCollection",
                "features": []
            };
            allocation[0].events.forEach(event => {
                event.type = event.point.type;
                geojson.features.push({
                    "type": "Feature",
                    "properties": event.point,
                    "geometry": {
                        "type": "Point",
                        "coordinates": [
                            event.point.lat,
                            event.point.lng,
                        ]
                    }
                })
            });

            allocation[0].events.forEach(event => {
                geojson.features.push({
                    "type": "Feature",
                    "properties": {
                        type: 'range',
                        priority: event.point.priority
                    },
                    "geometry": {
                        "type": "Point",
                        "coordinates": [
                            event.point.lat,
                            event.point.lng,
                        ]
                    }
                });
            });
            mapScope.addLayerOnMap(geojson,
                "Point",
                "Eventos",
                "Eventos",
                undefined,
                true,
                false,
                true,
                false);

        };

        this.cancelGeneration = function () {
            window.location.replace('#!/allocationteam');

        };

        this.generateAllocation = function () {
            var self = this;
            var events;
            try {
                if (self.generateParams.area == "line") {
                    events = _.filter($scope.main.alarmsList, alarm => {
                        return self.generateParams.areas.indexOf(alarm.Data.point.concession) >= 0;
                    });
                } else {
                    events = $scope.main.alarmsList;
                }

                events = _.filter(events, alarm => {
                    return self.generateParams.priorities.indexOf(alarm.Data.point.priority) >= 0;
                });

                if (self.generateParams.fire && !self.generateParams.thunder) {
                    events = _.filter(events, alarm => {
                        return alarm.Data.point.type == "fire";
                    });

                }
                if (!self.generateParams.fire && self.generateParams.thunder) {
                    events = _.filter(events, alarm => {
                        return alarm.Data.point.type == "thunder";
                    });
                }

                var areas = _.filter(self.bases, base => {
                    return self.generateParams.bases.indexOf(base.Name) >= 0;
                });

                events.forEach(e => {
                    e.Data.point.base = areas[Math.floor(Math.random() * areas.length)];
                });

                if (self.generateParams.workTime <= 0 || self.generateParams.velocity <= 0) {
                    throw new Error("Parâmetros");
                    return
                }
                connectionUtilities.sendAjaxRequest('/api/saveAllocation', "POST",
                    JSON.stringify({
                        id: "automatic_" + Date.now(),
                        date: new Date(),
                        dispached: false,
                        lines: self.generateParams.areas,
                        workTime: self.generateParams.workTime,
                        velocity: self.generateParams.velocity,
                        events: events.map(e => { return e.Data })
                    }),
                    function () {
                        domUtilities.showMessage("success", "Salvo com sucesso!");
                        window.location.replace('#!/allocationteam');
                    },
                    function () {
                        domUtilities.showMessage("error", "Erro ao tentar gerar a alocação!");
                    },
                    true);

            } catch (error) {
                domUtilities.showMessage("error", "Verifique os parâmetros!")
            }
        };

        this.showAllocationDetails = function (c) {
            this.allocations.forEach(function (e) {
                e.active = false;
            });
            c.active = true;
            this.selectedAllocation = c;
            $('#allocationModal').modal('show');
            //domUtilities.showModal("#allocationModal");
        };

        this.editAllocation = function (obj) {
            window.location.replace('#!/generateallocation/?isEditing=true&allocationId=' + this.selectedAllocation.id);
        };

        $scope.registerMapOnController = function () {
            var self = $scope.team;
            mapScope = window.mapScope;
            mapController = window.mapController;
            mapScope.properties.customLayerCallback = self.customMarker;
            self.loadAlarms();
            self.getBases();
            $(".map-search-area").hide();
            if ($routeParams.allocationId) {
                self.showAllocationsInMap($routeParams.allocationId);
            }
            try {
                geocoder = new google.maps.Geocoder;
            } catch (error) {
                domUtilities.showMessage("error", "Não conectado");
            }
        };

        this.loadAllocations();

        if ($routeParams.isEditing == 'true') {
            this.isEditing = true;
        }
        setTimeout(() => {
            //domUtilities.hideModal("#allocationModal");
            domUtilities.hideElement('.modal-backdrop');
        }, 50);
    }
})();

function actionOnActionBar() {
    if (location.hash.indexOf('generateallocation') >= 0) {
        $(".forecast").hide();
    }
}