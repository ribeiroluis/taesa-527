///<reference path="../scripts/services/connections.js"/>
///<reference path="../scripts/services/map.js"/>
(function () {
    "use strict";

    //Register controller
    window.app.controller("mapController", mapController);

    function mapController($scope) {
        var map, view, layerList, groupLayer, icons;
        var _Point;
        var date = new Date(new Date().setDate(new Date().getDate() - 15))
        var endDate = new Date();

        this.getLastMonitoredData = function () {
            var data;
            var self = this;
            connectionUtilities.sendAjaxRequest('./api/getLastMonitoredDate/thunder', "GET", undefined,
                function (data) {
                    var date = new Date(data);
                    self.searchDate.thunder = {
                        startDate: date,
                        endDate: date,
                        minDate: new Date(new Date(data).setDate(date.getDate() - 5))
                    };
                }, function () {
                    domUtilities.showMessage("Error", undefined, "Erro ao tentar buscar a ultima data");
                }, true);
            connectionUtilities.sendAjaxRequest('./api/getLastMonitoredDate/fireoutbreak', "GET", undefined,
                function (data) {
                    var date = new Date(data);
                    self.searchDate.fireoutbreak = {
                        startDate: date,
                        endDate: date,
                        minDate: new Date(new Date(data).setDate(date.getDate() - 5))
                    };
                }, function () {
                    domUtilities.showMessage("Error", undefined, "Erro ao tentar buscar a ultima data");
                }, true);
        };

        this.getConcessions = function () {
            var data;
            var self = this;
            connectionUtilities.sendAjaxRequest('./api/getConcessionsList', "GET", undefined,
                function (_data) {
                    data = _data;
                }, function () {
                    domUtilities.showMessage("Error", undefined, "Erro ao tentar buscar concessões");
                }, true);

            var concessions = [];
            data.forEach(element => {
                concessions.push({ concession: element });
            });

            // concessions.push(
            //     { concession: "PREVISAO" },
            //     { concession: "FOCOS" }
            // );

            domUtilities.enableAutoCompleteOnElement("#searchInput",
                concessions, "concession", self.search);
        };

        this.getIcons = function () {
            try {
                connectionUtilities.sendAjaxRequest('/api/getIcons/', "GET", undefined,
                    function (data) {
                        if (!data) {
                            throw new Error();
                        }
                        icons = data;
                    }, undefined, true);
            } catch (e) {
                console.error(e)
                domUtilities.showMessage("error", undefined, "Erro ao tentar buscar os icones cadastradaso, verifique");
            }
        };

        this.search = function (param, $event) {
            if (!param || $event && $event.keyCode !== 13) {
                return;
            }
            searchData(param);
        };

        function searchData(param) {
            try {
                domUtilities.showElement(".loader");
                var url = './api/search/' + param;
                setTimeout(() => {
                    connectionUtilities.sendAjaxRequest(url, "GET", undefined,
                        function (data) {
                            switch (data.type.toUpperCase()) {
                                case "FIREOUTBREAKS":
                                    map.removeGroupFromControlAndMap("Focos de incêndio");
                                    plotForecast(data.data);
                                    break;
                                case "THUNDERS":
                                    map.removeGroupFromControlAndMap("Raios");
                                    plotForecast(data.data);
                                    break;
                                case "FORECASTFIRE":
                                    map.removeGroupFromControlAndMap("Previsão de incêndio");
                                    plotForecast(data.data);
                                    break;
                                case "FORECASTTHUNDER":
                                    map.removeGroupFromControlAndMap("Previsão de raios");
                                    plotForecast(data.data);
                                    break;
                                case "FORECASTWIND":
                                    map.removeGroupFromControlAndMap("Previsão de Ventos");
                                    plotForecast(data.data);
                                    break;
                                case "CONCESSION":
                                    plotDistribuitionNetwork(data.data);
                                    break;
                            }
                            if (!data.type) {
                                domUtilities.hideElement(".loader");
                                domUtilities.showMessage("warning", "", "Tipo de busca inválido");
                                return;
                            }
                            //domUtilities.hideElement(".loader");
                        }, function () {
                            domUtilities.hideElement(".loader");
                            domUtilities.showMessage("error", undefined, "Erro ao tentar realizar a busca.");
                            console.error(error);
                        }, true);
                }, 0);
            }
            catch (error) {
                domUtilities.hideElement(".loader");
                domUtilities.showMessage("error", undefined, "Erro ao tentar realizar a busca.");
                console.error(error);
            }
        };

        var loadThundersLayers = function () {
            $scope.map.isPlay = false;
            $scope.map.animate();
            domUtilities.showElement(".loader");
            var object = $scope.map.searchDate.thunder;
            var url = `./api/getMonitoredEvents/thunder/${object.startDate.toISOString()}/${object.endDate.toISOString()}`;
            connectionUtilities.sendAjaxRequest(url, 'GET', undefined,
                (data) => {
                    if (!data) return;

                    var graphics = [];
                    var index = 1;
                    data.forEach(r => {
                        r.points.forEach(p => {
                            graphics.push({
                                geometry: new _Point({
                                    x: p.geometry.coordinates[0],
                                    y: p.geometry.coordinates[1]
                                }),
                                attributes: {
                                    title: "Descarga atmosférica",
                                    ObjectID: index,
                                    timeStamp: new Date(p.properties.collectionDate).getTime(),
                                    collectionDate: new Date(p.properties.collectionDate).toLocaleString(),
                                    concession: p.properties.concession,
                                    linePriority: p.properties.linePriority,
                                    distance: p.properties.distance,
                                    thunderType: p.properties.thunderType,
                                    discharges: p.properties.electricalDischarges,
                                    intensity: p.properties.intensity
                                }
                            });
                        });
                    });
                    graphics = _.sortBy(graphics, function (g) { return g.attributes.timeStamp; });
                    var fields = [
                        {
                            name: "ObjectID",
                            alias: "ObjectID",
                            type: "oid"
                        }, {
                            name: "timeStamp",
                            alias: "timeStamp",
                            type: "integer"
                        }, {
                            name: "title",
                            alias: "title",
                            type: "string"
                        }, {
                            name: "collectionDate",
                            alias: "Data da coleta",
                            type: "string"
                        }, {
                            name: "linePriority",
                            alias: "linePriority",
                            type: "string"
                        }, {
                            name: "concession",
                            alias: "linha",
                            type: "string"
                        }, {
                            name: "distance",
                            alias: "distance",
                            type: "double"
                        }, {
                            name: "thunderType",
                            alias: "thunderType",
                            type: "string"
                        }, {
                            name: "discharges",
                            alias: "discharges",
                            type: "integer"
                        }, {
                            name: "intensity",
                            alias: "intensity",
                            type: "string"
                        }
                    ];
                    var pTemplate = {
                        title: "Descarga atmosférica",
                        content: [{
                            type: "fields",
                            fieldInfos: [
                                {
                                    fieldName: "collectionDate",
                                    label: "Data da coleta",
                                    format: {
                                        dateFormat: "short"
                                    }
                                }, {
                                    fieldName: "concession",
                                    label: "Linha afetada",
                                    visible: true
                                }, {
                                    fieldName: "linePriority",
                                    label: "Prioridade da linha",
                                    format: {
                                        places: 3,
                                        digitSeparator: true
                                    }
                                }, {
                                    fieldName: "distance",
                                    label: "Distância da rede (m)",
                                    format: {
                                        places: 3,
                                        digitSeparator: true
                                    }
                                }, {
                                    fieldName: "thunderType",
                                    label: "Tipo de descarga",
                                    visible: true
                                }, {
                                    fieldName: "discharges",
                                    label: "Quantidade de descargas",
                                    visible: true
                                }, {
                                    fieldName: "intensity",
                                    label: "Intensidade",
                                    visible: true
                                }
                            ]
                        }]
                    };
                    var isVisible = false;
                    if (object.layer) {
                        isVisible = object.layer.visible;
                        groupLayer.remove(object.layer);
                        delete object.layer;
                    }
                    addFeatureLayerOnMap(graphics, fields, 'point', pTemplate, isVisible, 'thunder', object,
                        l => {
                            object.layer = l;
                            updateSearchContainer(object);
                        });
                }, (err) => {
                    console.error(err);
                });
        };

        var loadFireOutbreakLayers = function () {
            $scope.map.isPlay = false;
            $scope.map.animate();
            domUtilities.showElement(".loader");
            var object = $scope.map.searchDate.fireoutbreak;
            var url = `./api/getMonitoredEvents/fireoutbreak/${object.startDate.toISOString()}/${object.endDate.toISOString()}`;
            connectionUtilities.sendAjaxRequest(url, 'GET', undefined,
                (data) => {
                    if (!data) return;
                    var graphics = [];
                    data.forEach(r => {
                        r.points.forEach(p => {
                            graphics.push({
                                geometry: new _Point({
                                    x: p.geometry.coordinates[0],
                                    y: p.geometry.coordinates[1]
                                }),
                                attributes: {
                                    title: "Queimada",
                                    ObjectID: p.properties.id,
                                    timeStamp: new Date(p.properties.collectionDate).getTime(),
                                    collectionDate: new Date(p.properties.collectionDate).toLocaleString(),
                                    satellite: p.properties.satellite,
                                    concession: p.properties.concession,
                                    linePriority: p.properties.linePriority,
                                    distance: p.properties.distance
                                }
                            });
                        });
                    });
                    graphics = _.sortBy(graphics, function (g) { return g.attributes.timeStamp; });
                    var fields = [{
                        name: "ObjectID",
                        alias: "ObjectID",
                        type: "oid"
                    }, {
                        name: "timeStamp",
                        alias: "timeStamp",
                        type: "integer"
                    }, {
                        name: "title",
                        alias: "title",
                        type: "string"
                    }, {
                        name: "collectionDate",
                        alias: "Data da coleta",
                        type: "string"
                    }, {
                        name: "satellite",
                        alias: "Satélite",
                        type: "string"
                    }, {
                        name: "linePriority",
                        alias: "linePriority",
                        type: "string"
                    }, {
                        name: "concession",
                        alias: "linha",
                        type: "string"
                    }, {
                        name: "distance",
                        alias: "distance",
                        type: "double"
                    }];
                    var pTemplate = {
                        title: "Queimada",
                        content: [{
                            type: "fields",
                            fieldInfos: [
                                {
                                    fieldName: "collectionDate",
                                    label: "Data da coleta",
                                    format: {
                                        dateFormat: "short"
                                    }
                                }, {
                                    fieldName: "concession",
                                    label: "Linha afetada",
                                    visible: true
                                }, {
                                    fieldName: "linePriority",
                                    label: "Prioridade da linha",
                                    format: {
                                        places: 3,
                                        digitSeparator: true
                                    }
                                }, {
                                    fieldName: "distance",
                                    label: "Distância da rede (m)",
                                    format: {
                                        places: 3,
                                        digitSeparator: true
                                    }
                                }, {
                                    fieldName: "satellite",
                                    label: "Satélite",
                                    visible: true
                                }
                            ]
                        }]
                    };
                    var isVisible = false;
                    if (object.layer) {
                        isVisible = object.layer.visible;
                        groupLayer.remove(object.layer);
                        delete object.layer;
                    }
                    addFeatureLayerOnMap(graphics, fields, 'point', pTemplate, isVisible, 'fireoutbreak', object,
                        l => {
                            object.layer = l;
                            updateSearchContainer(object);
                        });
                }, (err) => {
                    console.error(err);
                });
        };

        var loadThunderForecast = function () {
            $scope.map.isPlay = false;
            $scope.map.animate();
            domUtilities.showElement(".loader");
            var object = $scope.map.searchDate.foreCastThunder;
            var url = `./api/getForecastEvents/thunders/${object.startDate.toISOString()}/${object.endDate.toISOString()}`;
            connectionUtilities.sendAjaxRequest(url, 'GET', undefined,
                (data) => {
                    var graphics = [];
                    data.forEach(t => {
                        t.points.forEach((p, i) => {
                            graphics.push({
                                geometry: new _Point({
                                    x: p.geometry.coordinates[0],
                                    y: p.geometry.coordinates[1]
                                }),
                                attributes: {
                                    title: "Previsão de descargas atmosféricas",
                                    ObjectID: p.properties.tempId,
                                    timeStamp: new Date(p.properties.date).getTime(),
                                    lastUpdate: new Date(p.properties.lastUpdate).toLocaleString(),
                                    date: new Date(p.properties.date).toLocaleString(),
                                    probability: `${p.properties.probability * 100}%`,
                                    concession: p.properties.concession,
                                    priority: p.properties.priority,
                                    discharges: p.properties.projectedElectricalDischarges,
                                    thunderType: p.properties.thunderType,
                                    distance: p.properties.distance
                                }
                            });
                        });
                    });
                    graphics = _.sortBy(graphics, function (g) { return g.attributes.timeStamp; });
                    var fields = [
                        {
                            name: "ObjectID",
                            alias: "ObjectID",
                            type: "oid"
                        }, {
                            name: "timeStamp",
                            alias: "timeStamp",
                            type: "integer"
                        }, {
                            name: "title",
                            alias: "title",
                            type: "string"
                        }, {
                            name: "date",
                            alias: "Previsto",
                            type: "string"
                        }, {
                            name: "probability",
                            alias: "Probabilidade",
                            type: "string"
                        }, {
                            name: "lastUpdate",
                            alias: "Última autalização",
                            type: "string"
                        }, {
                            name: "concession",
                            alias: "linha",
                            type: "string"
                        }, {
                            name: "priority",
                            alias: "Prioridade",
                            type: "string"
                        }, {
                            name: "thunderType",
                            alias: "Tipo de descarga",
                            type: "string"
                        }, {
                            name: "discharges",
                            alias: "Quantidade de descargas",
                            type: "integer"
                        }, {
                            name: "distance",
                            alias: "distance",
                            type: "double"
                        }];
                    var pTemplate = {
                        title: "{title}",
                        content: [{
                            type: "fields",
                            fieldInfos: [{
                                fieldName: "date",
                                label: "Previsto",
                                visible: true
                            }, {
                                fieldName: "probability",
                                label: "Probabilidade",
                                visible: true
                            }, {
                                fieldName: "concession",
                                label: "Linha",
                                visible: true
                            }, {
                                fieldName: "distance",
                                label: "Distância da rede (m)",
                                format: {
                                    places: 3,
                                    digitSeparator: true
                                }
                            }, {
                                fieldName: "priority",
                                label: "Prioridade",
                                visible: true
                            }, {
                                fieldName: "thunderType",
                                label: "Tipo de descarga",
                                visible: true
                            }, {
                                fieldName: "discharges",
                                label: "Quantiade de descargas",
                                visible: true
                            }, {
                                fieldName: "lastUpdate",
                                label: "Última autalização",
                                visible: true
                            }]
                        }]
                    };
                    var isVisible = false;
                    if (object.layer) {
                        isVisible = object.layer.visible;
                        groupLayer.remove(object.layer);
                        delete object.layer;
                    }
                    addFeatureLayerOnMap(graphics, fields, 'point', pTemplate, isVisible, 'foreCastThunder', object,
                        l => {
                            object.layer = l;
                            updateSearchContainer(object);
                        });

                }, (err) => {
                    dbugger;
                });
        };

        var loadWindsForecast = function () {
            $scope.map.isPlay = false;
            $scope.map.animate();
            domUtilities.showElement(".loader");
            var object = $scope.map.searchDate.winds;
            var url = `./api/getForecastEvents/winds/${object.startDate.toISOString()}/${object.endDate.toISOString()}`;
            connectionUtilities.sendAjaxRequest(url, 'GET', undefined,
                (data) => {
                    var graphics = [];
                    data.forEach(t => {
                        t.points.forEach((p, i) => {
                            graphics.push({
                                geometry: new _Point({
                                    x: p.geometry.coordinates[0],
                                    y: p.geometry.coordinates[1]
                                }),
                                attributes: {
                                    ObjectID: p.properties.tempId,
                                    timeStamp: new Date(p.properties.date).getTime(),
                                    title: "Previsão de ventos severos",
                                    lastUpdate: new Date(p.properties.lastUpdate).toLocaleString(),
                                    date: new Date(p.properties.date).toLocaleString(),
                                    probability: `${p.properties.probability * 100}%`,
                                    concession: p.properties.concession,
                                    priority: p.properties.priority,
                                    windVelocity: p.properties.windVelocity,
                                    windDirection: p.properties.windDirection,
                                    distance: p.properties.distance
                                }
                            });
                        });
                    });
                    graphics = _.sortBy(graphics, function (g) { return g.attributes.timeStamp; });
                    var fields = [
                        {
                            name: "ObjectID",
                            alias: "ObjectID",
                            type: "oid"
                        }, {
                            name: "timeStamp",
                            alias: "timeStamp",
                            type: "integer"
                        }, {
                            name: "title",
                            alias: "title",
                            type: "string"
                        }, {
                            name: "date",
                            alias: "Previsto",
                            type: "string"
                        }, {
                            name: "probability",
                            alias: "Probabilidade",
                            type: "string"
                        }, {
                            name: "lastUpdate",
                            alias: "Última autalização",
                            type: "string"
                        }, {
                            name: "concession",
                            alias: "linha",
                            type: "string"
                        }, {
                            name: "priority",
                            alias: "Prioridade",
                            type: "string"
                        }, {
                            name: "windVelocity",
                            alias: "windVelocity",
                            type: "string"
                        }, {
                            name: "windDirection",
                            alias: "windDirection",
                            type: "double"
                        }, {
                            name: "distance",
                            alias: "distance",
                            type: "double"
                        }];
                    var pTemplate = {
                        title: "Ventos severos",
                        content: [{
                            type: "fields",
                            fieldInfos: [
                                {
                                    fieldName: "date",
                                    label: "Previsão",
                                    visible: true,
                                    format: {
                                        dateFormat: "short-date-le-short-time-24"
                                    }
                                }, {
                                    fieldName: "windVelocity",
                                    label: "Velocidade do vento (km/h)",
                                    visible: true
                                }, {
                                    fieldName: "windDirection",
                                    label: "Direção do vento",
                                    visible: true
                                }, {
                                    fieldName: "probability",
                                    label: "Probabiliade",
                                }, {
                                    fieldName: "concession",
                                    label: "Linha afetada",
                                    visible: true
                                }, {
                                    fieldName: "distance",
                                    label: "Distância da rede (m)",
                                    format: {
                                        places: 3,
                                        digitSeparator: true
                                    }
                                }, {
                                    fieldName: "priority",
                                    label: "Prioridade",
                                    visible: true
                                }, {
                                    fieldName: "lastUpdate",
                                    label: "Data da atualização",
                                    visible: true,
                                    format: {
                                        dateFormat: "short-date-le-short-time-24"
                                    }
                                }
                            ]
                        }]
                    };
                    var isVisible = false;
                    if (object.layer) {
                        isVisible = object.layer.visible;
                        groupLayer.remove(object.layer);
                        delete object.layer;
                    }
                    addFeatureLayerOnMap(graphics, fields, 'point', pTemplate, isVisible, 'winds', object,
                        l => {
                            object.layer = l;
                            updateSearchContainer(object);
                        });

                }, (err) => {
                    dbugger;
                });

        };

        function parseDate(date) {
            return `${date.toISOString().split('T')[0]}T${date.toLocaleTimeString()}.${date.getMilliseconds()}`
        };

        function getIcon(type) {
            var icon = icons.find(i => { return i.element == type });
            return icon.path;
        };

        function plotDistribuitionNetwork(data) {
            var line, points;
            require(["esri/layers/GraphicsLayer", "esri/Graphic"],
                function (GraphicsLayer, Graphic) {
                    var layer = new GraphicsLayer({
                        title: data[0].FeederId
                    });
                    data.forEach(e => {
                        switch (e.GeoJsonType) {
                            case "LineString":
                                var paths = [];
                                var attributes;
                                e.GeoJson.features.forEach(f => {
                                    paths = paths.concat(f.geometry.coordinates);
                                    attributes = f.properties;
                                });
                                var name = `${e.FeederId} - ${e.EquipmentType}`;
                                var polyline = {
                                    type: "polyline",  // autocasts as new Polyline()
                                    paths: paths
                                };

                                var polylineSymbol = {
                                    type: "simple-line",  // autocasts as SimpleLineSymbol()
                                    color: [0, 0, 0],
                                    width: 2
                                };

                                var polylineAtt = attributes

                                var polylineGraphic = new Graphic({
                                    geometry: polyline,
                                    symbol: polylineSymbol,
                                    attributes: polylineAtt,
                                    popupTemplate: {
                                        title: "Linha - {Name}",
                                        content: [{
                                            type: "fields",
                                            fieldInfos: [{
                                                fieldName: "Tensão",
                                                visible: true
                                            }, {
                                                fieldName: "priority",
                                                label: "Prioridade",
                                                visible: true,
                                                format: {
                                                    places: 3,
                                                    digitSeparator: true
                                                }
                                            }]
                                        }]
                                    }
                                });
                                layer.add(polylineGraphic);
                                break;
                            case "Point":
                                e.GeoJson.features.forEach(f => {
                                    var point = {
                                        type: "point",
                                        longitude: f.geometry.coordinates[0],
                                        latitude: f.geometry.coordinates[1]
                                    };
                                    var markerSymbol = {
                                        type: "picture-marker",  // autocasts as new PictureMarkerSymbol()
                                        url: getIcon('tower'),
                                        width: "20px",
                                        height: "20px"
                                    };
                                    var pointGraphic = new Graphic({
                                        geometry: point,
                                        symbol: markerSymbol,
                                        attributes: f.properties,
                                        popupTemplate: {
                                            title: "Torre - {concession}",
                                            content: [{
                                                type: "fields",
                                                fieldInfos: [
                                                    {
                                                        fieldName: "id",
                                                        label: "ID",
                                                        visible: true
                                                    }, {
                                                        fieldName: "heigth",
                                                        label: "Altura (m)",
                                                        visible: true
                                                    }, {
                                                        fieldName: "voltage",
                                                        label: "Tensão",
                                                        visible: true
                                                    }, {
                                                        fieldName: "linePriority",
                                                        label: "Prioridade da linha",
                                                        visible: true,
                                                        format: {
                                                            places: 3,
                                                            digitSeparator: true
                                                        }
                                                    }]
                                            }]
                                        }
                                    });
                                    layer.add(pointGraphic);
                                });
                                break;
                        }
                    });
                    map.add(layer);
                    domUtilities.hideElement(".loader");
                });
        };

        function addFeatureLayerOnMap(
            graphics,
            fields,
            geometryType,
            popupTemplate,
            isVisible,
            customId,
            eventObject,
            callback) {
            require([
                "esri/layers/FeatureLayer",
                "dojo/domReady!"
            ], function (FeatureLayer) {
                if (graphics.length == 0) {
                    domUtilities.showMessage('warning', 'Não foram encontrados resultados para a busca');
                    domUtilities.hideElement(".loader");
                    return;
                }

                var renderer = {
                    type: "simple",
                    symbol: {
                        type: "simple-marker",
                        size: 15,
                        color: "rgb(0, 0, 0)",
                        outline: null
                    },
                    visualVariables: [{
                        type: "color",
                        field: "timeStamp",
                        size: 15,
                        stops: [{
                            value: graphics[0].attributes.timeStamp,
                            color: eventObject.colorOptions[0]
                        }, {
                            value: graphics[graphics.length - 1].attributes.timeStamp,
                            color: eventObject.colorOptions[1]
                        }]
                    }]
                };
                var layer = new FeatureLayer({
                    title: eventObject.title,
                    customId: customId,
                    visible: isVisible,
                    source: graphics,
                    fields: fields,
                    objectIdField: 'ObjectID',
                    renderer: renderer,
                    spatialReference: {
                        wkid: 4326
                    },
                    geometryType: geometryType,
                    popupTemplate: popupTemplate
                });
                // if (layerEvent) {
                //     layer.on(layerEvent.event, layerEvent.callback);
                // }
                //map.add(layer);
                groupLayer.add(layer);
                if (callback) {
                    callback(layer);
                }
                layer.on("layerview-create", function (event) {
                    domUtilities.hideElement(".loader");
                    var interval = setInterval(() => {
                        var ul = $('ul.esri-layer-list__list.esri-layer-list__list--exclusive');
                        console.log(ul);
                        if (ul.length == 0) return;

                        var lis = ul.find('li');
                        lis.each(li => {
                            $(lis[li].firstChild).on('click', e => {
                                e.stopPropagation();
                                var title = $(e.currentTarget).find('span[title]').attr('title');
                                if (e.currentTarget.firstChild.getAttribute('aria-checked') == 'false') {
                                    var eventObjects = angular.element('#search-container').scope().map.searchDate;
                                    _.each(eventObjects, e => {
                                        if (e.title == title) {
                                            if ($('span[title="Eventos"]').parent().attr('aria-checked') == "true") {
                                                $('#search-container').show();
                                            }
                                            updateSearchContainer(e);
                                        }
                                    });
                                }
                            });
                        });
                        clearInterval(interval);
                        domUtilities.hideElement(".loader");
                        $('span[title="Eventos"]').parent().on('click', e => {
                            debugger;
                            if (e.currentTarget.getAttribute('aria-checked') == "true") {
                                $('#search-container').hide();
                                return;
                            } else {
                                if (lis.find('[aria-checked="true"]').length > 0) {
                                    $('#search-container').show();
                                }
                            }
                            e.stopPropagation();
                        });
                    }, 1000);
                });
            });
        };

        this.showSideBar = function () {
            if (this.sideBarOpen) {
                $('.sidebar').removeClass('close');
                $('.concert-logo').removeClass('close');
                $('.full-map').addClass('open');
                $('.map').addClass('open');
                $('.menu-bar').removeClass('close');
            } else {
                $('.sidebar').addClass('close');
                $('.concert-logo').addClass('close');
                $('.full-map').removeClass('open');
                $('.map').removeClass('open');
                $('.menu-bar').addClass('close');
            }
        };

        function updateSearchContainer(object) {
            $scope.map.isPlay = false;
            $scope.map.animate();
            var existMinDate = new Date(object.layer.source.items[0].attributes.timeStamp);
            var existMaxDate = new Date(object.layer.source.items[object.layer.source.items.length - 1].attributes.timeStamp);
            $('#hourValue').text('Hora: ');
            $('#containerTitle').text(object.layer.title);
            $('#startDate').val(parseDate(object.startDate));
            $('#endDate').val(parseDate(object.endDate));
            $('#startDate').attr('customId', object.layer.customId);
            $('#endDate').attr('customId', object.layer.customId);
            $('#color').css('background', `linear-gradient(to right, ${object.colorOptions[0]}, ${object.colorOptions[1]}`);
            $('#min').text('Min: ' + existMinDate.toLocaleString());
            $('#max').text('Max: ' + existMaxDate.toLocaleString());
            $('#range').attr('min', existMinDate.getTime());
            $('#range').attr('max', existMaxDate.getTime());

            $("#searchButton").unbind();
            $("#searchButton").on('click', e => {
                object.callback();
            });
            $('#range').unbind();
            $('#range').on('change', e => {
                $('#hourValue').text('Hora: ' + new Date(eval(e.target.value)).toLocaleString());
                var value = parseInt(e.target.value);
                //var searchDate = angular.element('#search-container').scope().map.searchDate[layer.customId];
                object.layer.renderer = createRenderer(value, existMinDate.getTime(), existMaxDate.getTime(), object.colorOptions);
            });
        };

        function createRenderer(time, startTime, endTime, colorOptions) {
            var opacityStops = [{
                opacity: 1,
                value: time
            },
            {
                opacity: 0,
                value: time + 60000
            }];
            return {
                type: "simple",
                symbol: {
                    type: "simple-marker",
                    size: 15,
                    color: "rgb(0, 0, 0)",
                    outline: null
                },
                visualVariables: [{
                    type: "opacity",
                    field: "timeStamp",
                    stops: opacityStops,
                }, {
                    type: "color",
                    field: "timeStamp",
                    size: 15,
                    stops: [{
                        value: startTime,
                        color: colorOptions[0]
                    }, {
                        value: endTime,
                        color: colorOptions[1]
                    }]
                }]
            };
        };

        var frameId;
        this.animate = function () {
            function frame() {
                var range = $('#range');
                var value = parseInt(range.val());
                var min = parseInt(range.attr('min'));
                var max = parseInt(range.attr('max'));
                //value += 1000 * 60 * 60 * 24;
                value += TIME_LAPSE;
                if (value > max) {
                    value = min;
                }
                range.val(value);
                range.trigger('change');
                frameId = requestAnimationFrame(frame);
            }
            if (this.isPlay) {
                requestAnimationFrame(frame);
            } else {
                cancelAnimationFrame(frameId);
            }
            if (!$scope.$$phase) $scope.$apply();
        };

        this.initializeArcGisMap = function () {
            var self = this;
            require([
                "esri/Map",
                "esri/views/MapView",
                "esri/widgets/Search",
                "esri/widgets/BasemapGallery",
                "esri/widgets/Expand",
                "esri/widgets/Locate",
                "esri/widgets/LayerList",
                "esri/widgets/Fullscreen",
                "esri/tasks/Locator",
                "esri/geometry/Point",
                "esri/geometry/Polyline",
                "esri/widgets/Legend",
                "esri/core/watchUtils",
                "esri/layers/GroupLayer",
                "dojo/on",
                "dojo/domReady!",
            ], function (Map,
                MapView,
                Search,
                BasemapGallery,
                Expand,
                Locate,
                LayerList,
                Fullscreen,
                Locator,
                Point,
                Polyline,
                Legend,
                watchUtils,
                GroupLayer) {
                    _Point = Point
                    map = new Map({
                        basemap: "satellite"
                    });
                    view = new MapView({
                        container: "arcGisMap",
                        map: map,
                        zoom: 4,
                        center: [-44, -19] // longitude, latitude
                    });
                    view.when(function () {
                        $(".search").show();
                        groupLayer = new GroupLayer({
                            title: "Eventos",
                            visible: true,
                            visibilityMode: "exclusive",
                            layers: [],
                            opacity: 0.75
                        });
                        map.add(groupLayer);
                        layerList = new LayerList({
                            view: view,
                            container: document.createElement("div")
                        });
                        var _bgExpand = new Expand({
                            view: view,
                            content: layerList.container,
                            expandIconClass: "esri-icon-layer-list",
                            expandTooltip: "Abrir Camadas",
                            collapseTooltip: "Fechar Camadas",
                            expanded: false,
                        });
                        view.ui.add(_bgExpand, "top-left");
                        loadThundersLayers();
                        loadFireOutbreakLayers();
                        loadThunderForecast();
                        loadWindsForecast();
                        var layerARCDESSK = new FeatureLayer({
                            // URL to the service
                            url: "http://urbeluz.conasa.com:6080/arcgis/rest/services/TESTE_GISSERVER/MapServer"
                          });
                        //loadForeCastLayers(self);
                        map.add(layerARCDESSK);
                    });

                    var basemapGallery = new BasemapGallery({
                        view: view,
                        container: document.createElement("div")
                    });
                    var bgExpand = new Expand({
                        view: view,
                        content: basemapGallery.container,
                        expandIconClass: "esri-icon-basemap",
                        expandTooltip: "Abrir Mapas",
                        collapseTooltip: "Fechar Mapas"
                    });
                    var locateBtn = new Locate({
                        view: view
                    });
                    var full = new Fullscreen({
                        view: view
                    });
                    // var legend = new Legend({
                    //     view: view
                    // });
                    view.ui.add(locateBtn, "top-left");
                    view.ui.add(full, "top-left");
                    view.ui.add(bgExpand, "top-left");
                    // view.ui.add(legend, "bottom-right");
                });
        };

        $scope.initialize = function () {
            try {
                var self = $scope.map;
                domUtilities.showElement(".loader");
                setTimeout(() => {
                    //self.getLastMonitoredData();
                    self.initializeArcGisMap();
                    self.getConcessions();
                    self.getIcons();
                    domUtilities.hideElement(".loader");
                }, 50);
            } catch (error) {
                domUtilities.showMessage("error", "Não conectado");
            }
        };

        this.searchDate = {
            foreCastThunder: {
                startDate: new Date(date),
                endDate: new Date(endDate),
                title: 'Previsão de descargas atmosféricas',
                colorOptions: ['#fff', '#ff0'],
                layer: null,
                callback: loadThunderForecast
            },
            thunder: {
                startDate: new Date(date),
                endDate: new Date(endDate),
                title: 'Monitoramento de descargas atmosféricas',
                colorOptions: ['#d9e6df', '#3cf500'],
                callback: loadThundersLayers
            },
            fireoutbreak: {
                startDate: new Date(date),
                endDate: new Date(endDate),
                title: 'Monitoramento de queimadas',
                colorOptions: ['#ffff00', '#ff0000'],
                layer: null,
                callback: loadFireOutbreakLayers
            },
            winds: {
                startDate: new Date(date),
                endDate: new Date(endDate),
                title: 'Previsão de ventos severos',
                colorOptions: ['#adf', '#173bef'],
                layer: null,
                callback: loadWindsForecast
            }
        };
        this.incrementTime = function (type) {
            var range = 60 * 30 * 1000;
            if (type == 'minus') {
                if (TIME_LAPSE - range > 0) {
                    TIME_LAPSE -= range;
                }
            } else {
                TIME_LAPSE += range;
            }
        }
    }
})();

function changeSearchDate(e) {
    var searchDate = angular.element(e).scope().map.searchDate;
    if (e.id == 'startDate') {
        searchDate[e.getAttribute('customId')].startDate = new Date(e.value)
    } else {
        searchDate[e.getAttribute('customId')].endDate = new Date(e.value);
    }
    console.log(searchDate);
    if (!angular.element(e).scope().$$phase) angular.element(e).scope().$apply();

}
var TIME_LAPSE = 60 * 60 * 1000;