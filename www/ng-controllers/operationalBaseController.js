(function () {
    "use strict";

    //Register controller
    window.app.controller("operationalBaseController", operationalBaseController);

    function operationalBaseController($scope) {

        this.getBases = function () {
            var self = this;
            connectionUtilities.sendAjaxRequest('/api/getTeams', "GET", undefined,
                function (data) {
                    if (!data)
                        return;

                    self.teams = data;
                }, undefined, true);

            if (!$scope.$$phase)
                $scope.$apply();

        };

        this.displayActionBar = function () {
            //$("#autocomplete").val("");            
        };

        this.saveTeam = function (team) {
            this.isEditing = false;
            var editing;
            this.teams.forEach(t => {
                if (t.mRID == team.mRID) {
                    t.Address = team.addrees;
                    t.Name = team.name;
                    t.Teams = team.teams;
                    editing = true;
                }
            });

            if (editing) return;

            this.teams.push({
                "mRID": new Date().getTime(),
                "Latitude": null,
                "Longitude": null,
                "Address": team.addrees,
                "Name": team.name,
                "Teams": team.teams
            })
        };

        this.addTeam = function(e, index, value){
            this.object.teams[index] = value;
        }

        this.editTeam = function (team){
            this.isActive = true;
            this.isEditing = true;
            this.object = {};
            this.object.mRID = team.mRID;
            this.object.name = team.Name;
            this.object.addrees = team.Address;
            this.object.teams = team.Teams;
        };

        function addScript(fileName, type, callback) {
            try {
                var fileref, filesAdded = 0;
                switch (type) {
                    case "js":
                        fileref = document.createElement("script");
                        fileref.setAttribute("type", "text/javascript");
                        fileref.setAttribute("src", fileName);
                        break;
                    case "css":
                        fileref = document.createElement("link");
                        fileref.setAttribute("rel", "stylesheet");
                        fileref.setAttribute("type", "text/css");
                        fileref.setAttribute("href", fileName);
                        break;
                }
                if (fileName.indexOf("http") > -1) {
                    fileref.async = true;
                    fileref.defer = true;
                } else {
                    fileref.async = false;
                }


                fileref.onreadystatechange = fileref.onload = function (e) {
                    filesAdded--;
                    if (filesAdded === 0) {
                        callback();
                    }
                };
                document.getElementsByTagName(type === "js" ? "body" : "head")[0].appendChild(fileref);

            } catch (error) {
                console.error(error);
                throw error;
            }
        };

        function initilizeAutoComplete() {
            var src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBG89tJjt8yG0nTxrPNHy5gkIyncwyq3j8&libraries=places";
            addScript(src, "js", function () { });
        };

        $scope.initialize = function () {
            //initilizeAutoComplete();
            $scope.base.getBases();
            setTimeout(function () {
                self.autoCompleteAddress = new google.maps.places.Autocomplete(
                    /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                    { types: ['geocode'] });
            }, 1000);
        };
    }
})();