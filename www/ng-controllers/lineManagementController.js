(function () {
    "use strict";

    //Register controller
    window.app.controller("lineManagementController", lineManagementController);

    function lineManagementController($scope) {
        this.name = "lineManagement";

        this.getConfigGroupACLineSegmentsAndTypes = function () {
            var _data;
            connectionUtilities.sendAjaxRequest('/api/getCableConfig', "GET", undefined,
                function (data) {
                    if (!data)
                        return;

                    _data = data;
                }, undefined, true);

            if (_data) {
                this.configGroupACLineSegmentsAndTypes = _data;
                this.selectedConfigGroup = this.configGroupACLineSegmentsAndTypes[0];
            }
            if (!$scope.$$phase)
                $scope.$apply();

        };
        this.getConfigGroupACLineSegmentsAndTypes();
    }
})();