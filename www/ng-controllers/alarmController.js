(function () {
    "use strict";

    //Register controller
    window.app.controller("alarmController", alarmController);

    function alarmController($scope) {
        this.name = "alarm " + Date.now();
        var dataSet = [];
        var audio;
        var table = $('#example').DataTable({
            data: dataSet,
            language: {
                "url": "scripts/vendor/datatable/pt-br.json"
            },
            buttons: [
                'copy', 'excel', 'pdf'
            ],
            columns: [
                { title: "Data Atualização" },
                { title: "Previsão" },
                { title: "Concessão" },
                { title: "Descrição" },
                { title: "Prioridade" },
                { title: "Criticidade" },
                { title: "Prioriade da Linha" },
                { title: "Probabilidade" },
            ]
        });
        //table.buttons().enable();

        function playAlarm(alarmPatch) {
            var self = $scope.alarm;
            if (self.volumeOff) return;

            if (audio && !audio.isEnding) return;

            audio = new Audio(alarmPatch ? alarmPatch : '/scripts/dist/sounds/alert.wav');
            audio.addEventListener('ended', function () {
                audio.isEnding = true;
                if (location.hash.indexOf('alarm') >= 0) {
                    audio.play();
                }
            }, false);
            audio.play();
        };

        this.stopSounds = function () {
            this.volumeOff = !this.volumeOff;
            if (this.volumeOff)
                audio.pause();
            else {
                //playAlarm();
            }
        };

        this.getAlarmParameters = function () {
            var self = this;
            connectionUtilities.sendAjaxRequest('/api/getAlarmParameters', "GET", undefined,
                (data) => {
                    self.alarmParameters = data;

                    if (!$scope.$$phase) $scope.$apply();

                }, (err) => {
                    domUtilities.showMessage("error", "", "Erro ao tentar recupearar as configurações");
                }, true);
        };

        this.loadAlarms = function () {
            var alarmParameters = $scope.alarm.alarmParameters;

            $scope.main.alarmsList.forEach(alarm => {
                var data = alarm.Data;
                var table = $('#example').DataTable();
                var lastUpdate = `${new Date(data.point.lastUpdate).toLocaleDateString()} - ${new Date(data.point.lastUpdate).toLocaleTimeString()}`;
                var forecast = "";
                if (data.point.date) {
                    forecast = `${new Date(data.point.date).toLocaleDateString()} - ${new Date(data.point.date).toLocaleTimeString()}`;
                }
                var probability = "";
                if (data.point.probability >= 0) {
                    probability = (data.point.probability * 100).toFixed('2') + "%"
                }
                var rowNode = table
                    .row.add([lastUpdate, forecast, data.point.concession, data.type, data.point.priority, data.point.criticality.toFixed(2), data.point.linePriority.toFixed(2), probability])
                    .draw()
                    .node();                
                var alarmParam = alarmParameters.find(p => p.name == data.point.priority);
                $(rowNode).css('color', alarmParam.color);
                playAlarm(alarmParam.alarmPath);
            });
        };


        $scope.$watch('main.alarms', function (current, original) {

        });

        $scope.initialize = function () {
            domUtilities.showElement(".loader");
            var self = $scope.alarm;
            setTimeout(() => {
                self.getAlarmParameters();
                self.loadAlarms();
                domUtilities.hideElement(".loader");
            }, 0);

        };
    }
})();