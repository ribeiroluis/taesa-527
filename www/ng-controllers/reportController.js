///<reference path="../scripts/services/connections.js"/>

(function () {
    "use strict";

    //Register controller
    window.app.controller("reportController", reportController);

    function reportController($scope, $location) {
        var chart, table, damageTable;
        this.chartType = [
            { name: "Coluna", type: "column" },
            { name: "Linha", type: "line" },
            { name: "Area", type: "area" }
        ];
        this.reportType = "chart";
        this.selectedChart = this.chartType[0];
        var d = new Date();
        this.startDate = new Date(2017, 0, 1);
        this.endDate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
        this.intervals = [
            { name: "Mensal", value: 30 * 24 * 60 * 60 * 1000 },
            { name: "Trimestral", value: 3 * 30 * 24 * 60 * 60 * 1000 },
            { name: "Semestral", value: 6 * 30 * 24 * 60 * 60 * 1000 },
            { name: "Anual", value: 12 * 30 * 24 * 60 * 60 * 1000 }
        ];
        this.interval = this.intervals[0];
        this.selectedDamageRadius = 100;
        this.selectedDamageHours = 1;

        this.generateReport = function () {
            this.reports = [];
            this.reports.push(
                {
                    name: "Descargas atmosféricas por linhas",
                    value: "report1",
                    selectLine: true,
                    showDefaultFilter: true,
                },
                {
                    name: "Queimadas por linhas",
                    value: "report2",
                    selectLine: true,
                    showDefaultFilter: true,
                },
                {
                    name: "Previsão de ventos severos por linhas",
                    value: "report3",
                    selectLine: true,
                    showDefaultFilter: true,
                },
                {
                    name: "Danos à rede por linha",
                    value: "report4",
                    selectLine: true,
                    showDefaultFilter: true,
                },
                {
                    name: "Alarmes gerados por linha - Ventos Severos",
                    value: "report5",
                    selectLine: true,
                    showDefaultFilter: true,
                },
                {
                    name: "Alarmes gerados por linha - Descargas atmosféricas",
                    value: "report6",
                    selectLine: true,
                    showDefaultFilter: true,
                },
                {
                    name: "Danos x Eventos Registrados",
                    value: "report7",
                    showDefaultFilter: false,
                    report7Filter: true,
                },
                {
                    name: "Descargas atmosféricas Monitoradas x Previstas",
                    value: "report8",
                    selectLine: true,
                    showDefaultFilter: false,
                    report8Filter: true,
                    lines: undefined,
                    startDate: parseDatetoInput(new Date(new Date().setDate(new Date().getDate() - 1))),
                    endDate: parseDatetoInput(new Date())
                }
            );
        };

        this.changeDate = function (e, value) {
            var v = e.currentTarget.value;
            var d = new Date();
            if (v !== "") {
                d = new Date(v);
            }
            this.selectedReport[value] = parseDatetoInput(d);
            e.currentTarget.value = this.selectedReport[value];
            this.loadThudersForecastRport();
        };

        this.toggleAcordion = function (id) {
            $(`#${id}`).collapse('toggle');
        }

        this.selectDamage = function (damage) {
            this.damages.forEach(d => {
                d.selected = false;
            });
            damage.selected = true;
        };

        this.loadThudersForecastRport = function () {
            var self = this;
            domUtilities.showElement(".loader");
            self.selectedReport.lines = self.lines.filter(l => { return l.selected; }).map(l => { return l.name });
            connectionUtilities.sendAjaxRequest(`/api/getThundersAndForecast/`, "GET", self.selectedReport,
                (data) => {
                    domUtilities.hideElement(".loader");
                    self.correlatedThundersForecast = data;
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                    if (self.reportType == "chart") {

                        if (Object.keys(data.result).length == 0) return;

                        var series = [];
                        Object.keys(data.result).forEach(k => {
                            var monitoredSerie = {
                                name: `${k} - Monitorado`,
                                data: []
                            };
                            var forecastSerie = {
                                name: `${k} - Previsto`,
                                data: []
                            }
                            data.result[k].forEach(r => {
                                monitoredSerie.data.push({
                                    x: r.geometry.coordinates[0],
                                    y: r.geometry.coordinates[1],
                                    type: 'monitored',
                                    concession: r.properties.concession,
                                    date: r.properties.collectionDate,
                                    intensity: r.properties.intensity,
                                    thunderType: r.properties.thunderType,
                                    electricalDischarges: r.properties.electricalDischarges
                                })
                                r.properties.forecastEvents.forEach(fe => {
                                    forecastSerie.data.push({
                                        x: fe.lng,
                                        y: fe.lat,
                                        type: 'forecast',
                                        concession: fe.concession,
                                        date: fe.date,
                                        distance: fe.distanceToRealEvent.toFixed('2') + '(m)',
                                        priority: fe.priority,
                                        electricalDischarges: fe.projectedElectricalDischarges,
                                        probability: (fe.probability * 100).toFixed(2) + '%'
                                    });
                                });
                            });
                            series.push(monitoredSerie, forecastSerie);
                        });
                        chart = Highcharts.chart('forecastThunderChart', {
                            chart: {
                                type: 'scatter',
                                zoomType: 'xy',
                                backgroundColor: '#000',
                                borderColor: '#000',
                                borderWidth: 2
                            },
                            tooltip: {
                                formatter: function () {
                                    var html;
                                    if (this.point.type == "forecast") {
                                        html =
                                            `<b>Previsto</b><br>
                                            <b>Linha:</b><label>${this.point.concession}</label><br>
                                            <b>Data prevista:</b><label>${new Date(this.point.date).toLocaleString()}.${new Date(this.point.date).getMilliseconds()}</label><br>
                                            <b>Probabilidade:</b><label>${this.point.probability}</label><br>
                                            <b>Distânica do evento:</b><label>${this.point.distance}</label><br>
                                            <b>Prioridade:</b><label>${this.point.priority}</label><br>
                                            <b>Quantidade de descargas previstas:</b><label>${this.point.electricalDischarges}</label><br>
                                            <b>Lat:</b><label>${this.point.y.toFixed(3).replace('.', ',')}</label>
                                        <b>Lng:</b><label>${this.point.x.toFixed(3).replace('.', ',')}</label><br>`;
                                    } else {
                                        html = `<b>Monitorado</b><br>
                                        <b>Linha:</b><label>${this.point.concession}</label><br>
                                        <b>Data:</b><label>${new Date(this.point.date).toLocaleString()}.${new Date(this.point.date).getMilliseconds()}</label><br>
                                        <b>Intensidade:</b><label>${this.point.intensity}</label><br>
                                        <b>Tipo de descarga:</b><label>${this.point.thunderType}</label><br>
                                        <b>Quantidade de descargas:</b><label>${this.point.electricalDischarges}</label><br>
                                        <b>Lat:</b><label>${this.point.y.toFixed(3).replace('.', ',')}</label>
                                        <b>Lng:</b><label>${this.point.x.toFixed(3).replace('.', ',')}</label><br>`;
                                    }
                                    return html;
                                }
                            },
                            title: {
                                text: self.selectedReport.name
                            },
                            xAxis: {
                                title: {
                                    enabled: true,
                                    text: '<b>Lng</b>'
                                }
                            },
                            yAxis: {
                                title: {
                                    enabled: true,
                                    text: '<b>Lat</b>'
                                }
                            },
                            series: series
                        });
                    }

                }, (err) => {
                    console.error(err);
                    domUtilities.hideElement(".loader");
                });
        };

        this.loadDamageReport = function () {

            if (damageTable) {
                damageTable.destroy();
                $('#damageTable').empty();
                $('#damageTable').remove();
                damageTable = undefined;
            }

            if (!this.selectDamageType) return;

            var self = this;
            var d = new Date(self.selectDamageType.eventDate);
            self.reportDamageTableTitle = `${d.toLocaleString()} - ${self.selectDamageType.type} (Linha: ${self.selectDamageType.concession} - Torre: ${self.selectDamageType.tower})`;
            domUtilities.showElement(".loader");
            connectionUtilities.sendAjaxRequest(`/api/getEventsFromDamage/${d}/${self.selectedDamageRadius}/${self.selectedDamageHours}`, "GET", self.selectDamageType,
                function (data) {
                    if (!data)
                        return;


                    var _table = document.createElement('table');
                    _table.id = "damageTable";
                    _table.classList.add('table', 'table-striped', 'table-bordered', 'responsive', 'display', 'nowrap');
                    _table.style.width = "100%";
                    _table.style.marginTop = "30px";
                    var thead = document.createElement('thead');
                    var thr = document.createElement('tr');
                    thead.appendChild(thr);
                    ['Queimadas', 'Descargas', 'Ventos'].forEach(i => {
                        var th = document.createElement('th');
                        th.innerHTML = i;
                        thr.appendChild(th);
                    });
                    var tbody = document.createElement('tbody');
                    var i = 0;
                    while (data.fireoutbreak.length > 0 || data.thunder.length > 0 || data.wind.length > 0) {
                        var tbr = document.createElement('tr');
                        var fire = data.fireoutbreak.shift();
                        var thunder = data.thunder.shift();
                        var winds = data.wind.shift();

                        if (fire) {
                            var _td = document.createElement('td');
                            _td.appendChild(formatCell(fire.properties));
                            tbr.appendChild(_td);
                        } else {
                            var td = document.createElement('td');
                            td.innerHTML = "-";
                            tbr.appendChild(td);
                        }
                        if (thunder) {
                            var _td = document.createElement('td');
                            _td.appendChild(formatCell(thunder.properties));
                            tbr.appendChild(_td);
                        } else {
                            var td = document.createElement('td');
                            td.innerHTML = "-";
                            tbr.appendChild(td);
                        }
                        if (winds) {
                            var _td = document.createElement('td');
                            _td.appendChild(formatCell(winds.properties));
                            tbr.appendChild(_td);
                        } else {
                            var td = document.createElement('td');
                            td.innerHTML = "-";
                            tbr.appendChild(td);
                        }
                        tbody.appendChild(tbr);
                    }
                    _table.appendChild(thead);
                    _table.appendChild(tbody);
                    $('#reportDamageTable').append(_table);
                    damageTable = $(_table).DataTable({
                        searching: false,
                        scrollX: true,
                        scrollY: true,
                        scrollCollapse: true,
                        paging: false,
                        "columns": [
                            { "width": "33%" },
                            { "width": "33%" },
                            { "width": "3%" },
                        ],
                        language: {
                            "url": "scripts/vendor/datatable/pt-br.json"
                        }
                    });
                    new $.fn.dataTable.Buttons(damageTable, {
                        buttons: [
                            {
                                extend: 'print',
                                text: 'Imprimir',
                                messageTop: self.reportDamageTableTitle
                            },
                            {
                                extend: 'csv',
                                text: 'CSV',
                                messageTop: self.reportDamageTableTitle
                            }, {
                                extend: 'excel',
                                text: 'Excel',
                                messageTop: self.reportDamageTableTitle
                            }, {
                                extend: 'pdfHtml5',
                                orientation: 'landscape',
                                pageSize: 'LEGAL',
                                messageTop: self.reportDamageTableTitle
                            }
                        ]
                    });
                    damageTable.buttons().container()
                        .appendTo($('.button-group', damageTable.table().container()));
                    domUtilities.hideElement(".loader");

                    //https://www.highcharts.com/demo/scatter
                    function formatCell(p) {
                        var div = document.createElement('div');
                        div.classList.add('table-cell');
                        if (p.intensity) {
                            var _p = document.createElement('p');
                            _p.innerHTML = "Intensidade: " + p.intensity;
                            div.appendChild(_p);
                        }
                        if (p.thunderType) {
                            var _p = document.createElement('p');
                            _p.innerHTML = "Tipo de descarga: " + p.thunderType
                            div.appendChild(_p);
                        }
                        if (p.electricalDischarges) {
                            var _p = document.createElement('p');
                            _p.innerHTML = "Quantidade de descargas: " + p.electricalDischarges;
                            div.appendChild(_p);
                        }
                        if (p.collectionDate) {
                            var _p = document.createElement('p');
                            _p.innerHTML = "Data da coleta: " + new Date(p.collectionDate).toLocaleString();
                            div.appendChild(_p);
                        }
                        if (p.distaceToTower) {
                            var _p = document.createElement('p');
                            _p.innerHTML = "Distância da torre: " + p.distaceToTower.toFixed(2).replace('.', ',') + "m";
                            div.appendChild(_p);
                        }
                        if (p.satellite) {
                            var _p = document.createElement('p');
                            _p.innerHTML = "Satélite: " + p.satellite;
                            div.appendChild(_p);
                        }
                        if (p.date) {
                            var _p = document.createElement('p');
                            _p.innerHTML = "Previsão: " + new Date(p.date).toLocaleString();
                            div.appendChild(_p);
                        }
                        if (p.priority) {
                            var _p = document.createElement('p');
                            _p.innerHTML = "Prioridade: " + p.priority;
                            div.appendChild(_p);
                        }
                        if (p.probability) {
                            var _p = document.createElement('p');
                            _p.innerHTML = "Probabilidade: " + p.probability * 100 + "%";
                            div.appendChild(_p);
                        }
                        if (p.windDirection) {
                            var _p = document.createElement('p');
                            _p.innerHTML = "Direção do vento: " + p.windDirection;
                            div.appendChild(_p);
                        }
                        if (p.windVelocity) {
                            var _p = document.createElement('p');
                            _p.innerHTML = "Velocidade do vento: " + p.windVelocity + " km/h";
                            div.appendChild(_p);
                        }
                        return div;
                    }
                }, function (err) {
                    console.error(err);
                    domUtilities.hideElement(".loader");
                });
        };

        this.loadReport = function () {
            var categories;
            var series = [];

            if (this.selectedReport == null) return;

            if (this.selectedReport.value == "report7") {
                this.loadDamageReport();
                return;
            }

            var chartType = this.selectedChart.type;
            categories = generateDataWithTime(100).categories;
            switch (this.selectedReport.value) {
                case 'report1':
                case 'report2':
                case 'report3':
                    this.lines.forEach(line => {
                        if (!line.selected) return;

                        series.push({
                            type: chartType,
                            name: line.name,
                            data: generateDataWithTime(100).data
                        })
                    });
                    this.selectedReport.category = "Linhas";
                    break;
                case 'report4':
                    this.lines.forEach(line => {
                        if (!line.selected) return;

                        series.push({
                            type: chartType,
                            name: line.name + " - Queda de estrutura",
                            data: generateDataWithTime(10).data
                        }, {
                                type: chartType,
                                name: line.name + " - Rompimento de feixe de condutores / subcondutor",
                                data: generateDataWithTime(10).data
                            }, {
                                type: chartType,
                                name: line.name + " - Queda de cabo por rompimento de cadeia de isoladores",
                                data: generateDataWithTime(10).data
                            }, {
                                type: chartType,
                                name: line.name + " - Vandalismo em cadeia de isoladores",
                                data: generateDataWithTime(10).data
                            });
                    });
                    this.selectedReport.category = "Linha/Dano";
                    break;
                case 'report5':
                case 'report6':
                    this.lines.forEach(line => {
                        if (!line.selected) return;

                        series.push({
                            type: chartType,
                            name: line.name + " - Alto",
                            data: generateDataWithTime(10).data
                        }, {
                                type: chartType,
                                name: line.name + " - Médio",
                                data: generateDataWithTime(10).data
                            }, {
                                type: chartType,
                                name: line.name + " - Baixo",
                                data: generateDataWithTime(10).data
                            })
                    });
                    this.selectedReport.category = "Linha/Nível de Alarme";
                    break;
                case 'report8':
                    this.loadThudersForecastRport();
                    return;
            }
            this.selectedReport.series = series;
            this.selectedReport.categories = categories;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
            this.changeView();
        };

        this.changeView = function () {
            domUtilities.showElement(".loader");
            var self = this;
            if (this.reportType == "table") {
                setTimeout(() => {
                    var columnDefs = [{ title: self.selectedReport.category }];
                    var data = [];
                    self.selectedReport.categories.forEach(c => {
                        columnDefs.push({
                            title: c
                        })
                    });
                    self.selectedReport.series.forEach(s => {
                        var _d = [s.name];
                        s.data.forEach(d => {
                            _d.push(d)
                        });
                        data.push(_d);
                    });
                    if (table) {
                        table.destroy();
                        $('#reportTable').empty();
                    }
                    table = $('#reportTable').DataTable({
                        searching: false,
                        scrollY: "300px",
                        scrollX: true,
                        scrollCollapse: true,
                        paging: false,
                        fixedColumns: true,
                        // buttons: [
                        //     'csv', 'excel', 'pdf', 'print'
                        // ],
                        data: data,
                        language: {
                            "url": "scripts/vendor/datatable/pt-br.json"
                        },
                        columns: columnDefs
                    });
                    new $.fn.dataTable.Buttons(table, {
                        buttons: [
                            {
                                extend: 'print',
                                text: 'Imprimir',
                                messageTop: self.selectedReport.name
                            },
                            {
                                extend: 'csv',
                                text: 'CSV',
                                messageTop: self.selectedReport.name
                            }, {
                                extend: 'excel',
                                text: 'Excel',
                                messageTop: self.selectedReport.name
                            }, {
                                extend: 'pdfHtml5',
                                orientation: 'landscape',
                                pageSize: 'LEGAL',
                                messageTop: self.selectedReport.name
                            }
                        ]
                    });
                    table.buttons().container()
                        .appendTo($('.button-group', table.table().container()));

                    domUtilities.hideElement(".loader");
                }, 100);
                chart = undefined;
            } else {
                setTimeout(() => {
                    chart = Highcharts.chart('container', {
                        chart: {
                            zoomType: 'x'
                        },
                        title: {
                            text: self.selectedReport.name
                        },
                        xAxis: {
                            categories: self.selectedReport.categories
                        },
                        yAxis: {
                            title: {
                                text: 'Total'
                            }
                        },
                        legend: {
                            enabled: true
                        },
                        plotOptions: {
                            area: {
                                fillColor: {
                                    linearGradient: {
                                        x1: 0,
                                        y1: 0,
                                        x2: 0,
                                        y2: 1
                                    }

                                },
                                marker: {
                                    radius: 2
                                },
                                lineWidth: 1,
                                states: {
                                    hover: {
                                        lineWidth: 1
                                    }
                                },
                                threshold: null
                            }
                        },
                        tooltip: {
                            shared: true,
                            // formatter: function () {
                            //     return '<b>' + this.series.name + '</b>: ' + this.y + '<br/>';
                            // }
                        },
                        series: self.selectedReport.series
                    });
                    domUtilities.hideElement(".loader");
                }, 0);
            }
        };

        this.changeChartType = function () {
            chart.series.forEach(s => {
                s.update({
                    type: this.selectedChart.type
                })
            });
        };

        function generateData(qtd, maxValue, fixed) {
            var data = [];
            for (var i = 0; i < qtd; i++) {
                var value = ((Math.random() * maxValue) + 1).toFixed(fixed);
                if (fixed == 0) {
                    value = parseInt(value);
                } else {
                    value = parseFloat(value);
                }
                data.push(value);
            }
            return data;
        };

        function generateDataWithTime(maxValue, fixed) {
            var start = $scope.report.startDate;
            var end = $scope.report.endDate;
            var interval = $scope.report.interval.name
            //var interval = $scope.report.interval.value;
            var data = [];
            var categories = [];
            var month = {
                0: {
                    trimestre: 1,
                    semestre: 1
                },
                1: {
                    trimestre: 1,
                    semestre: 1
                },
                2: {
                    trimestre: 1,
                    semestre: 1
                },
                3: {
                    trimestre: 2,
                    semestre: 1
                },
                4: {
                    trimestre: 2,
                    semestre: 1
                },
                5: {
                    trimestre: 2,
                    semestre: 1
                },
                6: {
                    trimestre: 3,
                    semestre: 2
                },
                7: {
                    trimestre: 3,
                    semestre: 2
                },
                8: {
                    trimestre: 3,
                    semestre: 2
                },
                9: {
                    trimestre: 4,
                    semestre: 2
                },
                10: {
                    trimestre: 4,
                    semestre: 2
                },
                11: {
                    trimestre: 4,
                    semestre: 2
                }
            };
            var date = new Date(start);

            while (date < end) {
                switch (interval) {
                    case 'Mensal':
                        var months = Highcharts.getOptions().lang.shortMonths;
                        label = `${months[date.getMonth()]}'${date.getFullYear()}`;
                        maxValue = 50;
                        date.setMonth(date.getMonth() + 1);
                        break;
                    case 'Trimestral':
                        var period = month[date.getMonth()].trimestre;
                        label = `${period}º'${date.getFullYear()}`;
                        maxValue = 500;
                        date.setMonth(date.getMonth() + 3);
                        break;
                    case 'Semestral':
                        var period = month[date.getMonth()].semestre;
                        label = `${period}º'${date.getFullYear()}`;
                        maxValue = 1000;
                        date.setMonth(date.getMonth() + 6);
                        break;
                    case 'Anual':
                        label = `${date.getFullYear()}`;
                        maxValue = 1500;
                        date.setFullYear(date.getFullYear() + 1);
                        break;
                    default: return;
                }
                categories.push(label);
                data.push(Math.round((Math.random() * maxValue)));
            }
            if (interval == 'Anual' && date.getFullYear() == end.getFullYear()) {
                label = `${date.getFullYear()}`;
                maxValue = 1500;
                categories.push(label);
                data.push(Math.round((Math.random() * maxValue)));
            }
            return { data: data, categories: categories };


            /////

            var startPeriod, startYear, date;
            for (var i = start; i < end; i += interval) {
                var label;

                switch ($scope.report.interval.name) {
                    case 'Mensal':
                        if (!date) {
                            date = new Date(i);
                        } else {
                            date.setMonth(date.getMonth() + 1);
                        }
                        var months = Highcharts.getOptions().lang.shortMonths;
                        label = `${months[date.getMonth()]}'${date.getFullYear()}`;
                        maxValue = 50;
                        break;
                    case 'Trimestral':
                        if (!date) {
                            date = new Date(i);
                        } else {
                            date.setMonth(date.getMonth() + 3);
                            if (date > $scope.report.endDate) continue;
                        }
                        if (!startPeriod) {
                            startPeriod = periodo[date.getMonth()].trimestre;
                            startYear = date.getFullYear();
                        } else {
                            if (startPeriod <= 3) {
                                startPeriod += 1;
                            } else {
                                startPeriod = 1;
                                startYear += 1;
                            }
                        }
                        label = `${startPeriod}º'${startYear}`;
                        maxValue = 1000;
                        break;
                    case 'Semestral':
                        if (!date) {
                            date = new Date(i);
                        } else {
                            date.setMonth(date.getMonth() + 6);
                            if (date > $scope.report.endDate) continue;
                        }
                        if (!startPeriod) {
                            startPeriod = periodo[date.getMonth()].semestre;
                            startYear = date.getFullYear();
                        } else {
                            if (startPeriod <= 1) {
                                startPeriod += 1;
                            } else {
                                startPeriod = 1;
                                startYear += 1;
                            }
                        }
                        label = `${startPeriod}º'${startYear}`;
                        maxValue = 1000;
                        break;
                    case 'Anual':
                        label = `${new Date(i).getFullYear()}`;
                        maxValue = 1000;
                        break;
                    default: label = i; break;
                }
                categories.push(label);
                data.push(Math.round((Math.random() * maxValue)));
            }
            return { data: data, categories: categories };
        };

        function parseDatetoInput(date) {
            var time = date.toLocaleTimeString().split(':');
            return `${date.toISOString().split('T')[0]}T${time[0]}:${time[1]}`;
        };

        this.getConcessions = function () {
            var self = this;
            connectionUtilities.sendAjaxRequest('./api/getConcessionsList', "GET", undefined,
                function (_data) {
                    self.lines = _data.map((d, i, arr) => {
                        return { name: d, selected: i < 4 };
                    });
                    if (!$scope.$$phase) $scope.$apply();
                }, function () {
                    domUtilities.showMessage("Error", undefined, "Erro ao tentar buscar concessões");
                }, true);
        };

        this.getDamages = function () {
            var self = this;
            connectionUtilities.sendAjaxRequest('/api/getDamages', "GET", undefined,
                function (data) {
                    if (!data)
                        return;
                    data.forEach(d => {
                        d.title = `${new Date(d.eventDate).toLocaleString()} - ${d.concession} - ${d.type}`;
                    });
                    self.damages = data;
                }, function (err) {
                    console.error(err);
                }, true);
        };

        $scope.initialize = function () {
            var self = $scope.report;
            self.generateReport();
            self.getConcessions();
            self.getDamages();
            domUtilities.hideElement(".loader");
        };

        this.lineListClick = function (event, line) {
            if (line == 'selectAll' || line == 'unselectAll') {
                this.lines.forEach(l => {
                    l.selected = line == 'selectAll';
                });

            } else {
                line.selected = !line.selected;
            }
        };

        this.checkLinesSelecteds = function () {
            return this.lines.find(l => { return l.selected; });
        };

        $scope.$on('elementFinishRender', function (scope, attr) {
            var date = attr.id == "start" ? $scope.report.startDate : $scope.report.endDate;

            $(attr.$$element).val(`${date.toLocaleDateString()}`);
            $(attr.$$element).datepicker({
                // startView: 1,
                // minViewMode: 1,
                todayBtn: "linked",
                language: "pt-BR",
                forceParse: false,
                autoclose: true,
            }).on('changeDate', e => {
                console.log(e);
                if (e.target.id == "start") {
                    $scope.report.startDate = e.date;
                    $('#end').datepicker('setStartDate', e.date.toLocaleDateString());
                    if ($scope.report.startDate > $scope.report.startDate)
                        $('#end').datepicker('setDate', e.date.toLocaleDateString());
                } else {
                    $scope.report.endDate = e.date;
                    $('#start').datepicker('setEndDate', e.date.toLocaleDateString());
                    if ($scope.report.endDate < $scope.report.startDate)
                        $('#start').datepicker('setDate', e.date.toLocaleDateString());
                }
                $scope.report.loadReport();

            });
        });

        $scope.$on('dropDownFinishRender', (scope, attr) => {
            $(attr.$$element).on({
                "click": function (e) {
                    if (e.target.tagName == "A")
                        e.stopPropagation();
                }
            }).on('hide.bs.dropdown', function () {
                $scope.report.loadReport();
            });;
        });

        $scope.$on('chartFinishRender', (scope, attr) => {
            $scope.report.changeView();
        });

        $scope.$on('dropDownFinishRenderDamage', (scope, attr) => {
            $(attr.$$element).on('hide.bs.dropdown', function () {
                $scope.report.changeView();
            });
        });

        $scope.$on('chartFinishRender', (scope, attr) => {
            $scope.report.changeView();
        });
    }
})();
