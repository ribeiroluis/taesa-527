var fs = require('fs');


fs.readdir('../api/data/segmentos', { encoding: "utf-8" }, (err, r) => {
    if (err) {
        console.log(err)
        return;
    }
    r.forEach(c => {
        var line = require('../api/data/segmentos/' + c);

        line.features.forEach(f => {
            f.properties.priority = Math.random();
        });        
        console.log(line.features[0].properties.priority);
        fs.writeFile('./result/' + c, JSON.stringify(line), (err)=>{
            if (err) {
                console.error(err)
                return;
            }
            console.log('file saved!')
                
        });

    });
});