var geojson = require('./taesaDistConvertido.json');
var fs = require('fs');
var segmentos = {};
var torres;

geojson.features.forEach(feature => {
    if (feature.geometry.type.toUpperCase() == "POINT") return;

    var con = feature.properties.Name;
    feature.properties.equipmentType = "ACLineSegment";

    if (!segmentos[con]) {
        segmentos[con] = {
            "type": "FeatureCollection",
            "features": [feature]
        };
    } else {
        segmentos[con].features.push(feature);
    }
});

for (var con in segmentos) {
    var path = "../api/dados/segmentos/" + con + ".json";
    fs.writeFile(path, JSON.stringify(segmentos[con]), function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    });
}

// var fs = require('fs');

// fs.readdir('../api/dados/torres/', (err, files) => {
//     if (err) {
//         console.log(err);
//         return;
//     }
//     var tower;
//     files.forEach(file => {
//         var path = '../api/dados/torres/' + file;
//         tower = JSON.parse(fs.readFileSync(path, 'utf8'));
//         tower.features.forEach(feature => {
//             feature.properties.equipmentType = "Tower";
//         });
//         fs.writeFile(path, JSON.stringify(tower), function (err) {
//             if (err) {
//                 return console.log(err);
//             }
//             console.log("The file was saved!");
//         });
//     });    
// });



