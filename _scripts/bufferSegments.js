require('dotenv').config()
var st = require('geojson-bounds');
var fs = require('fs');
var turf_offset = require('@turf/line-offset');
var turf = require("@turf/helpers");
var appkeys = require('../package.json').appkeys;
var path = 'api/data/segmentos';
var rangeBuffer = eval(process.env["RANGEBUFFER"]);

fs.readdir(path, (err, files) => {
    if (err) {
        console.error(err);
        return;
    }
    var unit = 'meters';
    var result = {
        "type": "FeatureCollection",
        "features": []
    }
    files.forEach(file => {
        var geojson = JSON.parse(fs.readFileSync(path + '/' + file, 'utf8'));
        var concession = geojson.features[0].properties.Name;
        geojson.features.forEach(feature => {
            for (var i = 0; i < feature.geometry.coordinates.length - 1; i++) {
                var initial = feature.geometry.coordinates[i];
                var final = feature.geometry.coordinates[i + 1];
                var _feature = {
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "type": "LineString",
                        "coordinates": [initial, final]
                    }
                }
                var lineOffSetA = turf_offset(_feature, rangeBuffer, { units: unit });
                var lineOffSetB = turf_offset(_feature, -rangeBuffer, { units: unit });
                var polygon = turf.polygon([[
                    lineOffSetA.geometry.coordinates[0], lineOffSetA.geometry.coordinates[1],
                    lineOffSetA.geometry.coordinates[1], lineOffSetB.geometry.coordinates[1],
                    lineOffSetB.geometry.coordinates[1], lineOffSetB.geometry.coordinates[0],
                    lineOffSetB.geometry.coordinates[0], lineOffSetA.geometry.coordinates[0]
                ]]);
                polygon.properties.concession = concession;

                result.features.push(polygon);
            }
        });
    });
    fs.writeFile('./api/data/bufferAllArea.json', JSON.stringify(result), (err, result) => {
        if (err) {
            console.log(err);
            return;
        }
        console.log("The file was saved!");
    });
});

function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2)
        ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
}

function deg2rad(deg) {
    return deg * (Math.PI / 180)
}