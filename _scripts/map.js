var _map = L.map('map');
var line;
L.tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
    {
        attribution: "OpenStreetMap",
        "minZoom": 0,
        "maxZoom": 20
    }).addTo(_map);


function readSingleFile(e) {

    var file = e.target.files[0];
    if (!file) {
        return;
    }
    var reader = new FileReader();
    reader.onload = function (e) {
        if (line)
            _map.removeLayer(line);

        var geojson = JSON.parse(e.target.result);
        geojson.features.forEach(element => {
            if (element.geometry.type == "Point") return;

            line = L.polyline(element.geometry.coordinates, {
                distanceMarkers: { offset: 532, cssClass: 'towerMarker' },
                isTower: true
            });
            _map.setView(line._latlngs[0], 13)
            _map.addLayer(line);
        });
        downloadFileOnBrowser(file.name, geojson.features[0].properties);
    };
    reader.readAsText(file);
}

function downloadFileOnBrowser(fileName, properties) {

    var content = {
        "type": "FeatureCollection",
        "features": []
    };
    for (var i in _map._layers) {
        var point = _map._layers[i];

        if (!point.options || !point.options.icon || !point.options.icon.options) {
            console.log(point);
        } else {
            if (point.options.icon.options.className == "towerMarker") {
                var id = point._latlng.lat.toString().split('.')[1];
                var feature = {
                    "type": "Feature",
                    "properties": {
                        concession: properties.Name,
                        voltage: properties['Tensão'],
                        heigth: 30,
                        id: id.substr(id.length / 2).toString(),
                    },
                    "geometry": {
                        "type": "Point",
                        "coordinates": [
                            point._latlng.lat,
                            point._latlng.lng
                        ]
                    }
                };
                content.features.push(feature);
            }
        }
    }
    var a = document.createElement('a');
    var file = new Blob([JSON.stringify(content)], { type: 'json' });
    a.href = URL.createObjectURL(file);
    a.download = fileName;
    document.getElementById('download').appendChild(a);
    a.click();
    a.remove();
};
document.getElementById('file').addEventListener('change', readSingleFile, false);