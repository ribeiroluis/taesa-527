var _ = require('underscore');
var express = require('express');
var router = express.Router();
const http = require('http');
var parseString = require('xml2js').parseString;
var condTime = require('./data/condTime.json');
var forecast = require('./services/forecast.js');
var appConfig = require('../appconfig.json');
var turfDistance = require('@turf/distance');
var fs = require("fs");

var criticalParam;

// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("testando-headers", "teste1, teste2, teste3");
    console.log(req.originalUrl + "-> " + new Date().toISOString());
    next();
});

router.get('/condTime', function (req, res) {
    res.sendfile('./data/condTime.json', function (e) {
        if (e) console.error(e);
    });
});

router.get('/actualConditions', function (req, res) {
    var endPoint = 'http://servicos.cptec.inpe.br/XML/capitais/condicoesAtuais.xml';
    HttpGET(endPoint, res);
});

router.get('/getLocaleId/:city', function (req, res) {
    try {
        var endPoint = 'http://servicos.cptec.inpe.br/XML/listaCidades?city=' + req.params.city;
        HttpGET(endPoint, undefined, function (data) {
            if (data.cidades == "") {
                res.status(500).send('Cidade não encontrada na base CPTEC/INPE');
                return;
            }
            res.send({
                id: data.cidades.cidade[0].id[0],
                name: data.cidades.cidade[0].nome[0],
                uf: data.cidades.cidade[0].uf[0]
            })
        });

    } catch (error) {
        resp.status(500).send('Erro ao tentar localizar a cidade');
    }

});

router.get('/forecast/:localeId', function (req, res) {
    //http://servicos.cptec.inpe.br/XML/cidade/codigo_da_localidade/previsao.xml
    var endPoint = 'http://servicos.cptec.inpe.br/XML/cidade/' + req.params.localeId + '/previsao.xml';
    HttpGET(endPoint, undefined, function (data) {
        var result = {
            city: data.cidade.nome[0],
            uf: data.cidade.uf[0],
            time: data.cidade.atualizacao[0],
            forecast: []
        };
        result.forecast = data.cidade.previsao.map(function (e, i, arr) {
            return {
                day: e.dia[0],
                iuv: e.iuv[0],
                max: e.maxima[0],
                min: e.minima[0],
                condition: condTime[e.tempo[0]],
                conditionCode: e.tempo[0]
            };
        });
        res.send(result);
    });
});

router.get('/forecast7/:localeId', function (req, res) {
    //http://servicos.cptec.inpe.br/XML/cidade/7dias/codigo_da_localidade/previsao.xml
    var endPoint = 'http://servicos.cptec.inpe.br/XML/cidade/7dias/' + req.params.localeId + '/previsao.xml';
    HttpGET(endPoint, undefined, function (data) {
        var result = {
            city: data.cidade.nome[0],
            uf: data.cidade.uf[0],
            time: data.cidade.atualizacao[0],
            forecast: []
        }
        result.forecast = data.cidade.previsao.map(function (e, i, arr) {
            return {
                day: e.dia[0],
                iuv: e.iuv[0],
                max: e.maxima[0],
                min: e.minima[0],
                condition: condTime[e.tempo[0]],
                conditionCode: e.tempo[0]
            };
        });
        res.send(result);
    });
});

router.get('/getConcessionsList/', function (req, res) {
    fs.readdir('./api/data/segmentos/', (err, files) => {
        var consessions = [];
        files.forEach(file => {
            consessions.push(file.split('.')[0]);
        });
        res.send(consessions);
    });
});

router.get('/search/:param', function (req, res) {
    var param = req.params.param.toUpperCase();

    try {
        switch (param) {
            case "FORECAST":
                res.send(forecast.getForeCastSimulated());
                break;
            case "FOCOS": getFireOutbreaks(req, res); break;
            //case "RAIOS": getThunders(req, res); break;
            case "PREVISAOINCENDIO": getForecastFire(req, res); break;
            case "PREVISAORAIOS": getForecastThunder(req, res); break;
            case "PREVISAOVENTOS": getForecastWind(req, res); break;
            case "ALARMES": getAllAlarms(req, res); break;
            default: getConcession(param, req, res); break;
        }

    } catch (error) {
        res.status(500).send(error);

    }

});

router.get('/getEndpoints/', function (req, res) {
    res.send(appConfig);
});


router.get('/updateFireOutbreaks', (req, res) => {
    forecast.updateFireOutbreaks();
    res.send(200);
});

router.get('/getCableConfig', (req, res) => {
    try {
        res.sendfile('./api/data/cableConfig.json');
    } catch (error) {
        res.status(500).send(error);
    }
});

router.get('/getIcons', (req, res) => {
    try {
        res.sendfile('./api/data/icons.json');
    } catch (error) {
        res.status(500).send(error);
    }
});

router.get('/getTeams', (req, res) => {
    try {
        res.sendfile('./api/data/teams.json');
    } catch (error) {
        res.status(500).send(error);
    }
});

router.get('/getUsers', (req, res) => {
    try {
        res.sendfile('./api/data/users.json');
    } catch (error) {
        res.status(500).send(error);
    }
});

router.get('/getProfiles', (req, res) => {
    try {
        res.sendfile('./api/data/profiles.json');
    } catch (error) {
        res.status(500).send(error);
    }
});

router.get('/getAccesses', (req, res) => {
    try {
        res.sendfile('./api/data/accesses.json');
    } catch (error) {
        res.status(500).send(error);
    }
});

router.get('/getFeederColors', (req, res) => {
    try {
        res.sendfile('./api/data/feederColors.json');
    } catch (error) {
        res.status(500).send(error);
    }
});

router.get('/getAllocations', (req, res) => {
    try {
        res.sendfile('./api/data/allocations.json');
    } catch (error) {
        res.status(500).send(error);
    }
});

router.post('/saveAllocation', (req, res) => {
    try {

        var allocations = require('./data/allocations.json');
        allocations.push(req.body);
        fs.writeFile('./api/data/allocations.json', JSON.stringify(allocations), err => {
            if (err) {
                res.status(500).send(error);
                return;
            }
            res.send(200);
        });
    } catch (error) {
        res.send(500);
    }
});

router.get('/getDamagesParameters', (req, res) => {
    try {
        res.sendfile('./api/data/damageParameters.json');
    } catch (error) {
        res.status(500).send(error);
    }
});

router.get('/getContingencyLevels', (req, res) => {
    try {
        res.sendfile('./api/data/contingencyLevel.json');
    } catch (error) {
        res.status(500).send(error);
    }
});

router.get('/getTowersIdsFromConcession/:param', function (req, res) {
    try {
        var param = req.params.param.toUpperCase();
        var towers = require(`./data/torres/${param}.json`);
        var result = _.uniq(towers.features, true, f => { return f.properties.id })
        res.send(result.map(f => { return f.properties.id }));
    } catch (error) {
        res.status(500).send(error);
    }
});

router.get('/getDamages', (req, res) => {
    try {
        res.sendfile('./api/data/damages.json');
    } catch (error) {
        res.status(500).send(error);
    }
});

router.post('/saveDamage', (req, res) => {
    try {
        var damages = require('./data/damages.json');
        var contingencies = require('./data/contingencyLevel.json');
        var damage = req.body.damage;
        var contingency = req.body.contingency;

        contingencies.forEach(c => {
            if (c.id == contingency.id) {
                c.levels.forEach(l => {
                    if (l.name == contingency.name) {
                        l.peopleQuantity = contingency.peopleQuantity;
                    }
                });
            }
        });
        var params = [];
        _.each(damage.defaultParams, (val, key, list) => {
            params.push({
                key: key,
                value: val.name
            });
        });
        _.each(damage.damages, (val, key, list) => {
            params.push({
                key: key,
                value: val.name
            });
        });

        var _damage = {
            id: Date.now(),
            generateDate: new Date(),
            eventDate: new Date(damage.date),
            type: damage.damage.name,
            level: contingency.name,
            peopleQuantity: contingency.peopleQuantity,
            regional: contingency.regional,
            linePriority: parseInt((Math.random() * 10).toFixed(2)),
            concession: damage.concession,
            tower: damage.tower,
            params: params
        };
        damages.push(_damage);
        //damages = _.sortBy(damages, 'eventDate');
        fs.writeFile('./api/data/contingencyLevel.json', JSON.stringify(contingencies), err => {
            if (err) {
                console.error("error on try update contingency lelves");
            }
            fs.writeFile('./api/data/damages.json', JSON.stringify(damages), err => {
                if (err) {
                    res.status(500).send(error);
                    return;
                }
                res.send(200);
            });
        });
    } catch (error) {
        res.status(500).send(error);
    }
});

router.get('/getAlarmParameters', (req, res) => {
    try {
        res.sendfile('./api/data/alarmParameters.json');
    } catch (error) {
        res.status(500).send(error);
    }
});

router.post('/uploadAlarmSound', function (req, res) {
    if (!req.files)
        return res.status(400).send('No files were uploaded.');

    //./www/scripts/dist/sounds
    var alarmLevel = req.body.name;
    let soundFile = req.files.sound;
    var patch = `./www/scripts/dist/sounds/${soundFile.name}`;
    var alarmParameters = require('./data/alarmParameters.json');
    // // Use the mv() method to place the file somewhere on your server
    soundFile.mv(patch, function (err) {
        if (err)
            return res.status(500).send(err);

        var found;
        alarmParameters.forEach(p => {
            if (p.name == alarmLevel) {
                p.alarmPath = `/scripts/dist/sounds/${soundFile.name}`;
                found = p;
            }
        });
        fs.writeFile('./api/data/alarmParameters.json', JSON.stringify(alarmParameters), err => {
            if (err) {
                res.status(500).send(error);
                return;
            }
            res.send(found);
        });
    });
});

router.post('/uploadIconFile', function (req, res) {
    if (!req.files)
        return res.status(400).send('No files were uploaded.');

    //./www/scripts/dist/sounds
    var iconName = req.body.name;
    let soundFile = req.files.icon;
    var path = `./www/scripts/dist/img/${soundFile.name}`;
    var icons = require('./data/icons.json');
    soundFile.mv(path, function (err) {
        if (err)
            return res.status(500).send(err);

        var found;
        icons.forEach(p => {
            if (p.element == iconName) {
                p.path = `/scripts/dist/img/${soundFile.name}`;
                found = p;
            }
        });
        fs.writeFile('./api/data/icons.json', JSON.stringify(icons), err => {
            if (err) {
                res.status(500).send(error);
                return;
            }
            res.send(found);
        });
    });
});

router.post('/saveAlarmConfiguration', function (req, res) {
    var params = req.body;
    try {
        params.forEach(p => {
            delete p.$$hashKey;
            p.emails = p.emails.clean();
        });
        fs.writeFile('./api/data/alarmParameters.json', JSON.stringify(params), err => {
            if (err) {
                console.error("error on try save alarm parameterss");
            }
            res.send(200);
        });
    } catch (error) {
        res.status(500).send(error);
    }
});

router.get('/getLastMonitoredDate/:event', (req, res) => {
    try {
        var event = req.params.event;
        var path = '';
        if (event == 'thunder') {
            path = `./data/historicData/${new Date().toLocaleDateString()}_thunders.json`;
        } else {
            path = `./data/historicData/${new Date().toLocaleDateString()}_fireoutbreaks.json`;
        }
        var historic = require(path);
        res.send(new Date(historic[historic.length - 1].date));
    } catch (error) {
        res.status(500).send(error);
    }
});

router.get('/getMonitoredEvents/:event/:startDate/:endDate', (req, res) => {
    try {
        var event = req.params.event;
        var startDate = new Date(req.params.startDate);
        var endDate = new Date(req.params.endDate);
        var directoryPath = './api/data/historicData/';
        var files = fs.readdirSync(directoryPath);

        files = files.filter(f => {
            if (f.indexOf(event) >= 0) {
                var date = new Date(f.split('_')[0]);
                return date >= new Date(startDate.toLocaleDateString()) && date <= new Date(endDate.toLocaleDateString());
            }
        });
        var result = [];
        files.forEach(f => {
            var historic = require(directoryPath.replace('/api', '') + f);
            var filter = historic.filter(h => {
                var date = new Date(h.date);
                return date >= startDate.getTime() && date <= endDate.getTime();
            });
            result = result.concat(filter);
        });
        //result.forEach(r => { console.log(new Date(r.date)) });
        res.send(result);
    } catch (error) {
        res.status(500).send(error);
    }
});

router.get('/getForecastEvents/:event/:startDate/:endDate', (req, res) => {
    try {
        var event = req.params.event;
        var startDate = new Date(req.params.startDate);
        var endDate = new Date(req.params.endDate);
        var directoryPath = './api/data/historicData/';
        var files = fs.readdirSync(directoryPath);
        files = files.filter(f => {
            if (f.indexOf('forecast') >= 0) {
                var date = new Date(f.split('_')[0]);
                return date >= new Date(startDate.toLocaleDateString()) && date <= new Date(endDate.toLocaleDateString());
            }
        });
        var result = [];
        files.forEach(f => {
            var historic = require(directoryPath.replace('/api', '') + f);
            var filter = historic[event].filter(h => {
                var date = new Date(h.date);
                return date >= startDate.getTime() && date <= endDate.getTime();
            });
            result = result.concat(filter);
        });
        //result.forEach(r => { console.log(new Date(r.date)) });
        res.send(result);
    } catch (error) {
        res.status(500).send(error);
    }
});


router.get('/getEventsFromDamage/:damageDate/:radius/:hoursInterval', (req, res) => {
    try {
        var date = new Date(req.params.damageDate);
        var concession = req.query.concession;
        var tower = req.query.tower;
        var radius = parseInt(req.params.radius);
        var hoursInterval = parseInt(req.params.hoursInterval);
        var startDate = new Date(new Date(req.params.damageDate).setHours(date.getHours() - hoursInterval));
        var endDate = new Date(new Date(req.params.damageDate).setHours(date.getHours() + hoursInterval));
        var towerInfo = require(`./data/torres/${concession}.json`).features.find(t => {
            return t.properties.id == tower;
        });

        var result = {
            fireoutbreak: [],
            thunder: [],
            wind: []
        };

        var directoryPath = './api/data/historicData';
        var files = fs.readdirSync(directoryPath).filter(f => {
            var d = new Date(f.split('_')[0]);
            return d >= new Date(startDate.toLocaleDateString()) && d <= new Date(endDate.toLocaleDateString());
        });
        var events = [];
        files.forEach(f => {
            var file = require(`./data/historicData/${f}`);
            //Eventos no range da data
            var _events = [];
            if (file.winds) {
                _events = file.winds.filter(e => {
                    var d = new Date(e.date);
                    return d >= startDate && d <= endDate;
                });
            } else {
                _events = file.filter(e => {
                    var d = new Date(e.date);
                    return d >= startDate && d <= endDate;
                });
            }
            events = events.concat(_events)
        });
        //Filtra a concessão e proximo à torre selecionada
        var eventsOnConcession = [];
        events.forEach(e => {
            var _events = e.points.filter(p => {
                var distance = turfDistance(towerInfo, p) * 1000;
                p.properties.distaceToTower = distance;
                return p.properties.concession == concession && distance <= radius;
            });
            eventsOnConcession = eventsOnConcession.concat(_events);
        });
        eventsOnConcession.forEach(e => {
            console.log(e);
            result[e.properties.equipmentType].push(e);
        });
        res.send(result);
    } catch (error) {
        res.status(500).send(error);
    }
});

router.get('/getThundersAndForecast/', (req, res) => {
    try {
        var body = req.query;
        var minuteTolerance = body.minutes ? parseInt(body.minutes) : 30;
        var radiusTolerance = body.radius ? parseInt(body.radius) : 50;
        var lines = body.lines;

        var startDate = new Date(body.startDate);
        var endDate = new Date(body.endDate);

        var directoryPath = './api/data/historicData';
        var files = fs.readdirSync(directoryPath).filter(f => {
            var d = f.split('_');
            if (d[1] == 'fireoutbreaks.json') return false;

            var d = new Date(d[0]);
            return d >= new Date(startDate.toLocaleDateString()) && d <= new Date(endDate.toLocaleDateString());
        });
        files = _.groupBy(files, f => {
            return f.split('_')[1].replace('.json', '');
        });

        var thunders = [];
        var foreCastThunders = [];
        files.thunders.forEach(f => {
            var file = require(`./data/historicData/${f}`);
            var _thunders = file.filter(e => {
                var d = new Date(e.date);
                return d >= startDate && d <= endDate;
            });
            _thunders.forEach(_t => {
                var d = new Date(_t.date);
                _t.points.forEach(element => {
                    element.properties.date = d;
                    element.properties.forecastEvents = [];
                    if (lines == undefined || lines.indexOf(element.properties.concession) < 0) return;
                    thunders.push(element);
                });
            });
        });
        files.forecast.forEach(f => {
            var file = require(`./data/historicData/${f}`);
            var _thunders = file.thunders.filter(e => {
                var d = new Date(e.date);
                var _startDate = new Date(new Date(startDate).setMinutes(startDate.getMinutes() - minuteTolerance));
                var _endDate = new Date(new Date(endDate).setMinutes(endDate.getMinutes() + minuteTolerance));
                return d >= _startDate && d <= _endDate;
            });
            _thunders.forEach(_t => {
                var d = new Date(_t.date);
                _t.points.forEach(element => {
                    element.properties.date = d;

                    if (lines == undefined || lines.indexOf(element.properties.concession) < 0) return;
                    foreCastThunders.push(element);
                });
            });
        });
        thunders = _.groupBy(thunders, t => { return t.properties.concession });

        // lines.forEach(l => {
        //     if (!thunders[l]) {
        //         thunders[l] = [];
        //     }
        // });       

        Object.keys(thunders).forEach(k => {
            thunders[k].forEach(t => {
                foreCastThunders.forEach(ft => {
                    var thunderDate = t.properties.date;
                    var ftDate = ft.properties.date;
                    var startForecastDate = new Date(new Date(ftDate).setMinutes(ftDate.getMinutes() - minuteTolerance));
                    var endForecastDate = new Date(new Date(ftDate).setMinutes(ftDate.getMinutes() + minuteTolerance));

                    var distance = turfDistance(t, ft) * 1000;
                    var isCloseToDate = thunderDate >= startForecastDate && thunderDate <= endForecastDate;
                    var isNearly = distance <= radiusTolerance;
                    if (isCloseToDate && isNearly) {
                        t.properties.forecastEvents.push({
                            lat: ft.geometry.coordinates[1],
                            lng: ft.geometry.coordinates[0],
                            concession: ft.properties.concession,
                            date: ft.properties.date,
                            distanceToRealEvent: distance,
                            projectedElectricalDischarges: ft.properties.projectedElectricalDischarges,
                            probability: ft.properties.probability,
                            priority: ft.properties.priority,
                        });
                    }
                });
            });
        });
        res.send({
            result: thunders,
        });
    } catch (error) {
        res.status(500).send(error);
    }
});

//forecast latlong http://servicos.cptec.inpe.br/XML/cidade/7dias/-25.9166813/-43.9344931/previsaoLatLon.xml

function HttpGET(url, resp, callback) {
    try {
        http.get(url, (r) => {
            let data = '';
            // A chunk of data has been recieved.
            r.on('data', (chunk) => {
                data += chunk;
            });
            // The whole response has been received. Print out the result.
            r.on('end', () => {
                parseString(data, function (err, result) {
                    if (callback) {
                        callback(result);
                        return;
                    }
                    if (resp) {
                        resp.send(result);
                    }
                    console.log(result);
                });
            });
        }).on("error", (err) => {
            console.log("Error: " + err.message);
        });

    } catch (error) {
        if (resp)
            resp.status(500).send('Erro ao tentar localizar a cidade');
    }
};

function getConcession(param, req, res) {
    var result = [];
    try {
        var acLine = JSON.parse(fs.readFileSync('./api/data/segmentos/' + param + '.json', 'utf8'));
        var tower = JSON.parse(fs.readFileSync('./api/data/torres/' + param + '.json', 'utf8'));

        tower.features.forEach(f => {
            f.properties.linePriority = acLine.features[0].properties.priority;
        });
        result.push({
            EquipmentType: "Cabos",
            FeederId: param.toUpperCase(),
            GeoJson: acLine,
            GeoJsonType: "LineString"
        });
        result.push({
            EquipmentType: "Torres",
            FeederId: param.toUpperCase(),
            GeoJson: tower,
            GeoJsonType: "Point"
        });
        res.send({ type: "concession", data: result });
    } catch (error) {
        res.status(500).send(error.message);
    }
};

function getFireOutbreaks(req, res) {
    try {
        var result = [];
        var fireoutbreaks = forecast.getOutbreakFire();
        var geojson = {
            type: "FeatureCollection",
            features: []
        }
        fireoutbreaks.forEach(point => {
            geojson.features.push(point);
        });
        fireoutbreaks.forEach(point => {
            geojson.features.push({
                type: "Feature",
                properties: {
                    tempId: point.properties.tempId,
                    concession: point.properties.concession,
                    point: point.properties.point,
                    distance: point.properties.distance,
                    collectionDate: point.properties.collectionDate,
                    priority: point.properties.priority,
                    equipmentType: "range"
                },
                geometry: point.geometry
            });
        });

        result.push({
            EquipmentType: "Focos",
            FeederId: "Focos de incêndio",
            GeoJson: geojson,
            GeoJsonType: "Point"
        });
        res.send({ type: "fireoutbreaks", data: result });
    } catch (error) {
        res.status(500).send(error.message);
    }
};

function getForecastWind(req, res) {
    try {
        var result = [];
        var winds = forecast.getObservedWinds();
        var geojson = {
            type: "FeatureCollection",
            features: []
        }
        winds.forEach(point => {
            geojson.features.push(point);
        });
        winds.forEach(point => {
            geojson.features.push({
                type: "Feature",
                properties: {
                    tempId: point.properties.tempId,
                    concession: point.properties.concession,
                    point: point.properties.point,
                    distance: point.properties.distance,
                    collectionDate: point.properties.collectionDate,
                    priority: point.properties.priority,
                    equipmentType: "range"
                },
                geometry: point.geometry
            });
        });

        result.push({
            EquipmentType: "Ventos observados",
            FeederId: "Previsão de Ventos",
            GeoJson: geojson,
            GeoJsonType: "Point"
        });

        var forecastWind = forecast.getForecastWind();

        var geojson = {
            type: "FeatureCollection",
            features: []
        }
        forecastWind.forEach((forecast, index, array) => {
            var color = getForeCastColor(index + 1, array.length);
            forecast.points.forEach(point => {
                geojson.features.push({
                    type: "Feature",
                    properties: {
                        tempId: point.properties.tempId,
                        lat: point.properties.lat,
                        lng: point.properties.lng,
                        date: point.properties.date,
                        radius: point.properties.radius,
                        priority: point.properties.priority,
                        equipmentType: "range"
                    },
                    geometry: point.geometry
                });
            });
        });
        forecastWind.forEach(forecast => {
            forecast.points.forEach(point => {
                geojson.features.push({
                    type: "Feature",
                    properties: {
                        tempId: point.properties.tempId,
                        lat: point.properties.lat,
                        lng: point.properties.lng,
                        forecastDate: point.properties.date,
                        distance: point.properties.distance,
                        equipmentType: "forecastWind",
                        concession: point.properties.concession,
                        lastUpdate: point.properties.lastUpdate,
                        linePriority: point.properties.linePriority,
                        probability: point.properties.probability,
                        criticality: point.properties.criticality,
                        priority: point.properties.priority,
                        windVelocity: point.properties.windVelocity,
                        windDirection: point.properties.windDirection
                    },
                    geometry: point.geometry
                });
            });
        });
        result.push({
            EquipmentType: "Previsto",
            FeederId: "Previsão de Ventos",
            GeoJson: geojson,
            GeoJsonType: "Point"
        });
        res.send({ type: "forecastWind", data: result });

    } catch (e) {
        res.status(500).send(e);
    }
};

function getThunders(req, res) {
    try {
        criticalParam = require('./data/criticalParameters.json');
        var data = forecast.getThunders();
        var result = [];
        for (var i in criticalParam) {
            if (i.indexOf("_") == 0) continue;
            var param = criticalParam[i];
            var geojson = {
                type: "FeatureCollection",
                features: data.filter((e) => {
                    if (e.properties.distance >= param.minValue && e.properties.distance < param.maxValue) {
                        e.properties.criticalColor = param.color;
                        return e;
                    }
                })
            };

            if (geojson.features.length <= 0) continue;


            result.push({
                EquipmentType: param.alias,
                FeederId: "Raios",
                GeoJson: geojson,
                GeoJsonType: "Point"
            });
        }
        res.send({ type: "thunders", data: result });
    } catch (error) {
        res.status(500).send(error.message);
    }
};

function getForecastFire(req, res) {
    var result = [];
    var fireoutbreaks = forecast.getOutbreakFire();
    var geojson = {
        type: "FeatureCollection",
        features: []
    }
    fireoutbreaks.forEach(point => {
        geojson.features.push(point);
    });
    fireoutbreaks.forEach(point => {
        geojson.features.push({
            type: "Feature",
            properties: {
                tempId: point.properties.tempId,
                concession: point.properties.concession,
                point: point.properties.point,
                distance: point.properties.distance,
                collectionDate: point.properties.collectionDate,
                priority: point.properties.priority,
                equipmentType: "range"
            },
            geometry: point.geometry
        });
    });

    result.push({
        EquipmentType: "Foco de incêndio",
        FeederId: "Previsão de incêndio",
        GeoJson: geojson,
        GeoJsonType: "Point"
    });

    var forecastFire = forecast.getForecastFire();

    var geojson = {
        type: "FeatureCollection",
        features: []
    }
    forecastFire.forEach((forecast, index, array) => {
        var color = getForeCastColor(index + 1, array.length);
        forecast.points.forEach(point => {
            geojson.features.push({
                type: "Feature",
                properties: {
                    tempId: point.properties.tempId,
                    lat: point.properties.lat,
                    lng: point.properties.lng,
                    date: point.properties.date,
                    radius: point.properties.radius,
                    priority: point.properties.priority,
                    equipmentType: "range"
                },
                geometry: point.geometry
            });;
        });
    });
    forecastFire.forEach(forecast => {
        forecast.points.forEach(point => {
            geojson.features.push({
                type: "Feature",
                properties: {
                    tempId: point.properties.tempId,
                    lat: point.properties.lat,
                    lng: point.properties.lng,
                    forecastDate: point.properties.date,
                    distance: point.properties.distance,
                    equipmentType: "forecastFire",
                    concession: point.properties.concession,
                    lastUpdate: point.properties.lastUpdate,
                    linePriority: point.properties.linePriority,
                    probability: point.properties.probability,
                    criticality: point.properties.criticality,
                    priority: point.properties.priority
                },
                geometry: point.geometry
            });
        });
    });
    result.push({
        EquipmentType: "Previsto",
        FeederId: "Previsão de incêndio",
        GeoJson: geojson,
        GeoJsonType: "Point"
    });
    res.send({ type: "forecastfire", data: result });
};

function getForecastThunder(req, res) {
    var result = [];

    var thunders = forecast.getThunders();
    var geojson = {
        type: "FeatureCollection",
        features: []
    };
    thunders.forEach(point => {
        geojson.features.push(point);
    });
    thunders.forEach(point => {
        geojson.features.push({
            type: "Feature",
            properties: {
                tempId: point.properties.tempId,
                concession: point.properties.concession,
                point: point.properties.point,
                distance: point.properties.distance,
                collectionDate: point.properties.collectionDate,
                color: "#ff0000",
                equipmentType: "range",
                projectedElectricalDischarges: point.properties.projectedElectricalDischarges
            },
            geometry: point.geometry
        });
    });
    result.push({
        EquipmentType: "Raio",
        FeederId: "Previsão de raios",
        GeoJson: geojson,
        GeoJsonType: "Point"
    });

    var forecastThunder = forecast.getForecastThunder();
    var geojson = {
        type: "FeatureCollection",
        features: []
    }
    forecastThunder.forEach((forecast, index, array) => {
        var color = getForeCastColor(index + 1, array.length);
        forecast.points.forEach(point => {
            geojson.features.push({
                type: "Feature",
                properties: {
                    tempId: point.properties.tempId,
                    lat: point.properties.lat,
                    lng: point.properties.lng,
                    date: point.properties.date,
                    radius: point.properties.radius,
                    distance: point.properties.distance,
                    priority: point.properties.priority,
                    equipmentType: "range"
                },
                geometry: point.geometry
            });;
        });
    });

    forecastThunder.forEach(forecast => {
        forecast.points.forEach(point => {
            geojson.features.push({
                type: "Feature",
                properties: {
                    tempId: point.properties.tempId,
                    lat: point.properties.lat,
                    lng: point.properties.lng,
                    forecastDate: point.properties.date,
                    distance: point.properties.distance,
                    equipmentType: "forecastThunder",
                    projectedElectricalDischarges: point.properties.projectedElectricalDischarges,
                    concession: point.properties.concession,
                    lastUpdate: point.properties.lastUpdate,

                    linePriority: point.properties.linePriority,
                    probability: point.properties.probability,
                    criticality: point.properties.criticality,
                    priority: point.properties.priority

                },
                geometry: point.geometry
            });
        });
    });

    result.push({
        EquipmentType: "Previsto",
        FeederId: "Previsão de raios",
        GeoJson: geojson,
        GeoJsonType: "Point"
    });

    res.send({ type: "forecastThunder", data: result });
};

function getCriticalColor(value) {
    if (value <= criticalParam.high.value) {
        return criticalParam.high.color;
    } else if (value >= criticalParam.low.value) {
        return criticalParam.low.color;
    }
    return criticalParam.medium.color;
};

function getForeCastColor(value, end) {
    var value = Math.round((510 * value) / end);
    if (value <= 255) {
        value = value.toString(16);
        if (value.length == 1)
            value = "0" + value;
        return `#ff${value}00`;
    } else {
        value = (255 - (value - 255)).toString(16);
        if (value.length == 1)
            value = "0" + value;
        return `#${value}ff00`;
    }
};

function getAllAlarms(req, res) {
    try {
        forecast.simuleForeCast();
        var obj = { type: "alarms", data: forecast.getAllAlarms() };
        res.send(obj);
    } catch (error) {
        res.status(500).send(error);
    }

};

Array.prototype.clean = function () {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == "" || this[i] == null || this[i] == undefined) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};
module.exports = router;