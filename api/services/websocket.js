module.exports = function (forecast) {
    var module = {};
    var http = require('http');
    var pointToLineDistance = require('@turf/point-to-line-distance');
    var bbox = require('@turf/bbox');
    var random = require('@turf/random');
    var destination = require('@turf/destination');
    var WebSocketServer = require('websocket').server;
    var bufferAllArea = require('../data/bufferAllArea.json');
    var listeners = [];

    var server = http.createServer(function (request, response) {
        console.log((new Date()) + ' Received request for ' + request.url);
        response.writeHead(404);
        response.end();
    });

    wsServer = new WebSocketServer({
        httpServer: server,
        // You should not use autoAcceptConnections for production
        // applications, as it defeats all standard cross-origin protection
        // facilities built into the protocol and the browser.  You should
        // *always* verify the connection's origin and decide whether or not
        // to accept it.
        autoAcceptConnections: false
    });

    wsServer.on('request', function (request) {
        if (!module.originIsAllowed(request.origin)) {
            // Make sure we only accept requests from an allowed origin
            request.reject();
            console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
            return;
        }

        var connection = request.accept(undefined, request.origin);
        console.log((new Date()) + ' Connection accepted.');
        connection.on('message', function (message) {
            if (message.type === 'utf8') {
                if (message.utf8Data == "") return;

                console.log('Received Message: ' + message.utf8Data);
                module.messageProcessing(JSON.parse(message.utf8Data), connection);
                //connection.sendUTF(message.utf8Data);
            }
            else if (message.type === 'binary') {
                console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
                //connection.sendBytes(message.binaryData);
            }
        });
        connection.on('close', function (reasonCode, description) {
            module.messageProcessing("DISCONNECT");
            var index = listeners.findIndex(l => l.conn == connection);
            listeners.splice(index, 1);
            console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
        });
    });

    module.originIsAllowed = function (origin) {
        // put logic here to detect whether the specified origin is allowed.
        return true;
    };

    module.messageProcessing = function (message, connection) {
        switch (message.Type) {
            case "REGISTER LISTENER":
                listeners.push({
                    conn: connection,
                    user: message.UserId,
                    peer: connection.remoteAddress
                });
                var _message = {
                    "Type": "Success",
                    "Message": "Cliente registrado com sucesso ao servidor",
                    "MessageType": "Status",
                    "UserId": message.UserId,
                    "Date": new Date().toISOString()
                }
                connection.sendUTF(JSON.stringify(_message));
                var directoryPath = '../data/historicData';
                var startDate = new Date();
                var endDate = new Date(new Date().getTime() + (24 * 60 * 60 * 1000));
                var filePrefixStart = startDate.toLocaleDateString();
                var filePrefixEnd = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate() + 1).toLocaleDateString();
                var pathStart = `${directoryPath}/${filePrefixStart}_forecast.json`;
                var pathEnd = `${directoryPath}/${filePrefixEnd}_forecast.json`;
                var dataStart = require(pathStart)
                var dataEnd = require(pathEnd);
                var thunders = dataStart.thunders.filter(t => {
                    return new Date(t.date) > startDate && new Date(t.date) <= endDate;
                }).concat(dataEnd.thunders.filter(t => {
                    return new Date(t.date) > startDate && new Date(t.date) <= endDate;
                }));

                var winds = dataStart.winds.filter(t => {
                    return new Date(t.date) > startDate && new Date(t.date) <= endDate;
                }).concat(dataEnd.winds.filter(t => {
                    return new Date(t.date) > startDate && new Date(t.date) <= endDate;
                }));
                module.sendAllAlarmsToUsers({
                    thunders: thunders,
                    winds: winds
                });
                break;
            case "DISCONNECT":
                debugger;
                break;
        }
    };

    module.sendAllAlarmsToUsers = function (data) {
        listeners.forEach(l => {
            data.thunders.forEach(t => {
                var forecast = t.date
                t.points.forEach(point => {
                    var alarm = {
                        point: point.properties,
                        type: "Previsão de Raios"
                    }
                    var _message = {
                        "Type": "Warning",
                        "Message": `Alarme - Previsão de Raios - ${point.properties.priority}`,
                        "MessageType": "alarm",
                        "Date": new Date(point.properties.date).toISOString(),
                        "Data": alarm
                    }
                    l.conn.sendUTF(JSON.stringify(_message));
                });
            });

            data.winds.forEach(w => {
                var forecast = w.date
                w.points.forEach(point => {
                    var alarm = {
                        point: point.properties,
                        type: "Previsão de Ventos"
                    }
                    var _message = {
                        "Type": "Warning",
                        "Message": `Alarme - Previsão de Ventos - ${point.properties.priority}`,
                        "MessageType": "alarm",
                        "Date": new Date(point.properties.date).toISOString(),
                        "Data": alarm
                    }
                    l.conn.sendUTF(JSON.stringify(_message));
                });
            });
        });
    };

    module.HttpGET = function (url, resp, callback) {
        try {
            http.get(url, (r) => {
                let data = '';
                // A chunk of data has been recieved.
                r.on('data', (chunk) => {
                    data += chunk;
                });
                // The whole response has been received. Print out the result.
                r.on('end', () => {
                    if (callback) {
                        callback(JSON.parse(data));
                    } else if (res) {
                        resp.send(JSON.parse(data));
                    }
                });
            }).on("error", (err) => {
                console.log("Error: " + err.message);
            });

        } catch (error) {
            if (resp)
                resp.status(500).send('Erro ao tentar localizar a cidade');
        }
    };

    server.listen(8081, function () {
        console.log('websocket is alive at 8081!');
    });

    return module;
}


