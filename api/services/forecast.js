//#region properties
var pointToLineDistance = require('@turf/point-to-line-distance');
var bbox = require('@turf/bbox');
var random = require('@turf/random');
var destination = require('@turf/destination');
var fs = require("fs");
var bufferAllArea = require('../data/bufferAllArea.json');
var satelliteList = require('../data/satelliteList.json');
var criticalValues = ["Alto", "Médio", "Baixo"];
var historicInterval = parseInt(process.env.HISTORIC_INTERVAL) * 60 * 1000;
var historicDays = parseInt(process.env.HISTORICDAYS) * 24 * 60 * 60 * 1000;
var thunderMonitoredInterval = parseInt(process.env.THUNDER_MONITORED_INTERVAL) * 60 * 1000;
var fireMonitoredInterval = parseInt(process.env.FIREOUTBREAK_MONITORED_INTERVAL) * 60 * 1000;
var websocket = require('./websocket.js')(this);
//#endregion

//#region Thunders
async function simulateHistoricDataForMonitoredThunders() {
  try {
    var finalDate = new Date();
    var directoryPath = './api/data/historicData';
    var files = fs.readdirSync(directoryPath).filter(f => {
      return f.split('_')[1] == 'forecast.json';
    });
    var existHistorics = fs.readdirSync('./api/data/historicData').filter(f => {
      return f.indexOf('thunders.json') >= 0;
    });

    files.forEach(f => {
      var foreCastFile = f.split('_');
      var path = `${directoryPath}/${foreCastFile[0]}_thunders.json`;
      if (existHistorics.indexOf(foreCastFile[0] + '_thunders.json') >= 0) {
        return;
      }
      var file = require(`../data/historicData/${f}`);
      var result = [];
      file.thunders.forEach(t => {
        var d = new Date(t.date);
        if (d >= finalDate) return;

        var collectionDate = new Date(d.getTime() + getRandomInt(1, 59 * 60 * 1000));
        var obj = { date: collectionDate.getTime(), points: [] };
        t.points.forEach(p => {

          var thunderType = p.properties.thunderType;
          var electricalDischarges = p.properties.projectedElectricalDischarges;
          var concession = p.properties.concession;
          var linePriority = p.properties.linePriority;
          var distance = p.properties.distance;

          p = destination(p, Math.random() / 10, generateRandomDisplacement(), { units: "kilometers" });


          var intensity = "Baixo";
          if (electricalDischarges > 30 && electricalDischarges < 180) {
            intensity = "Médio";
          } else if (electricalDischarges > 180) {
            intensity = "Alto";
          }
          p.properties = {
            collectionDate: collectionDate,
            equipmentType: "thunder",
            thunderType: thunderType,
            intensity: intensity,
            electricalDischarges: electricalDischarges + Math.round((Math.random() - 0.5) * 2 * electricalDischarges),
            concession: concession,
            linePriority: linePriority,
            distance: distance
          }
          obj.points.push(p);
        });
        result.push(obj);
      });
      if (result.length < 1) return;

      fs.writeFile(path, JSON.stringify(result), (err, data) => {
        if (err) {
          console.error(err);
          return;
        }
        console.info(`${path} - Thunder historic saved!`);
      });
    });
    // while (_initialDate < finalDate) {
    //   var filePrefix = _initialDate.toLocaleDateString();
    //   var path = `${directoryPath}/${filePrefix}_thunders.json`;
    //   if (existHistorics.indexOf(filePrefix + '_thunders.json') >= 0) {
    //     _initialDate.setTime(_initialDate.getTime() + (24 * 60 * 60 * 1000));
    //     continue;
    //   }
    //   var result = await simulateThunders(_initialDate).catch(e => { console.error(e) });
    //   fs.writeFile(path, JSON.stringify(result), (err, data) => {
    //     if (err) {
    //       console.error(err);
    //       return;
    //     }
    //     console.info(`${filePrefix} - Thunder historic saved!`);
    //   });
    //   _initialDate.setTime(_initialDate.getTime() + (24 * 60 * 60 * 1000));
    // }
    // console.log("Historico descargas elétricas gerado.")
  } catch (error) {
    console.error(error)
  }
};
async function simulateMonitoredThunders() {
  try {
    var date = new Date();
    var directoryPath = './api/data/historicData/';
    var filePrefix = date.toLocaleDateString();
    var path = `${directoryPath}${filePrefix}_thunders.json`;
    var file = JSON.parse(fs.readFileSync(path, 'UTF-8'));
    var result = await simulateThunders(date, path).catch(e => console.error(e));
    var file = file.concat(result);
    fs.writeFile(path, JSON.stringify(file), (err, data) => {
      if (err) {
        console.error(err);
        return;
      }
      console.info(`${filePrefix} - Thunder monitored saved!`);
    });
  } catch (error) {
    console.error(error);
  }
};
function simulateThunders(date) {
  return new Promise((resolve, reject) => {
    try {
      var thunderTypes = ["nuvem-solo", "intra-nuvem", "solo-nuvem"];
      var result = [];
      var points = getRandomInt();
      var _initialDate = new Date(date);
      var _endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59);

      while (_initialDate <= _endDate && _initialDate <= new Date()) {
        var obj = { date: _initialDate.getTime(), points: [] };
        while (obj.points.length < points) {
          var marker = getRandom_marker();
          if (marker) {
            var electricalDischarges = getRandomInt(1, 200);
            var intensity = "Baixo";
            if (electricalDischarges > 30 && electricalDischarges < 180) {
              intensity = "Médio";
            } else if (electricalDischarges > 180) {
              intensity = "Alto";
            }
            marker.properties.collectionDate = _initialDate;
            marker.properties.equipmentType = "thunder";
            marker.properties.thunderType = thunderTypes[Math.floor(Math.random() * thunderTypes.length)];
            marker.properties.intensity = intensity;
            marker.properties.electricalDischarges = electricalDischarges;
            obj.points.push(marker);
          }
        }
        result.push(obj);
        _initialDate = new Date(_initialDate.getTime() + historicInterval);
      }
      resolve(result);
    } catch (error) {
      reject(error);
    }
  });
};
//Simula Histórico
//simulateHistoricDataForMonitoredThunders();
setInterval(() => {
  //simulateMonitoredThunders()
}, thunderMonitoredInterval);
//#endregion

//#region Fireoutbreak
async function simulateHistoricDataForMonitoredFireoutbreaks() {
  try {
    var directoryPath = './api/data/historicData';
    var d = new Date(new Date().toLocaleDateString());
    var finalDate = new Date();
    var _initialDate = new Date(d.getTime() - historicDays);
    var files = fs.readdirSync(directoryPath);
    files.forEach(f => {
      if (f.indexOf(d.toLocaleDateString() + '_fireoutbreaks.json') >= 0) {
        fs.unlinkSync(`${directoryPath}/${f}`);
      }
    });
    var existHistorics = fs.readdirSync('./api/data/historicData').filter(f => {
      return f.indexOf('_fireoutbreaks.json') >= 0;
    });
    while (_initialDate < finalDate) {
      var filePrefix = _initialDate.toLocaleDateString();
      var path = `${directoryPath}/${filePrefix}_fireoutbreaks.json`;
      if (existHistorics.indexOf(filePrefix + '_fireoutbreaks.json') >= 0) {
        _initialDate.setTime(_initialDate.getTime() + (24 * 60 * 60 * 1000));
        continue;
      }

      var result = await simulateFireoutbreaks(_initialDate).catch(e => { console.error(e) });
      fs.writeFile(path, JSON.stringify(result), (err, data) => {
        if (err) {
          console.error(err);
          return;
        }
        console.info(`${filePrefix} - Fireoutbreak historic saved!`);
      });
      _initialDate.setHours(_initialDate.getHours() + 24);
    }
    console.log("Historico queimadas gerado.");
  } catch (error) {
    console.error(error)
  }
};
async function simulateMonitoredFireoutbreaks() {
  try {
    var date = new Date();
    var directoryPath = './api/data/historicData/';
    var filePrefix = date.toLocaleDateString();
    var path = `${directoryPath}${filePrefix}_fireoutbreaks.json`;
    var file = JSON.parse(fs.readFileSync(path, 'UTF-8'));
    var result = await simulateFireoutbreaks(date).catch(e => console.error(e));
    var file = file.concat(result);
    fs.writeFile(path, JSON.stringify(file), (err, data) => {
      if (err) {
        console.error(err);
        return;
      }
      console.info(`${filePrefix} - Fireoutbreak monitored saved!`);
    });
  } catch (error) {
    console.error(error);
  }
};
function simulateFireoutbreaks(date) {
  return new Promise((resolve, reject) => {
    try {
      var result = [];

      var _initialDate = new Date(date);
      var _endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59);

      while (_initialDate <= _endDate && _initialDate <= new Date()) {
        var collectionDate = new Date(_initialDate.getTime() + getRandomInt(1, 59 * 60 * 1000));
        var obj = { date: collectionDate.getTime(), points: [] };
        var points = getRandomInt();
        while (obj.points.length < points) {
          var marker = getRandom_marker();
          if (marker) {
            marker.properties.collectionDate = new Date(collectionDate);
            marker.properties.equipmentType = "fireoutbreak";
            marker.properties.satellite = satelliteList[Math.floor(Math.random() * satelliteList.length)].satellite;
            obj.points.push(marker);
          }
        }
        result.push(obj);
        _initialDate = new Date(_initialDate.getTime() + historicInterval);
      }
      resolve(result);
    } catch (error) {
      reject(error);
    }
  });
};
//Simula Histórico
simulateHistoricDataForMonitoredFireoutbreaks();
setInterval(() => {
  simulateMonitoredFireoutbreaks()
}, fireMonitoredInterval);
//#endregion

//#region ForeCast
async function simulateHistoricDataForForeCast() {
  try {
    var directoryPath = './api/data/historicData';
    var d = new Date(new Date().toLocaleDateString());
    var _initialDate = new Date(d.getTime() - historicDays);
    var finalDate = new Date(d.getFullYear(), d.getMonth(), d.getDate() + 1, 23, 59, 59);
    var existHistorics = fs.readdirSync('./api/data/historicData').filter(f => {
      return f.indexOf('_forecast.json') >= 0;
    });
    while (_initialDate < finalDate) {
      var filePrefix = _initialDate.toLocaleDateString();
      var path = `${directoryPath}/${filePrefix}_forecast.json`;
      if (existHistorics.indexOf(filePrefix + '_forecast.json') >= 0) {
        _initialDate.setHours(_initialDate.getHours() + 24);
        continue;
      }
      var _endDate = new Date(_initialDate.getFullYear(), _initialDate.getMonth(), _initialDate.getDate(), 23, 59, 59);
      var _forecast = {
        winds: simulateForecastWind(_initialDate, _endDate),
        thunders: simulateForecastThunder(_initialDate, _endDate)
      };
      fs.writeFileSync(path, JSON.stringify(_forecast), { encoding: "UTF-8" });
      console.info(`${filePrefix} - Forecast historic saved!`);
      _initialDate.setHours(_initialDate.getHours() + 24);
    }
    simulateHistoricDataForMonitoredThunders();
    console.log("Historico previsões gerado.");
  } catch (error) {
    console.error(error)
  }
};
function simulePoints() {
  var points = getRandomInt();
  var observedWindList = [];
  while (observedWindList.length < points) {
    var marker = getRandom_marker();
    if (marker) {
      marker.properties.lastUpdate = marker.properties.collectionDate;
      marker.properties.windVelocity = parseFloat((Math.random() * 300).toFixed(2));
      marker.properties.windDirection = Math.round((Math.random() * 360));

      var priority = Math.round(2 - (3 * ((marker.properties.linePriority + marker.properties.criticality + (marker.properties.windVelocity / 300)) / 3)));
      if (priority > 3) {
        marker.properties.priority = criticalValues[3]
      } else if (priority < 0) {
        marker.properties.priority = criticalValues[0];
      }
      else {
        marker.properties.priority = criticalValues[priority]
      }
      observedWindList.push(marker);
    }
  }
  return observedWindList;
};
function simulateForecastWind(startDate, endDate) {
  var forecastWind = [];
  var tempWind = simulePoints();
  var distance = eval(process.env.CRITICALRADIUS);
  var maxDistance = eval(process.env.MAXDISTANCEMETERS) / 1000;
  var _initialDate = new Date(startDate);
  while (_initialDate < endDate) {
    var _forecastWind = {
      date: new Date(_initialDate),
      points: []
    }
    tempWind.forEach((wind, index, arrray) => {
      if (index == 0) {
        //console.log(JSON.stringify(wind.geometry.coordinates));
      }
      if (!wind.properties.bearing) {
        wind.properties.bearing = generateRandomBearing();
      } else {
        wind.properties.bearing += generateRandomDisplacement();
      }
      var d = destination(wind, distance, wind.properties.bearing, { units: "kilometers" });

      var line = require('../data/segmentos/' + wind.properties.concession + '.json').features[0];
      var _distance = pointToLineDistance(d, line, { units: 'kilometers' });
      if (_distance > maxDistance)
        return;

      d.properties.tempId = wind.properties.tempId;
      d.properties.bearing = wind.properties.bearing;
      d.properties.concession = wind.properties.concession;
      d.properties.lat = d.geometry.coordinates[0];
      d.properties.lng = d.geometry.coordinates[1];
      d.properties.date = _initialDate;
      d.properties.radius = distance * 1000;
      d.properties.distance = _distance * 1000;
      d.properties.lastUpdate = new Date();
      d.properties.equipmentType = "wind";

      //dados criticos
      d.properties.linePriority = line.properties.priority;
      d.properties.probability = parseFloat((Math.random() * 10).toFixed(0)) / 10;
      d.properties.criticality = Math.random();
      d.properties.windVelocity = parseFloat((Math.random() * 20).toFixed(2));
      d.properties.windDirection = getWindDirection();

      var priority = Math.round(2 - (3 * ((d.properties.linePriority + d.properties.criticality + d.properties.probability + (d.properties.windVelocity / 300)) / 4)));
      if (priority > 3) {
        d.properties.priority = criticalValues[2]
      } else if (priority < 0) {
        d.properties.priority = criticalValues[0];
      }
      else {
        d.properties.priority = criticalValues[priority]
      }

      _forecastWind.points.push(d);
      tempWind[index] = d;
    });
    forecastWind.push(_forecastWind);
    _initialDate = new Date(_initialDate.setHours(_initialDate.getHours() + 1));;
  }
  return forecastWind;
};
function simulateForecastThunder(startDate, endDate) {
  var forecastThunder = [];
  var tempThunder = simulePoints();
  var distance = eval(process.env.CRITICALRADIUS);
  var maxDistance = eval(process.env.MAXDISTANCEMETERS) / 1000;
  var _initialDate = new Date(startDate);
  var thunderTypes = ["nuvem-solo", "intra-nuvem", "solo-nuvem"];
  while (_initialDate < endDate) {
    var _forecastThunder = {
      date: new Date(_initialDate),
      points: []
    }
    tempThunder.forEach((thunder, index, arrray) => {
      if (index == 0) {
        //console.log(JSON.stringify(thunder.geometry.coordinates));
      }
      if (!thunder.properties.bearing) {
        thunder.properties.bearing = generateRandomBearing();
      } else {
        thunder.properties.bearing += generateRandomDisplacement();
      }

      var d = destination(thunder, distance, thunder.properties.bearing, { units: "kilometers" });
      var line = require('../data/segmentos/' + thunder.properties.concession + '.json').features[0];
      var _distance = pointToLineDistance(d, line, { units: 'kilometers' });
      if (_distance > maxDistance)
        return;


      d.properties.tempId = thunder.properties.tempId;
      d.properties.bearing = thunder.properties.bearing;
      d.properties.concession = thunder.properties.concession;
      d.properties.lat = d.geometry.coordinates[0];
      d.properties.lng = d.geometry.coordinates[1];
      d.properties.date = _initialDate;
      d.properties.distance = _distance * 1000;
      d.properties.lastUpdate = new Date();
      d.properties.equipmentType = "thunder";
      d.properties.thunderType = thunderTypes[Math.floor(Math.random() * thunderTypes.length)];

      //dados criticos
      d.properties.radius = Math.random() * 100;
      d.properties.linePriority = line.properties.priority;
      d.properties.criticality = Math.random();
      d.properties.probability = parseFloat((Math.random() * 10).toFixed(0)) / 10;
      d.properties.projectedElectricalDischarges = Math.floor(d.properties.criticality * 100);

      var priority = Math.round(2 - (3 * ((d.properties.linePriority + d.properties.criticality + d.properties.probability) / 3)));
      if (priority > 3) {
        d.properties.priority = criticalValues[2]
      } else if (priority < 0) {
        d.properties.priority = criticalValues[0];
      }
      else {
        d.properties.priority = criticalValues[priority]
      }

      _forecastThunder.points.push(d);
      tempThunder[index] = d;
    });
    forecastThunder.push(_forecastThunder);
    _initialDate = new Date(_initialDate.setHours(_initialDate.getHours() + 1));
  }
  return forecastThunder;
};
//#endregion

function getRandom_marker() {
  var feature = bufferAllArea.features[Math.floor(Math.random() * bufferAllArea.features.length)];
  var randomPoint = random.randomPoint(100, { bbox: bbox(feature) });
  var line = require('../data/segmentos/' + feature.properties.concession + '.json').features[0];
  var distance = eval(process.env.MAXDISTANCEMETERS) / 1000;
  var point, target;

  randomPoint.features.forEach(f => {
    var d = pointToLineDistance(f, line, { units: 'kilometers' });
    if (d < distance) {
      distance = d;
      point = f.geometry.coordinates;
      target = f;
    }
  });

  if (!target) {
    return;
  }

  target.properties = {
    tempId: new Date().getTime(),
    concession: feature.properties.concession,
    point: point,
    distance: distance * 1000,
    collectionDate: new Date(),
    //dados criticos
    linePriority: line.properties.priority,
    criticality: Math.random()
  }
  return target;
};
function generateRandomBearing() {
  return Math.cos(Math.PI * Math.round(Math.random())) * Math.ceil(Math.random() * 180);
};
function getRandomInt(min, max) {
  if (!min || !max) {
    min = 0;
    max = parseInt(process.env.MAXPOINTS);
  }
  return Math.floor(Math.random() * (max - min + 1)) + min;
};
function generateRandomDisplacement() {
  return Math.cos(Math.PI * Math.round(Math.random())) * Math.ceil(Math.random() * 45);
};
function getWindDirection() {
  var cardinaisPoints = ["Leste", "Norte", "Oeste", "Sul", "Nordeste", "Noroeste", "Sudeste", "Sudoeste", "lés-nordeste", "lés-sudeste", "sul-sudeste", "nor-nordeste", "nor-noroeste", "sul-sudoeste", "oés-sudoeste", "oés-noroeste"];
  return cardinaisPoints[Math.floor(Math.random() * cardinaisPoints.length)];
};
module.exports = {
  getForeCastSimulated: function () {
    var directoryPath = '../data/historicData';
    var startDate = new Date();
    var endDate = new Date(new Date().getTime() + (24 * 60 * 60 * 1000));
    var filePrefixStart = startDate.toLocaleDateString();
    var filePrefixEnd = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate() + 1).toLocaleDateString();
    var pathStart = `${directoryPath}/${filePrefixStart}_forecast.json`;
    var pathEnd = `${directoryPath}/${filePrefixEnd}_forecast.json`;
    var dataStart = require(pathStart)
    var dataEnd = require(pathEnd);
    var thunders = dataStart.thunders.filter(t => {
      return new Date(t.date) > startDate && new Date(t.date) <= endDate;
    }).concat(dataEnd.thunders.filter(t => {
      return new Date(t.date) > startDate && new Date(t.date) <= endDate;
    }));
    var winds = dataStart.winds.filter(t => {
      return new Date(t.date) > startDate && new Date(t.date) <= endDate;
    }).concat(dataEnd.winds.filter(t => {
      return new Date(t.date) > startDate && new Date(t.date) <= endDate;
    }));
    return {
      thunders: thunders,
      winds: winds
    };
  }
};
simulateHistoricDataForForeCast();
console.log('Forecast Simulation is alive!');