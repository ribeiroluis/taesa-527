class ArcMap {
    constructor() {
        this.loadMap();
    }
    loadMap() {
        require([
            "esri/Map",
            "esri/layers/FeatureLayer",
            "esri/views/MapView"
          ], function(
            Map,
            FeatureLayer,
            MapView
          ) {
      
            // Create the map
            let map = new Map({
              basemap: "gray"
            });
      
            // Create the MapView
            let view = new MapView({
              container: "viewDiv",
              map: map,
              center: [-73.950, 40.702],
              zoom: 11
            });
      
            /*************************************************************
             * The PopupTemplate content is the text that appears inside the
             * popup. {fieldName} can be used to reference the value of an
             * attribute of the selected feature. HTML elements can be used
             * to provide structure and styles within the content. The
             * fieldInfos property is an array of objects (each object representing
             * a field) that is use to format number fields and customize field
             * aliases in the popup and legend.
             **************************************************************/
      
            let template = { // autocasts as new PopupTemplate()
              title: "Marriage in NY, Zip Code: {ZIP}",
              content: [{
                // It is also possible to set the fieldInfos outside of the content
                // directly in the popupTemplate. If no fieldInfos is specifically set
                // in the content, it defaults to whatever may be set within the popupTemplate.
                type: "fields",
                fieldInfos: [{
                  fieldName: "MARRIEDRATE",
                  label: "Married %",
                  visible: true
                }, {
                  fieldName: "MARRIED_CY",
                  label: "People Married",
                  visible: true,
                  format: {
                    digitSeparator: true,
                    places: 0
                  }
                }, {
                  fieldName: "NEVMARR_CY",
                  label: "People that Never Married",
                  visible: true,
                  format: {
                    digitSeparator: true,
                    places: 0
                  }
                }, {
                  fieldName: "DIVORCD_CY",
                  label: "People Divorced",
                  visible: true,
                  format: {
                    digitSeparator: true,
                    places: 0
                  }
                }]
              }]
            };
      
            // Reference the popupTemplate instance in the
            // popupTemplate property of FeatureLayer
            let featureLayer = new FeatureLayer({
              url: "https://services.arcgis.com/V6ZHFr6zdgNZuVG0/ArcGIS/rest/services/NYCDemographics1/FeatureServer/0",
              outFields: ["*"],
              popupTemplate: template
            });
            map.add(featureLayer);
          });

    }
}
new ArcMap();