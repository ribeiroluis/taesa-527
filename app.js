require('dotenv').config()
var express = require('express');
var bodyParser = require('body-parser');
var fileUpload = require('express-fileupload');
var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(fileUpload());
var api = require('./api/route.js');
app.use('/api', api);
app.use(express.static('www'));
app.use('/arcgis', express.static('arcgis'));
app.use('/arcgis-leaflet', express.static('arcgis-leaflet'));
app.listen(process.env.PORT, function () {
    console.log(`Server is live at port ${process.env.PORT}!`);
});